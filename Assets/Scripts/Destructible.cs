﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: Destructible.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 16/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Cee Jay	| 16/05/16	|	- added various functions
/// 									- modified onDestroy callback to non parameter type
/// 					  17/05/16	|	- added support for multiple colliders and vital collider
/// 					  23/05/16	|	- modified parts of the code into interface design
/// 					  24/05/16	|	- fixed incorrect damage apply logic
/// 					  27/05/16	|	- added directional hit detection logic (Beta : required adjustment)
/// 		|Patrick	| 27/05/16  |   - changed update health ui calling function.
/// 					| 30/05/16  |   - changed update health ui calling function.
/// 					| 31/05/16  |   - changed update health ui calling function.
/// 					| 06/06/16  |   - changed update health ui calling function.
/// 					| 08/06/16 	|   - Added ReceiveHealth function.
/// 		| Cee Jay	| 10/06/16	|	- Added OnDeath callback for PlayerManager status check
/// 									- removed disableOnDeathBehaviours
/// 		| Cee Jay	| 17/06/16	|	- added shield support
/// 					  23/06/16	|	- added shooteridentifier for OnDestroy function
/// 					  27/06/16	|	- modified Hit & ReceiveDamage function which are taking <Weapon> as parameter
/// 		|Patrick	| 28/06/16  |   - added OnDirectionalDamageReceive action, which will be called when player takes damage.
///                                 |   - removed existing hit direction detection algorithm.
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

public class Destructible : NetworkBehaviour, IHittable, IDamagable, IHitByGrenade {

	[SyncVar]
	private bool isDead;

	[SerializeField]
	private int maxHealth = 100;

	[SyncVar(hook = "OnHealthChanged")]private int currentHealth;
	[SyncVar]private int currentShield;

	public Action<bool> OnDestroy;
	public Action<bool> OnGrenadeDestroy;
	public Action<bool,byte,Weapon,bool> OnDeath;
	public Action<bool,byte> OnGrenadeDeath;
	public Action OnShieldExpire;
	public Action<byte> OnDirectionalDamageReceive;

	[SerializeField]
	private List<Collider> colliders = new List<Collider>();

	[SerializeField]
	private Collider vitalCollider = null;

	//Other components
	private DamageIndicatorUIController dmgIndicatorUIController;
	private PlayerAnimationManager animationManager;

	public override void OnStartServer(){
		base.OnStartServer();

		SetDefaults();

		SetupCollider();

		Debug.Log ("Destructible start server");
	}

	public override void OnStartClient(){
		base.OnStartClient();

		Debug.Log ("Destructible start client");

	}

	void Start() {
		dmgIndicatorUIController = GameManager.instance.uiManager.dmgIndicatorUIController;
		animationManager = GetComponent<PlayerAnimationManager>();
		UpdateHealthUI (currentHealth, 1.0f);
	}

	void SetDefaults(){
		isDead = false;
		currentHealth = maxHealth;
	}

	void UpdateHealthUI(int _remainingHealth, float _healthRatio){
		if(isLocalPlayer)
			GameManager.instance.uiManager.playerStatusUIControl.playerStatusHealthUIControl.UpdateHealthUI (_remainingHealth.ToString(), _healthRatio);
	}

	private void SetupCollider(){
		Component[] _componentHolders;

		_componentHolders = GetComponentsInChildren<Collider>();

		colliders.Clear();

		foreach(Collider _c in _componentHolders){
			colliders.Add(_c);
		}

		for (int i = 0; i < colliders.Count; i++) {
			//remove vital & directional collider
			if(colliders[i] == vitalCollider){
				colliders.RemoveAt(i);
			}
		}
	}

	//add weapon and vital killed
	private void Destroyed(byte _shooterIdentifier, Weapon _weaponUsed, bool _isHeadShot){
		isDead = true;

		if(OnDeath != null){
			OnDeath(isDead,_shooterIdentifier,_weaponUsed,_isHeadShot);
		}
	}

	private void DestroyedByGrenade(byte _shooterIndentifier) {
		isDead = true;
		if(OnGrenadeDeath != null) {
			OnGrenadeDeath(isDead, _shooterIndentifier);
		}
	}
		
	private void OnHealthChanged(int healthChange){
		currentHealth = healthChange;
		UpdateHealthUI(currentHealth, (float)currentHealth / maxHealth);
	}

	public void Hit(Collider _collider, Bullet _bullet, bool _isServerProcess){
		Debug.Log("Hit By Bullet");
		bool _vitalShot = false;
		int _damageTaken = 0;

		//check for headshot 
		if(_collider == vitalCollider){
			_damageTaken = _bullet.currentWeapon.damage * 2;
			_vitalShot = true;
		}else{
			_damageTaken = _bullet.currentWeapon.damage;
			_vitalShot = false;
		}

		//Server process damage logic
		if(_isServerProcess) ReceiveDamage(_bullet.currentWeapon,_damageTaken,_vitalShot,_bullet.shooterIdentifier);


		//Directional hit detection (on local player only)
		if(!isLocalPlayer) return;

		else
		{
			OnDirectionalDamageReceive (_bullet.shooterIdentifier);
		}

//		if(Vector3.Dot(left,hitDir) > 0){
//			Debug.Log("Hit from left");
//		}else{
//			Debug.Log("Hit from right");
//		}
//
//		if(Vector3.Dot(front,hitDir) >0){
//			Debug.Log("Hit from front");
//		}else{
//			Debug.Log("Hit from back");
//		}
//
//		Debug.Log("Horizontal " + Vector3.Dot(left,hitDir));
//		Debug.Log("Vertical " + Vector3.Dot(front,hitDir));
	}

	public void GrenadeHit(Collider _collider, Grenade _grenade, bool _isServerProcess){
		Debug.Log("Taking Grenade Dmg");
		int _damageTaken = 0;

		//check for headshot 
		if(_collider == vitalCollider){
			_damageTaken = 90;
		}else{
			_damageTaken = 50;
		}

		//Server process damage logic
		if(_isServerProcess) ReceiveGrenadeDamage(GrenadeType.FragGrenade,_damageTaken, _grenade.shooterIdentifier);

		if(!isLocalPlayer) return;

		else OnDirectionalDamageReceive (_grenade.shooterIdentifier);
	}

	public void ReceiveDamage(Weapon _weaponUsed, int _damageTaken, bool _vitalShot, byte _shooterIdentifier){
		if(isDead){
			return;
		}

		//Deduct from shield if shield existed
		if (currentShield > 0) {
			int difference;

			//when shield can sustain the damage taken
			if (currentShield >= _damageTaken) {
				currentShield -= _damageTaken;

				// when shield breaks
			} else {
				difference = _damageTaken - currentShield;
				currentShield = 0;
				currentHealth -= difference;

				if (OnShieldExpire != null) {
					OnShieldExpire ();
				}
			}
			// if no shield found
		} else {
			currentHealth -= _damageTaken;
		}


		Debug.Log("Current Health : " + currentHealth);

		if(currentHealth <= 0){
			currentHealth = 0;
			// trigger dead (headshot killed / normal killed)
			if(OnDestroy != null){
				OnDestroy(_vitalShot);
			}
			Destroyed(_shooterIdentifier,_weaponUsed,_vitalShot);
		}
	}

	public void ReceiveGrenadeDamage(GrenadeType _grenade, int _damageTaken, byte _shooterIndentifier) {
		if(isDead) return;

		if(currentShield > 0) {
			int difference;

			if(currentShield >= _damageTaken) {
				currentShield -= _damageTaken;
			}

			else {
				difference = _damageTaken - currentShield;
				currentShield = 0;
				currentHealth -= difference;

				if (OnShieldExpire != null) {
					OnShieldExpire ();
				}
			}
		}

		else {
			currentHealth -= _damageTaken;
		}

		Debug.Log("Current Health : " + currentHealth);

		if(currentHealth <= 0){
			currentHealth = 0;	

			if(OnGrenadeDestroy != null){
				OnGrenadeDestroy(true);
			}

			DestroyedByGrenade(_shooterIndentifier);
		}
	}

	public void ReceiveHealth(int _healAmount)
	{
		currentHealth += _healAmount;

		if (currentHealth > maxHealth) 
		{
			currentHealth = maxHealth;
		}
	}

	public void ReceiveShield(int _shieldAmount){
		currentShield = _shieldAmount;
	}

	public void RemoveShield(){
		currentShield = 0;
	}

	public bool CheckIsDead(){
		return isDead;
	}
}
