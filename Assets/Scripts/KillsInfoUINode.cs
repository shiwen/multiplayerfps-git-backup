﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KillsInfoUINode : MonoBehaviour 
{
	public Text killerNameText;
	public Text dierNameText;
	public Image weaponImage;
	public GameObject headShootObject;
	
	public void Setup(string _killerName, string _dierName, Sprite _weaponSprite, bool _isHeadShoot, float _existTime)
	{
		float _totalContentLength = 0.0f;
		
		dierNameText.text = _dierName;
		dierNameText.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
		dierNameText.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (-_totalContentLength, 0.0f, 0.0f);

		_totalContentLength += dierNameText.GetComponent<RectTransform> ().sizeDelta.x;

		if (_isHeadShoot) 
		{
			headShootObject.SetActive (true);
			headShootObject.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (-_totalContentLength, 0.0f, 0.0f);
			_totalContentLength += headShootObject.GetComponent<RectTransform> ().sizeDelta.x;
		} 
		else 
		{
			headShootObject.SetActive (false);
		}

		weaponImage.sprite = _weaponSprite;
		weaponImage.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (-(_totalContentLength + weaponImage.GetComponent<RectTransform> ().sizeDelta.x), 0.0f, 0.0f);

		_totalContentLength += weaponImage.GetComponent<RectTransform> ().sizeDelta.x;

		killerNameText.text = _killerName;
		killerNameText.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
		killerNameText.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (-_totalContentLength, 0.0f, 0.0f);

		Invoke ("DestroyNode", _existTime);
	}

	public void SetupGrenadeKills(string _killerName, string _dierName, Sprite _grenadeSprite, float _existTime) 
	{
		float _totalContentLength = 0.0f;

		dierNameText.text = _dierName;
		dierNameText.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
		dierNameText.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (-_totalContentLength, 0.0f, 0.0f);

		_totalContentLength += dierNameText.GetComponent<RectTransform> ().sizeDelta.x;

		weaponImage.sprite = _grenadeSprite;
		weaponImage.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (-(_totalContentLength + weaponImage.GetComponent<RectTransform> ().sizeDelta.x), 0.0f, 0.0f);

		_totalContentLength += weaponImage.GetComponent<RectTransform> ().sizeDelta.x;

		killerNameText.text = _killerName;
		killerNameText.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
		killerNameText.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (-_totalContentLength, 0.0f, 0.0f);

		Invoke ("DestroyNode", _existTime);

	}

	public void DestroyNode()
	{
		GameObject.Destroy (this.gameObject);
	}
}
