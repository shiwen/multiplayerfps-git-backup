﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeathmatchEndGameUICandidate : MonoBehaviour 
{
	public Image nameBorder;
	public Image roleBorder;
	public Image killsCountBorder;
	public Image deathsCountBorder;
	
	public Text nameText;
	public Text roleText;
	public Text killsCountText;
	public Text deathsCountText;

	public void Setup(string _nameString, string _roleString, string _killsCountString, string _deathsCountString, Color _newBorderColor)
	{
		Setup (_nameString, _roleString, _killsCountString, _deathsCountString);

		nameBorder.color = new Color (_newBorderColor.r, _newBorderColor.g, _newBorderColor.b, nameBorder.color.a);
		roleBorder.color = new Color (_newBorderColor.r, _newBorderColor.g, _newBorderColor.b, roleBorder.color.a);
		killsCountBorder.color = new Color (_newBorderColor.r, _newBorderColor.g, _newBorderColor.b, killsCountBorder.color.a);
		deathsCountBorder.color = new Color (_newBorderColor.r, _newBorderColor.g, _newBorderColor.b, deathsCountBorder.color.a);
	}

	public void Setup(string _nameString, string _roleString, string _killsCountString, string _deathsCountString)
	{
		nameText.text = _nameString;
		roleText.text = _roleString;
		killsCountText.text = _killsCountString;
		deathsCountText.text = _deathsCountString;
	}
}
