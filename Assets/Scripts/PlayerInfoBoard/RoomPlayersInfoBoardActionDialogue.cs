﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;

public class RoomPlayersInfoBoardActionDialogue : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Vector3 showDialogueOffset;
	Action kickButtonClickedFunction;
	
	public void Setup(Action _kickButtonClickedFunction)
	{
		kickButtonClickedFunction = _kickButtonClickedFunction;
	}
	
	public void Show(Vector3 _position)
	{
		transform.GetComponent<RectTransform>().anchoredPosition = _position + showDialogueOffset;
		gameObject.SetActive (true);
	}

	public void Hide()
	{
		gameObject.SetActive (false);
	}

	public void KickButtonClicked()
	{
		kickButtonClickedFunction ();
		Hide ();
	}

	public void OnPointerEnter(PointerEventData _data)
	{
	}

	public void OnPointerExit(PointerEventData _data)
	{
		Hide ();
	}

	public void Dummy()
	{
		Hide ();
	}
}
