﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class RoomPlayersInfoBoardControl : MonoBehaviour 
{
	//prefabs
	public GameObject infoObjectPrefab;

	//references
	public RoomPlayersInfoBoardActionDialogue actionDialogue;
	public GameObject infoObjectsHolder;

	//properties
	public Dictionary<byte, RoomPlayersInfoBoardInfoObject> generatedInfoObjects;
	public float infoObjectHeight;

	private GameManager gameManager;
	Action<byte> kickPlayerFunction;
	private byte currentActionDialogueTargetedIdentifier;
	private float canvasScaleFactor;

	void Awake()
	{
		generatedInfoObjects = new Dictionary<byte, RoomPlayersInfoBoardInfoObject> ();
		gameManager = GameManager.instance;
	}

	public void Setup(Action<byte> _actionDialogueKickFunction, float _canvasScaleFactor)
	{
		kickPlayerFunction = _actionDialogueKickFunction;
		actionDialogue.Setup (ActionDialogueKickButtonClicked);
		canvasScaleFactor = _canvasScaleFactor;
	}

	public void Show()
	{
		gameObject.SetActive (true);
		RetreiveCurrentRoomPlayersInfo ();
		InvokeRepeating ("RetreiveCurrentRoomPlayersInfo", 0, 0.5f);	
	}

	public void Hide()
	{
		gameObject.SetActive (false);
		CancelInvoke ();
	}

	private void RetreiveCurrentRoomPlayersInfo()
	{
		Dictionary<byte, RoomPlayerInfo> _playerInfos = gameManager.GetRoomPlayerInfos ();
		List<byte> _playerInfoToBeRemove = new List<byte>(generatedInfoObjects.Keys);

		foreach (KeyValuePair<byte, RoomPlayerInfo> _entry in _playerInfos) 
		{
			_playerInfoToBeRemove.Remove (_entry.Key);

			if (generatedInfoObjects.ContainsKey (_entry.Key)) 
			{
				generatedInfoObjects [_entry.Key].SetStatus (ConvertPlayerStateAndRoleToString (_entry.Value.playerState, _entry.Value.playerRole));
				generatedInfoObjects [_entry.Key].SetKillsCountText (_entry.Value.killsCount.ToString ());
				generatedInfoObjects [_entry.Key].SetDeathsCountText (_entry.Value.deathsCount.ToString ());
			} 
			else 
			{
				bool _isSelf = (_entry.Key == NetworkConnectionManager.instance.GetLocalPlayer().identifier);

				AddNewRoomPlayerInfoObject (_entry.Key, _entry.Value.playerName, ConvertPlayerStateAndRoleToString (_entry.Value.playerState, _entry.Value.playerRole), "0", "0", _isSelf);
			}
		}

		foreach (byte _candidate in _playerInfoToBeRemove) 
		{
			RemoveRoomPlayerInfoObject (_candidate);
		}
	}

	private void AddNewRoomPlayerInfoObject(byte _identifier, string _playerName, string _status, string _killsCount, string _deathsCount, bool _isSelf)
	{
		RoomPlayersInfoBoardInfoObject _infoObject = GameObject.Instantiate (infoObjectPrefab).GetComponent<RoomPlayersInfoBoardInfoObject>();
		_infoObject.transform.SetParent (infoObjectsHolder.transform);
		_infoObject.transform.localPosition = new Vector3 (0, 0, 0);
		_infoObject.transform.localScale = new Vector3 (1, 1, 1);

		_infoObject.Setup (_identifier, _playerName, _status, _killsCount, _deathsCount, _isSelf, CallActionDialogue);


		generatedInfoObjects.Add (_identifier, _infoObject);
		RepositionInfoObjects ();
	}

	private void RemoveRoomPlayerInfoObject(byte _identifier)
	{
		GameObject _gameObject = generatedInfoObjects [_identifier].gameObject;
		generatedInfoObjects.Remove (_identifier);
		GameObject.Destroy(_gameObject);
		RepositionInfoObjects ();
	}

	private void RepositionInfoObjects()
	{
		List<byte> _sortedIdentifier = new List<byte> (generatedInfoObjects.Keys);
		int _count = 0;

		_sortedIdentifier.Sort ();

		foreach (byte _candidate in _sortedIdentifier) 
		{
			generatedInfoObjects [_candidate].transform.localPosition = new Vector3 (0, -infoObjectHeight * _count, 0);
			_count++;
		}
	}

	private string ConvertPlayerStateAndRoleToString(GameplayPlayerState _playerState, PlayerRole _playerRole)
	{
		string _result = "";
		
		if (_playerState == GameplayPlayerState.InLobby)
			_result = "In lobby";
		else 
		{
			if (_playerState == GameplayPlayerState.SelectingRole)
				_result = "Selecting role";
			else 
			{
				_result = _playerRole.ToString ();
			}
		}

		return _result;
	}

	private void CallActionDialogue(byte _targetedPlayerIndentifier, Vector3 _callPosition)
	{
		currentActionDialogueTargetedIdentifier = _targetedPlayerIndentifier;
		actionDialogue.Show ((_callPosition - transform.position) / canvasScaleFactor);
	}

	private void ActionDialogueKickButtonClicked()
	{
		kickPlayerFunction (currentActionDialogueTargetedIdentifier);
	}
}
