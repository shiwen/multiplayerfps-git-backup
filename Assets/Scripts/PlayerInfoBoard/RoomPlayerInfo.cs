﻿using UnityEngine;
using System.Collections;

public class RoomPlayerInfo 
{
	public string playerName;
	public GameplayPlayerState playerState;
	public PlayerRole playerRole;
	public int killsCount;
	public int deathsCount;

	public RoomPlayerInfo(string _playerName, GameplayPlayerState _playerState, PlayerRole _playerRole)
	{
		playerName = _playerName;
		playerState = _playerState;
		playerRole = _playerRole;
		killsCount = 0;
		deathsCount = 0;
	}

	public RoomPlayerInfo()
	{
	}
}
