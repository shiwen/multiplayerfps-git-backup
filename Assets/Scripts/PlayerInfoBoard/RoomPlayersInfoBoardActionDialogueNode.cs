﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RoomPlayersInfoBoardActionDialogueNode : Selectable, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
	public Color normalColor;
	public Color hoverColor;
	
	Action clickedFunction;
	
	public void Setup(Action _clickedFunction)
	{
		clickedFunction = _clickedFunction;
	}

	public override void OnPointerEnter(PointerEventData _data)
	{
		base.OnPointerEnter (_data);
		base.OnPointerEnter (_data);
	}

	public override void OnPointerUp(PointerEventData _data)
	{
		clickedFunction ();
	}
}
