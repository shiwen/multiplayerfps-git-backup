﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class RoomPlayersInfoBoardInfoObject : MonoBehaviour, IPointerDownHandler
{
	public CanvasGroup canvasGroup;
	public RectTransform playerNameSection;
	public RectTransform statusTextSection;
	public float playerNameSectionToStatusTextSectionDistance;
	public Text playerNameText;
	public Text statusText;
	public Text killsCountText;
	public Text deathsCountText;

	public float selfCanvasGroupAlpha;
	public float nonSelfCanvasGroupAlpha;
	private Action<byte, Vector3> actionDialogueCallFunction;

	private byte playerIdentifier;

	public void Setup(byte _playerIdentifier, string _playerName, string _statusText, string _killsCountText, string _deathsCountText, bool _isSelf, Action<byte, Vector3> _actionDialogueCall)
	{
		playerIdentifier = _playerIdentifier;
		playerNameText.text = _playerName;
		statusText.text = _statusText;
		killsCountText.text = _killsCountText;
		deathsCountText.text = _deathsCountText;

		if (_isSelf)
			canvasGroup.alpha = selfCanvasGroupAlpha;
		else
			canvasGroup.alpha = nonSelfCanvasGroupAlpha;

		RectTransform _rectTransform = GetComponent<RectTransform> ();
		_rectTransform.offsetMin = new Vector2 (0, _rectTransform.offsetMin.y);
		_rectTransform.offsetMax = new Vector2 (0, _rectTransform.offsetMax.y);
		playerNameSection.sizeDelta = new Vector2 (GetComponent<RectTransform> ().rect.width + statusTextSection.anchoredPosition.x - playerNameSection.anchoredPosition.x - playerNameSectionToStatusTextSectionDistance,
													playerNameSection.sizeDelta.y);

		actionDialogueCallFunction += _actionDialogueCall;
	}

	public void SetStatus(string _statusText)
	{
		statusText.text = _statusText;
	}

	public void SetKillsCountText(string _text)
	{
		killsCountText.text = _text;
	}

	public void SetDeathsCountText(string _text)
	{
		deathsCountText.text = _text;
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (eventData.pointerId == -2) 
		{
			actionDialogueCallFunction (playerIdentifier, eventData.pressPosition);
		}
	}
}
