﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HealthIndicatorUIController : MonoBehaviour {

	public Image healthImage;
	public float startTriggerThreshold;
	public float floatingRange;
	public float floatingRate;

	private float currentRatio = 0.0f;
	private int floatingDirection = 1;
	private bool floating = false;
	private float currentFloatingValue;

	private float tempValue = 1.0f;

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.T)) {
			tempValue -= 0.1f;
			UpdateHealthUI (tempValue);
		} if(Input.GetKeyDown(KeyCode.Y)) {
			tempValue += 0.1f;
			UpdateHealthUI (tempValue);
		}

		float _activeRatio = currentRatio;

		if (floating) 
		{
			if (Mathf.Abs (currentFloatingValue) > (floatingRange / 2)) 
			{
				currentFloatingValue = (floatingRange / 2) * floatingDirection;
				floatingDirection = -floatingDirection;
			} 
			else 
			{
				currentFloatingValue += floatingRate * Time.deltaTime * floatingDirection;
			}
		}

		Color _originColor = healthImage.color;
		healthImage.color = new Color (_originColor.r, _originColor.g, _originColor.b, _activeRatio + currentFloatingValue);
	}

	public void UpdateHealthUI(float _currentHealthRatio) 
	{
		currentRatio = (startTriggerThreshold - _currentHealthRatio) / startTriggerThreshold;

		if (_currentHealthRatio < startTriggerThreshold)
			floating = true;
		else 
		{
			if (floating) 
			{
				floating = false;
				currentFloatingValue = 0.0f;
			}
		}

		if (currentRatio < 0.0f)
			currentRatio = 0.0f;
		else if (currentRatio > 1.0f)
			currentRatio = 1.0f;
	}
}
