﻿using UnityEngine;
using System;
using System.Collections;

public abstract class EndGameConditionChecker
{
	private Action EndGameAction;

	protected abstract void SetConditions(params object[] _parameters);
	public abstract void UpdateCondition (params object[] _conditionParameters);

	public void InitializeEndGameCondition(Action _endGameAction, params object[] _conditionParameters)
	{
		EndGameAction = _endGameAction;
		SetConditions (_conditionParameters);
	}

	protected void EndGame()
	{
		EndGameAction ();
	}
}

public class DeathmatchEndGameConditionChecker : EndGameConditionChecker
{
	private int killsCountToEndGame;

	protected override void SetConditions (params object[] _parameters)
	{
		killsCountToEndGame = (int)_parameters [0];
	}
	
	public override void UpdateCondition (params object[] _conditionParameters)
	{
		if ((int)_conditionParameters [0] >= killsCountToEndGame) 
		{
			EndGame ();
		}
	}
}