﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DamageIndicatorUIController : MonoBehaviour {

	public GameObject damageIndicator;
	[SerializeField] private List<GameObject> damageIndicatorList = new List<GameObject>();
	public float indicatorDuration = 3.0f;

	void Awake () {
		for (int i = 0; i < damageIndicator.transform.childCount; i++) {
			damageIndicatorList.Add(damageIndicator.transform.GetChild(i).gameObject);
		}
	}

	private void PlayerHit(){
		ShowDamageIndicator();
		CancelInvoke("HideDamageIndicator");
		Invoke("HideDamageIndicator", indicatorDuration);
	}

	private void ShowDamageIndicator(){
		damageIndicator.SetActive(true);
	}

	private void HideDamageIndicator(){
		damageIndicator.SetActive(false);
	}

	public void DisplayDmgIndicator(string _direction) {

		switch ( _direction) { 
		case "Front" : 
			StartCoroutine(alphaDelay(1f, 0));
			break;

		case "Front Left" : 
			StartCoroutine(alphaDelay(1f, 1));
			break;

		case "Front Right" : 
			StartCoroutine(alphaDelay(1f, 2));
			break;

		case "Left" : 
			StartCoroutine(alphaDelay(1f, 3));
			break;

		case "Right" : 
			StartCoroutine(alphaDelay(1f, 4));
			break;

		case "Back" :
			StartCoroutine(alphaDelay(1f, 5));
			break;

		case "Back Left" :
			StartCoroutine(alphaDelay(1f, 6));
			break;

		case "Back Right" : 
			StartCoroutine(alphaDelay(1f, 7));
			break;
		}
	}

	public IEnumerator alphaDelay(float _duration, int _index) {
		Color transparent = new Color(1,1,1,0);
		Color opaque = new Color(1,1,1,1);
		float elapsedTime = 0;

		while(elapsedTime < _duration) {
			damageIndicatorList[_index].GetComponent<Image>().color = Color.Lerp(transparent, opaque, (elapsedTime/_duration));
			damageIndicatorList[_index].GetComponent<Image>().color = Color.Lerp(opaque, transparent, (elapsedTime/_duration));
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
	}
}
