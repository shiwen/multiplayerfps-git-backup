﻿using UnityEngine;
using System.Collections;

public class PropCollider : MonoBehaviour, IHittable {

	[SerializeField]
	private GameObject bulletHolePrefab = null;

	public void Hit(Collider _collider, Bullet _bullet, bool _isServerProcess){
		if(bulletHolePrefab == null){
			return;
		}

		Instantiate(bulletHolePrefab,_bullet.hit_point,_bullet.hit_normal != Vector3.zero ? Quaternion.LookRotation(_bullet.hit_normal) : Quaternion.identity);
	}
}
