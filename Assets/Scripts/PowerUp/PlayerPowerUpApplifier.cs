﻿using UnityEngine;
using System.Collections;

public class PlayerPowerUpApplifier : PowerUpApplier
{
	private PlayerManager playerManager;
	private int shieldPowerUpIdentifier;

	protected override void Initialize()
	{
		playerManager = GetComponent<PlayerManager> ();
	}
	
	protected override void AffectMovementSpeed (float _affectRate)
	{
		
	}

	protected override void AffectDamageReceive (float _affectRate)
	{
		
	}

	protected override void AffectWeaponDamage (float _affectRate)
	{
		
	}

	protected override void ToggleGunDisable (bool _on)
	{
			
	}

	protected override void ToggleStun (bool _on)
	{
			
	}

	protected override void ToggleImmobile(bool _on)
	{
		
	}

	protected override void ToggleDamageAbsorbShield (bool _on, float _damageThreshold, int _powerUpIdentifier)
	{
		if (_on) 
		{
			shieldPowerUpIdentifier = _powerUpIdentifier;
			playerManager.ApplyShield ((int)_damageThreshold);
		} 
		else 
		{
			playerManager.RemoveShield ();
		}
	}

	public void DestroyShield()
	{
		MutatePowerUp (shieldPowerUpIdentifier);
	}
}
