﻿using UnityEngine;
using System.Collections;

public abstract class PowerUpMutationChecker : MonoBehaviour 
{
	PowerUpApplier listener;
	int powerUpIdentifier;

	public void SetUp(PowerUpApplier _listener, int _powerUpIdentifier)
	{
		listener = _listener;
		powerUpIdentifier = _powerUpIdentifier;
	}
	
	protected void ApplyMutate(float _mutateRate)
	{
		listener.ApplyMutateOnPowerUp (powerUpIdentifier, _mutateRate);
	}
}
