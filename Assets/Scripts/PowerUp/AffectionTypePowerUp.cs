﻿using UnityEngine;
using System.Collections;

public class AffectionTypePowerUp : PowerUp 
{
	public PowerUpAffection affection;
	public float affectionRate;

	public AffectionTypePowerUp(int _identifier, PowerUpAffection _affection, float _affectionRate, float _duration, string _prefabPath, PowerUpMutation _powerUpMutation) : 
						  base (_identifier, _duration, PowerUpCategory.PowerUpAffection, Mathf.Abs (_affectionRate), _prefabPath, _powerUpMutation)
	{
		affection = _affection;
		affectionRate = _affectionRate;
	}

	public override void ExecutePowerUp ()
	{
		if (powerUpMutation.mutationType != PowerUpMutationType.NONE) 
		{
			listener.ApplyAffection (affection, affectionRate, powerUpMutation.mutationThreshold, identifier);
		} 
		else 
		{
			listener.ApplyAffection (affection, affectionRate);
		}
	}

	public override void RemovePowerUp()
	{
		listener.ApplyAffection (affection, -affectionRate);
	}
}
