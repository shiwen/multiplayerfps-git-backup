﻿using UnityEngine;
using System.Collections;

public class ToggleTypePowerUp : PowerUp 
{
	public PowerUpToggle toggleType;

	public ToggleTypePowerUp(int _identifier, PowerUpToggle _toggleType, float _duration, string _prefabPath, PowerUpMutation _powerUpMutation) : 
					   base (_identifier, _duration, PowerUpCategory.PowerUpToggle, 1, _prefabPath, _powerUpMutation)
	{
		toggleType = _toggleType;
	}

	public override void ExecutePowerUp ()
	{
		if (powerUpMutation.mutationType != PowerUpMutationType.NONE) 
		{
			listener.ApplyToggle (toggleType, true, powerUpMutation.mutationThreshold, identifier);
		} 
		else 
		{
			listener.ApplyToggle (toggleType, true);
		}
	}

	public override void RemovePowerUp()
	{
		listener.ApplyToggle (toggleType, false);
	}
}
