﻿using System;
using UnityEngine;
using System.Collections;

[System.Serializable]
public abstract class PowerUp : ICloneable
{
	public int identifier;
	public PowerUpCategory category;
	public float strengthness;
	public float duration;
	public string prefabPath;
	public PowerUpMutation powerUpMutation;

	protected PowerUpApplier listener;

	public PowerUp(int _identifier, float _duration, PowerUpCategory _category, float _strengthness, string _prefabPath, PowerUpMutation _powerUpMutation)
	{
		identifier = _identifier;
		duration = _duration;
		category = _category;
		strengthness = _strengthness;
		prefabPath = _prefabPath;
		powerUpMutation = _powerUpMutation;
	}

	public object Clone()
	{
		return this.MemberwiseClone ();
	}

	public abstract void ExecutePowerUp ();

	public abstract void RemovePowerUp ();

	public void SetListener(PowerUpApplier _listener)
	{
		listener = _listener;
	}
}
