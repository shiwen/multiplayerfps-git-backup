﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PowerUpCategory
{
	PowerUpAffection,
	PowerUpToggle
}

public enum PowerUpAffection
{
	MOVEMENT_SPEED,
	DAMAGE_OUTPUT,
	DAMAGE_RECEIVE,
}

public enum PowerUpToggle
{
	STUN,
	GUN_DISABLE,
	IMMOBILE,
	DAMAGE_ABSORB_BARRIER
}

public enum PowerUpMutationType
{
	NONE,
	DISABLE
}

public class PowerUpMutation
{
	public PowerUpMutationType mutationType;
	public float mutationThreshold;
	public System.Type mutationCheckerType;

	public PowerUpMutation(PowerUpMutationType _mutationType, float _mutationThreshold, System.Type _mutationCheckerType)
	{
		mutationType = _mutationType;
		mutationThreshold = _mutationThreshold;

		if (_mutationCheckerType.IsSubclassOf (typeof(PowerUpMutationChecker)))
			mutationCheckerType = _mutationCheckerType;
		else
			Debug.LogError ("Assigned invalid mutation checker!");
	}

	public PowerUpMutation(PowerUpMutationType _mutationType)
	{
		mutationType = _mutationType;
		mutationThreshold = 0.0f;;
	}
}

public class PowerUpDatabase 
{
	public static readonly PowerUpDatabase instance = new PowerUpDatabase();

	private List<PowerUp> powerUpList;

	public void Init()
	{
		powerUpList = new List<PowerUp> ();

		powerUpList.Add(new AffectionTypePowerUp(0, PowerUpAffection.MOVEMENT_SPEED, 0.3f, 20.0f, "", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new AffectionTypePowerUp(1, PowerUpAffection.MOVEMENT_SPEED, 0.6f, 10.0f, "", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new AffectionTypePowerUp(2, PowerUpAffection.MOVEMENT_SPEED, -0.3f, 20.0f, "", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new AffectionTypePowerUp(3, PowerUpAffection.MOVEMENT_SPEED, -0.6f, 10.0f, "", new PowerUpMutation(PowerUpMutationType.NONE)));

		powerUpList.Add(new AffectionTypePowerUp(4, PowerUpAffection.DAMAGE_OUTPUT, 0.3f, 20.0f, "Prefabs/PowerUp/powerup_weapondamageamplifier", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new AffectionTypePowerUp(5, PowerUpAffection.DAMAGE_OUTPUT, 0.6f, 10.0f, "Prefabs/PowerUp/powerup_weapondamageamplifier", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new AffectionTypePowerUp(6, PowerUpAffection.DAMAGE_OUTPUT, -0.3f, 20.0f, "", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new AffectionTypePowerUp(7, PowerUpAffection.DAMAGE_OUTPUT, -0.6f, 10.0f, "", new PowerUpMutation(PowerUpMutationType.NONE)));

		powerUpList.Add(new AffectionTypePowerUp(8, PowerUpAffection.DAMAGE_RECEIVE, 0.3f, 20.0f, "", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new AffectionTypePowerUp(9, PowerUpAffection.DAMAGE_RECEIVE, 0.6f, 10.0f, "", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new AffectionTypePowerUp(10, PowerUpAffection.DAMAGE_RECEIVE, -0.3f, 20.0f, "Prefabs/PowerUp/powerup_damagereduction", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new AffectionTypePowerUp(11, PowerUpAffection.DAMAGE_RECEIVE, -0.6f, 10.0f, "Prefabs/PowerUp/powerup_damagereduction", new PowerUpMutation(PowerUpMutationType.NONE)));

		powerUpList.Add(new ToggleTypePowerUp(12, PowerUpToggle.STUN, 4.0f, "Prefabs/PowerUp/powerup_stunned", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new ToggleTypePowerUp(15, PowerUpToggle.STUN, 7.0f, "Prefabs/PowerUp/powerup_stunned", new PowerUpMutation(PowerUpMutationType.NONE)));
		powerUpList.Add(new ToggleTypePowerUp(13, PowerUpToggle.GUN_DISABLE, 6.0f, "", new PowerUpMutation(PowerUpMutationType.NONE)));

		powerUpList.Add(new ToggleTypePowerUp(14, PowerUpToggle.DAMAGE_ABSORB_BARRIER, 30.0f, "Prefabs/PowerUp/powerup_damageabsorbbarrier", new PowerUpMutation(PowerUpMutationType.DISABLE, 50.0f, typeof(PowerUpDamageAbsorberMutationChecker))));
	}

	public PowerUp GetPowerUp(int _identifier)
	{
		foreach (PowerUp _candidate in powerUpList) 
		{
			if (_candidate.identifier == _identifier)
				return (PowerUp)_candidate.Clone();
		}

		return null;
	}
}
