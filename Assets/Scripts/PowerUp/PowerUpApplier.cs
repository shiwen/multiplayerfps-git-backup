﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class AppliedPowerUp
{
	public PowerUp powerUp;
	public float elapsedTimeCounter;
	public float powerUpRemainingTime
	{
		get 
		{
			return powerUp.duration - elapsedTimeCounter;
		}
	}

	public AppliedPowerUp(PowerUp _powerUp, PowerUpApplier _listener)
	{
		powerUp = _powerUp;
		powerUp.SetListener (_listener);
		elapsedTimeCounter = 0.0f;
	}

	public void ReplacePowerUp(PowerUp _powerUp, PowerUpApplier _listener)
	{
		powerUp = _powerUp;
		powerUp.SetListener (_listener);
		elapsedTimeCounter = 0.0f;
	}
}

public abstract class PowerUpApplier : NetworkBehaviour 
{
	private List<AppliedPowerUp> appliedPowerUps = new List<AppliedPowerUp>();
	private Dictionary<PowerUpAffection, float > currentAffectionRate = new Dictionary<PowerUpAffection, float> ();
	private Dictionary<int, GameObject> powerUpsObject = new Dictionary<int, GameObject>();

	protected abstract void Initialize();

	public void Awake()
	{
		foreach (PowerUpAffection _candidate in System.Enum.GetValues(typeof(PowerUpAffection))) 
		{
			currentAffectionRate.Add (_candidate, 1.0f);
		}

		Initialize ();
	}

	void Update()
	{
		for (int i = 0; i < appliedPowerUps.Count; i++) 
		{
			if (appliedPowerUps[i].elapsedTimeCounter >= appliedPowerUps[i].powerUp.duration) 
			{
				RemovePowerUp(appliedPowerUps[i].powerUp.identifier, true);
				//appliedPowerUps.RemoveAt (i);
				//i--;
			}
			else 
			{
				appliedPowerUps[i].elapsedTimeCounter += Time.deltaTime;
			}
		}

		if (Input.GetKeyDown (KeyCode.V))
			ApplyPowerUp (12);
		else if (Input.GetKeyDown (KeyCode.B))
			ApplyPowerUp (15);
		else if (Input.GetKeyDown (KeyCode.N))
			ApplyPowerUp (4);
		else if (Input.GetKeyDown (KeyCode.M))
			ApplyPowerUp (10);
	}

	public void ApplyPowerUp(int _powerUpIdentifier)
	{
		if(isServer)
			RpcApplyPowerUp (_powerUpIdentifier);
	}

	public void ApplyMutateOnPowerUp(int _targetPowerupIdentifier, float _mutateRate)
	{
		AppliedPowerUp _targetedPowerUp = PowerUpIdentifierExisted (_targetPowerupIdentifier);
		PowerUpMutation _mutation = _targetedPowerUp.powerUp.powerUpMutation;

		if (_mutation.mutationType != PowerUpMutationType.NONE) 
		{
			_mutation.mutationThreshold -= _mutateRate;

			if (_mutation.mutationThreshold <= 0) 
			{
				switch (_mutation.mutationType) 
				{
				case PowerUpMutationType.DISABLE:
					RemovePowerUp (_targetedPowerUp.powerUp.identifier, true);
					break;
				}
			}
		}
	}

	public void MutatePowerUp(int _targetPowerupIdentifier)
	{
		AppliedPowerUp _targetedPowerUp = PowerUpIdentifierExisted (_targetPowerupIdentifier);
		PowerUpMutation _mutation = _targetedPowerUp.powerUp.powerUpMutation;

		if (_mutation.mutationType != PowerUpMutationType.NONE) 
		{
			switch (_mutation.mutationType) 
			{
			case PowerUpMutationType.DISABLE:
				RemovePowerUp (_targetedPowerUp.powerUp.identifier, true);
				break;
			}
		}
	}

	private AppliedPowerUp PowerUpIdentifierExisted(int _identifier)
	{
		foreach (AppliedPowerUp _candidate in appliedPowerUps) 
		{
			if(_candidate.powerUp.identifier == _identifier)
			{
				return _candidate;
			}
		}

		return null;
	}

	private AppliedPowerUp PowerUpToggleTypeExisted(PowerUpToggle _toggleType)
	{
		foreach (AppliedPowerUp _candidate in appliedPowerUps) 
		{
			if (_candidate.powerUp.category == PowerUpCategory.PowerUpToggle) 
			{
				ToggleTypePowerUp _toggleTypePowerUp = (ToggleTypePowerUp)_candidate.powerUp;

				if (_toggleTypePowerUp.toggleType == _toggleType) 
				{
					return _candidate;
				}
			}
		}

		return null;
	}

	private void ExecutePowerUp(PowerUp _powerUp)
	{
		_powerUp.ExecutePowerUp ();
	
		GameObject _prefabSoruce = (GameObject)Resources.Load (_powerUp.prefabPath);

		if (_prefabSoruce != null) 
		{
			GameObject _powerUpObject = (GameObject)GameObject.Instantiate (_prefabSoruce);
			powerUpsObject.Add (_powerUp.identifier, _powerUpObject);
			_powerUpObject.transform.SetParent (this.transform);
			_powerUpObject.transform.localPosition = new Vector3 (0, 0, 0);
			_powerUpObject.transform.localRotation = Quaternion.Euler (new Vector3 (0, 0, 0));
			_powerUpObject.gameObject.layer = gameObject.layer;

			if (_powerUp.powerUpMutation.mutationType != PowerUpMutationType.NONE) 
			{
				_powerUpObject.AddComponent(_powerUp.powerUpMutation.mutationCheckerType);
				_powerUpObject.GetComponent<PowerUpMutationChecker> ().SetUp (this, _powerUp.identifier);
			}
		} 
		else 
		{
			Debug.LogError ("Power-up prefab missing! : " + _powerUp.prefabPath);
		}
	}

	private void RemovePowerUp(int _powerUpIdentifier, bool _removeFromList)
	{
		if (isServer) 
		{
			RpcRemovePowerUp (_powerUpIdentifier, _removeFromList);
		}
	}

	private void ReplacePowerUp(AppliedPowerUp _replacee, PowerUp _replacer)
	{
		LocalRemovePowerUp (_replacee.powerUp.identifier, false);
		_replacee.ReplacePowerUp (_replacer, this);
		ExecutePowerUp (_replacer);
	}

	private void LocalRemovePowerUp(int _powerUpIdentifier, bool _removeFromList)
	{
		AppliedPowerUp _appliedPowerUp = PowerUpIdentifierExisted (_powerUpIdentifier);

		if (_appliedPowerUp != null) 
		{
			_appliedPowerUp.powerUp.RemovePowerUp ();

			if (_removeFromList) 
			{
				appliedPowerUps.Remove (_appliedPowerUp);
			}

			if (powerUpsObject.ContainsKey (_appliedPowerUp.powerUp.identifier)) 
			{
				GameObject _powerUpObject = powerUpsObject [_appliedPowerUp.powerUp.identifier];
				powerUpsObject.Remove (_appliedPowerUp.powerUp.identifier);
				GameObject.Destroy (_powerUpObject);
			}
		} 
		else
			Debug.LogError ("Power Up Missing!");
	}

	#region rpcs

	[ClientRpc]
	private void RpcApplyPowerUp(int _powerUpIdentifier)
	{
		PowerUp _powerUp = PowerUpDatabase.instance.GetPowerUp (_powerUpIdentifier);
		AppliedPowerUp _existedPowerUp = null;

		if (_powerUp.category == PowerUpCategory.PowerUpAffection) 
		{
			_existedPowerUp = PowerUpIdentifierExisted (_powerUp.identifier);
		}
		else if(_powerUp.category == PowerUpCategory.PowerUpToggle)
		{
			_existedPowerUp = PowerUpToggleTypeExisted(((ToggleTypePowerUp)_powerUp).toggleType);
		}

		if (_existedPowerUp != null) //if powerup already existed
		{
			if (_powerUp.strengthness > _existedPowerUp.powerUp.strengthness)
			{
				ReplacePowerUp (_existedPowerUp, _powerUp);
			}
			else if(_powerUp.strengthness == _existedPowerUp.powerUp.strengthness && _powerUp.duration > _existedPowerUp.powerUpRemainingTime)
			{
				ReplacePowerUp (_existedPowerUp, _powerUp);
			}
		} 
		else 
		{
			appliedPowerUps.Add (new AppliedPowerUp (_powerUp, this));
			ExecutePowerUp (_powerUp);
		}
	}

	[ClientRpc]
	private void RpcRemovePowerUp(int _powerUpIdentifier, bool _removeFromList)
	{
		LocalRemovePowerUp (_powerUpIdentifier, _removeFromList);
	}

	#endregion

	#region listener

	protected abstract void AffectMovementSpeed (float _affectRate);
	protected abstract void AffectWeaponDamage(float _affectRate);
	protected abstract void AffectDamageReceive(float _affectRate);

	public void ApplyAffection(PowerUpAffection _affectionType, float _affectionValue)
	{
		ApplyAffection (_affectionType, _affectionValue, 0.0f, -1);
	}

	public void ApplyAffection(PowerUpAffection _affectionType, float _affectionValue, float _mutationThreshold, int _mutationBindedIdentifier)
	{
		currentAffectionRate [_affectionType] += _affectionValue;
		float _currentAffectionRate = currentAffectionRate [_affectionType];

		switch (_affectionType) 
		{
		case PowerUpAffection.MOVEMENT_SPEED:
			AffectMovementSpeed (_currentAffectionRate);
			break;
		case PowerUpAffection.DAMAGE_OUTPUT:
			AffectWeaponDamage (_currentAffectionRate);
			break;
		case PowerUpAffection.DAMAGE_RECEIVE:
			AffectDamageReceive (_currentAffectionRate);
			break;
		}
	}

	protected abstract void ToggleStun (bool _on);
	protected abstract void ToggleGunDisable(bool _on);
	protected abstract void ToggleImmobile (bool _on);
	protected abstract void ToggleDamageAbsorbShield (bool _on, float _damageThreshold, int _powerUpIdentifier);

	public void ApplyToggle(PowerUpToggle _toggleType, bool _on)
	{
		ApplyToggle (_toggleType, _on, 0.0f, -1);
	}

	public void ApplyToggle(PowerUpToggle _toggleType, bool _on, float _mutationThreshold, int _mutationBindedIdentifier)
	{
		switch (_toggleType) 
		{
		case PowerUpToggle.STUN:
			ToggleStun (_on);
			break;
		case PowerUpToggle.GUN_DISABLE:
			ToggleGunDisable (_on);
			break;
		case PowerUpToggle.IMMOBILE:
			ToggleImmobile (_on);
			break;
		case PowerUpToggle.DAMAGE_ABSORB_BARRIER:
			ToggleDamageAbsorbShield (_on, _mutationThreshold, _mutationBindedIdentifier);
			break;
		}
	}

	#endregion
}
