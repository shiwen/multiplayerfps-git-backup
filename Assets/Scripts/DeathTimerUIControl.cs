﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeathTimerUIControl : MonoBehaviour 
{
	public Text timerText;
	public string textFormat;
	public string timerColorCode;

	private float timerCounter;

	public void StartDeathTimer(float timer)
	{
		timerCounter = timer;
		gameObject.SetActive (true);
	}

	void Update()
	{
		timerText.text = string.Format(textFormat, "<color=" + timerColorCode + ">" + ((int)timerCounter).ToString() + "</color>");
		
		if (timerCounter <= 0) 
		{
			gameObject.SetActive (false);
		}

		timerCounter -= Time.deltaTime;
	}
}
