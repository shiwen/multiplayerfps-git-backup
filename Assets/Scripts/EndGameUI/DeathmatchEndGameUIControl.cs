﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeathmatchEndGameUIControl : MonoBehaviour 
{
	public class UIParameter
	{
		public string playerName;
		public string playerRole;
		public int killsCount;
		public int deathsCount;

		public UIParameter(string _playerName, string _playerRole, int _killsCount, int _deathsCount)
		{
			playerName = _playerName;
			playerRole = _playerRole;
			killsCount = _killsCount;
			deathsCount = _deathsCount;
		}
	}

	public GameObject candidateSample;
	public Color selfObjectHighlightColor;
	public GameObject candidateHolder;

	public void ShowDeathmatchEndGameUI(List<UIParameter> _uiParameters, int _selfIndex)
	{
		for(int i = 0; i < _uiParameters.Count; i++)
		{
			GameObject _newlyCreatedCandidate = GameObject.Instantiate (candidateSample);
			_newlyCreatedCandidate.SetActive (true);
			_newlyCreatedCandidate.transform.SetParent (candidateHolder.transform);
			_newlyCreatedCandidate.transform.localScale = new Vector3 (1, 1, 1);

			if (i == _selfIndex)
				_newlyCreatedCandidate.GetComponent<DeathmatchEndGameUICandidate> ().Setup (_uiParameters [i].playerName, _uiParameters [i].playerRole,
																							_uiParameters [i].killsCount.ToString(), _uiParameters [i].deathsCount.ToString(), selfObjectHighlightColor);
			else
				_newlyCreatedCandidate.GetComponent<DeathmatchEndGameUICandidate> ().Setup (_uiParameters [i].playerName, _uiParameters [i].playerRole, 
																							_uiParameters [i].killsCount.ToString(), _uiParameters [i].deathsCount.ToString());
		}
	}
}
