﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class EndGameUIControl : MonoBehaviour 
{
	[SerializeField]
	private GameObject exitButton;
	[SerializeField]
	private DeathmatchEndGameUIControl deathmatchEndGameUIControl;
	private Action OnExitButtonClicked;

	public void Setup(Action _onExitButtonClicked)
	{
		OnExitButtonClicked = _onExitButtonClicked;
	}

	public void ShowDeathmatchEndGameUI (List<DeathmatchEndGameUIControl.UIParameter> _uiParameters, int _selfIndex)
	{
		deathmatchEndGameUIControl.gameObject.SetActive (true);
		deathmatchEndGameUIControl.ShowDeathmatchEndGameUI (_uiParameters, _selfIndex);
		exitButton.SetActive (true);
	}

	public void ExitButtonClicked()
	{
		OnExitButtonClicked ();
	}
}
