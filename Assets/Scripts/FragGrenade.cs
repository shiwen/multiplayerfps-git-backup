﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: FragGrenade.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Ronnie
/// DATE			: 12/07/16
/// SCRIPT REF		: GrenadeBase.cs
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Ronnie	| 12/07/16	|	- Pass over functions from grenadeBase
/// </summary> ------------------------------------------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class FragGrenade : Grenade {

	public GameObject explosionPrefab;

	protected FragGrenade (FragGrenade other) : base (other) {
	}

	void Awake() 	{
		StartCoroutine(Detonate());
	}

	public void CheckMasterObj(bool _value, GameObject _cunt)
	{
		CanSelfHarm(_value, _cunt);
		masterObject = _cunt;
		Debug.Log("Cunt : " + _cunt.name);
	}

	protected override IEnumerator Detonate() {
		yield return new WaitForSeconds(timeBfrExplode);

		hitPoint = preciseCollider.GetPreviousPosition();
		hitNormal = preciseCollider.GetHitNormal();

		if (explosionPrefab != null) {
			Instantiate(explosionPrefab, hitPoint, Quaternion.identity);
		}

		Collider[] _hitColliders = Physics.OverlapSphere(hitPoint, explosionRadius);

		foreach(Collider col in _hitColliders) {
			RaycastHit hit;
			if(Physics.Raycast(hitPoint, (col.transform.position - hitPoint), out hit)) {
				if(hit.collider.transform.root.tag == "Player"){
					Debug.Log("HITTTTTTT");
				}

				if(OnGrenadeImpact != null){
					OnGrenadeImpact(col,this);
				}
				Debug.DrawLine (hitPoint, col.transform.position, Color.red, 2f);
			}
		}
	}
}

