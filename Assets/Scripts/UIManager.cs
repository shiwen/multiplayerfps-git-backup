﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIManager : MonoBehaviour 
{
	public RoleSelectionUIControl roleSelectionUIControl;
	public PlayerStatusUIControl playerStatusUIControl;
	public WeaponInfoUIControl weaponInfoUIControl;
	public DamageIndicatorUIController dmgIndicatorUIController;
	public RoomPlayersInfoBoardControl roomPlayersInfoBoardControl;
	public DeathTimerUIControl deathTimerUIControl;
	public KillsInfoUIControl killsInfoUIControl;
	public DamageReceiveTrackerUIControl damageReceiveTrackerUIControl;
	public ModeratorUIControl moderatorUIControl;
	public FlashBangEffect flashBangEffect;
	public GrenadeSlotUIControl grenadeSlotUIControl;
	public EndGameUIControl endGameUIControl;

	public void Setup(Action<byte> _roomPlayerInfoActionDialogueKickFunction, Action _onEndGameUIExitButtonClickedAction)
	{
		roomPlayersInfoBoardControl.Setup (_roomPlayerInfoActionDialogueKickFunction, GetComponent<Canvas>().scaleFactor);
		endGameUIControl.Setup (_onEndGameUIExitButtonClickedAction);
	}
		
	public void EnableRoleUI(PlayerRole _playerRole)
	{
		HideRoleRelatedUI ();

		switch (_playerRole) {
		case PlayerRole.Assault:
		case PlayerRole.Healer:
		case PlayerRole.Support:
		case PlayerRole.Trapper:
			EnablePlayingRoleUI ();
			break;

		case PlayerRole.Spectator:
			EnableSpectatorUI ();
			break;
		}
	}

	private void HideRoleRelatedUI()
	{
		playerStatusUIControl.gameObject.SetActive (false);
		weaponInfoUIControl.gameObject.SetActive (false);
		grenadeSlotUIControl.gameObject.SetActive (false);
	}

	private void EnablePlayingRoleUI()
	{
		playerStatusUIControl.gameObject.SetActive (true);
		weaponInfoUIControl.gameObject.SetActive (true);
	}

	private void EnableSpectatorUI()
	{
		
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Tab)) 
		{
			roomPlayersInfoBoardControl.Show ();
		}
		else if (Input.GetKeyUp (KeyCode.Tab)) 
		{
			roomPlayersInfoBoardControl.Hide ();
		}
	}

	#region components

	public void RegisterFlashBangCamera(Camera _playerCamera)
	{
		flashBangEffect.RegisterCamera (_playerCamera);
		flashBangEffect.gameObject.SetActive (true);
	}

	public void DisableFlashBang()
	{
		flashBangEffect.gameObject.SetActive (false);
	}

	public void TriggerFlashBang(float _freezeTime = 1f, float _recoveryRate = 0.3f)
	{
		flashBangEffect.Flash (_freezeTime, _recoveryRate);
	}

	#endregion

	#region end game

	public void ShowDeathmatchEndGameUI (List<DeathmatchEndGameUIControl.UIParameter> _uiParameters, int _selfIndex)
	{
		endGameUIControl.ShowDeathmatchEndGameUI (_uiParameters, _selfIndex);
	}

	#endregion
}
