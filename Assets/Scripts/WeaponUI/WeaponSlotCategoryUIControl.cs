﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WeaponSlotCategoryUIControl : MonoBehaviour 
{
	[System.Serializable]
	public struct CategoryNodeChangeColorProperties
	{
		public Color gunImageColor;
		public Color gunBackgroundColor;
	}
		
	public GameObject nodePrefab;
	public GameObject nodesHolder;
	public Image gunImage;
	public Image gunBackgroundImage;
	public CanvasGroupAlphaFade canvasFade;
	public CategoryNodeChangeColorProperties selectedColorProperties;
	public CategoryNodeChangeColorProperties defaultColorProperties;
	public UIImageColorTransitor colorTransitor;

	private List<WeaponSlotCategoryNodeUIControl> nodes = new List<WeaponSlotCategoryNodeUIControl>();
	private int currentSelectedNodeIndex = -1;
	private float nodesHeight = 100f;

	public void SetSelected(bool _showNodes)
	{
		colorTransitor.StartTransition (gunImage, selectedColorProperties.gunImageColor, 0.1f);
		colorTransitor.StartTransition (gunBackgroundImage, selectedColorProperties.gunBackgroundColor, 0.1f);

		if (_showNodes)
			ShowNodes ();
	}

	public void SetUnSelected()
	{
		colorTransitor.StartTransition (gunImage, defaultColorProperties.gunImageColor, 0.1f);
		colorTransitor.StartTransition (gunBackgroundImage, defaultColorProperties.gunBackgroundColor, 0.1f);

		if (currentSelectedNodeIndex != -1)
			nodes [currentSelectedNodeIndex].SetUnselected ();

		HideNodes ();
	}

	public void SetNodeSelected(int _index)
	{
		nodes [_index].SetSelected ();
		gunImage.sprite = nodes [_index].gunImage.sprite;

		if (currentSelectedNodeIndex != -1 && currentSelectedNodeIndex != _index)
			nodes [currentSelectedNodeIndex].SetUnselected ();

		currentSelectedNodeIndex = _index;
	}

	public void AddNode(string _gunPictureAddress)
	{
		int nodesCount = nodesHolder.transform.childCount;
		WeaponSlotCategoryNodeUIControl _newlyAddedNode = GameObject.Instantiate (nodePrefab).GetComponent<WeaponSlotCategoryNodeUIControl> ();
		_newlyAddedNode.gunImage.sprite = Resources.Load<Sprite>(_gunPictureAddress);
		_newlyAddedNode.transform.SetParent(nodesHolder.transform);
		_newlyAddedNode.transform.localScale = new Vector3 (1f, 1f, 1f);
		_newlyAddedNode.transform.localPosition = new Vector3 (0, -nodesCount * nodesHeight, 0);
		nodes.Add(_newlyAddedNode);

		if (nodesCount + 1 == 1) 
		{
			gameObject.SetActive (true);
			gunImage.sprite = nodes [0].gunImage.sprite;
		}
	}

	public void ClearNodes()
	{
		while(nodesHolder.transform.childCount != 0)
		{
			GameObject.DestroyImmediate (nodesHolder.transform.GetChild (0).gameObject);
		}
	}

	private void ShowNodes()
	{
		nodesHolder.SetActive (true);
		canvasFade.StopFade ();
		canvasFade.StartFade (1.0f, 0.1f);
	}

	private void HideNodes()
	{
		canvasFade.StopFade ();
		canvasFade.StartFade (0.0f, 0.1f, DeactiveNodes);
	}

	private void DeactiveNodes()
	{
		nodesHolder.SetActive (false);
	}
}
