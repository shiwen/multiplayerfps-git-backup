﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GrenadeSlotUIControl : MonoBehaviour 
{
	public GameObject nodePrefab;
	public Image currentGranadeImage;
	public GameObject grenadeNodesHolder;

	public float holderFadeTime;
	public float holderExistTime;

	public void AddNewGrenadeUINode(GrenadeType _identifier)
	{
		GrenadeSlotUINode _newNode = GameObject.Instantiate (nodePrefab).GetComponent<GrenadeSlotUINode> ();
		_newNode.transform.SetParent (grenadeNodesHolder.transform);
		_newNode.Setup (_identifier, null, grenadeNodesHolder.GetComponent<ToggleGroup> ());

		if (grenadeNodesHolder.transform.childCount == 1) 
		{
			gameObject.SetActive (true);
			currentGranadeImage.sprite = _newNode.GetGrenadeImage ();
		}
	}

	public void RemoveExsitingGrenadeUINode(GrenadeType _identifier)
	{
		GameObject.Destroy (GetNodeByIdentifier (_identifier).gameObject);
	}

	public void UpdateGranadeCount(GrenadeType _identifier, int _count)
	{
		GetNodeByIdentifier (_identifier).SetGrenadeCount (_count);
	}

	public void SetSelectedGrenadeUINode(GrenadeType _identifier)
	{
		grenadeNodesHolder.SetActive(true);
		grenadeNodesHolder.GetComponent<CanvasGroupAlphaFade> ().StartFade (1.0f, holderFadeTime, GrenadeHolderShowed);
	}

	public void ClearGrenadeHolders()
	{
		for (int i = 0; i < grenadeNodesHolder.transform.childCount; i++) 
		{
			GameObject.Destroy (grenadeNodesHolder.transform.GetChild (i).gameObject);
		}
	}

	private GrenadeSlotUINode GetNodeByIdentifier(GrenadeType _identifier)
	{
		for (int i = 0; i < grenadeNodesHolder.transform.childCount; i++) 
		{
			GrenadeSlotUINode _currentNode = grenadeNodesHolder.transform.GetChild (i).GetComponent<GrenadeSlotUINode> ();

			if (_currentNode.bindedGrenadeType == _identifier) 
			{
				return _currentNode;
			}
		}

		return null;
	}

	private void GrenadeHolderShowed()
	{
		CancelInvoke ("StartHideGrenadeHolder");
		Invoke ("StartHideGrenadeHolder", holderExistTime);
	}

	private void StartHideGrenadeHolder()
	{
		grenadeNodesHolder.GetComponent<CanvasGroupAlphaFade> ().StartFade (0.0f, holderFadeTime, DisableGrenadeHolder);
	}

	private void DisableGrenadeHolder()
	{
		grenadeNodesHolder.SetActive (false);
	}
}
