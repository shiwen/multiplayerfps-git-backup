﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponStatusUIControl : MonoBehaviour 
{
	public Text CurrentAmmoText;
	public Text TotalAmmoText;
	public Image GunImage;

	public void SetupWeaponStatus(string _gunTexturePath, int _currentAmmo, int _totalAmmo)
	{
		GunImage.sprite = Resources.Load<Sprite> (_gunTexturePath);
		UpdateAmmoCount (_currentAmmo, _totalAmmo);
	}

	public void UpdateAmmoCount(int _currentAmmo, int _totalAmmo)
	{
		CurrentAmmoText.text = _currentAmmo.ToString ();
		TotalAmmoText.text = _totalAmmo.ToString ();
	}
}
