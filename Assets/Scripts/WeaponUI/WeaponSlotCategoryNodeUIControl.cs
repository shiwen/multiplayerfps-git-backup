﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponSlotCategoryNodeUIControl : MonoBehaviour 
{
	public Image gunImage;
	public Image gunBackgroundImage;
	public WeaponSlotCategoryUIControl.CategoryNodeChangeColorProperties selectedColorProperties;
	public WeaponSlotCategoryUIControl.CategoryNodeChangeColorProperties defaultColorProperties;
	public UIImageColorTransitor colorTransitor;

	public void SetSelected()
	{
		colorTransitor.StartTransition (gunImage, selectedColorProperties.gunImageColor, 0.1f);
		colorTransitor.StartTransition (gunBackgroundImage, selectedColorProperties.gunBackgroundColor, 0.1f);
	}

	public void SetUnselected()
	{
		colorTransitor.StartTransition (gunImage, defaultColorProperties.gunImageColor, 0.1f);
		colorTransitor.StartTransition (gunBackgroundImage, defaultColorProperties.gunBackgroundColor, 0.1f);
	}
}
