﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GrenadeSlotUINode : MonoBehaviour 
{
	public GrenadeType bindedGrenadeType;
	public Image grenadeImage;
	public Text grenadeCountText;
	
	public void Setup(GrenadeType _bindedGrenadeType, Sprite _grenadeImage, ToggleGroup _toggleGroup)
	{
		bindedGrenadeType = _bindedGrenadeType;
		grenadeImage.sprite = _grenadeImage;
		GetComponent<Toggle> ().group = _toggleGroup;
	}

	public Sprite GetGrenadeImage()
	{
		return grenadeImage.sprite;
	}

	public void SetGrenadeCount(int _count)
	{
		grenadeCountText.text = _count.ToString ();
	}
}
