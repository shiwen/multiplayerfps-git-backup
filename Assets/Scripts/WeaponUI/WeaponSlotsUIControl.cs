﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class WeaponSlotsUIControl : MonoBehaviour 
{
	public WeaponSlotCategoryUIControl[] categoryControls;
	public CanvasGroupAlphaFade canvasFade;

	private int currentlySelectedCategoryIndex = -1;

	public void InitializeWeaponSlots(Dictionary<int, List<string>> _currentWeapons)
	{
		foreach (WeaponSlotCategoryUIControl _candidate in categoryControls) 
		{
			_candidate.ClearNodes ();
			_candidate.gameObject.SetActive (false);
		}
		
		foreach (KeyValuePair<int, List<string>> _entry in _currentWeapons) 
		{
			foreach(string _candidate in _entry.Value)
				categoryControls [_entry.Key].AddNode (_candidate);
		}
	}

	public void AddNewWeaponNode(int _weaponTierIndex, string _weaponPicAddress)
	{
		categoryControls [_weaponTierIndex].AddNode (_weaponPicAddress);
	}

	public void UpdateWeaponSlotUI(int _weaponTierIndex, int _slotIndex)
	{
		if(!gameObject.activeInHierarchy)
			Show ();
	
		categoryControls [_weaponTierIndex].SetNodeSelected (_slotIndex);

		if (currentlySelectedCategoryIndex != _weaponTierIndex) 
		{
			categoryControls [_weaponTierIndex].SetSelected (true);

		}
	
		if (currentlySelectedCategoryIndex != -1 && currentlySelectedCategoryIndex != _weaponTierIndex) 
		{
			categoryControls [currentlySelectedCategoryIndex].SetUnSelected ();
		}

		currentlySelectedCategoryIndex = _weaponTierIndex;

		CancelInvoke ();
		Invoke ("Hide", 2.0f);
	}

	private void Show()
	{
		gameObject.SetActive (true);
		canvasFade.StartFade (1.0f, 0.1f);
	}

	private  void Hide()
	{
		canvasFade.StartFade (0.0f, 0.1f, Deactive);
	}

	private void Deactive()
	{
		gameObject.SetActive (false);
	}
}
