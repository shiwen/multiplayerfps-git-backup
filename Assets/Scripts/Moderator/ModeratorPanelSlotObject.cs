﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ModeratorPanelSlotObject : MonoBehaviour 
{
	public Text playerNameText;
	public Dropdown playerRoleSelectionDropDown;
	[HideInInspector]
	public byte bindedPlayerIdentifier;
	Action<ModeratorPanelSlotObject, string> OnNewRoleSelected;

	public void Setup(Action<ModeratorPanelSlotObject, string> _onNewRoleSelected)
	{
		OnNewRoleSelected = _onNewRoleSelected;
	}

	public string GetCurrentRoleString()
	{
		return playerRoleSelectionDropDown.captionText.text;
	}

	public void SetBindedPlayer(byte _identifier, string _playerName)
	{
		bindedPlayerIdentifier = _identifier;
		playerNameText.text = "Player: " + _playerName;
	}

	public void SetEmpty()
	{
		bindedPlayerIdentifier = 0;
		playerNameText.text = "Player: -";
	}

	public void NewRoleSelected()
	{
		OnNewRoleSelected (this, playerRoleSelectionDropDown.captionText.text);
	}
}
