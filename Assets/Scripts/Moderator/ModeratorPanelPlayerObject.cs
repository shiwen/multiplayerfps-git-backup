﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ModeratorPanelPlayerObject : MonoBehaviour 
{
	public Text nameText;

	[HideInInspector]
	public byte playerIdentifier;

	public Action<ModeratorPanelPlayerObject> OnObjectClicked, OnKickButtonClicked;

	public void SetUp(string _nameText, byte _playerIdentifier, Action<ModeratorPanelPlayerObject> _onObjectClicked, Action<ModeratorPanelPlayerObject> _onKickButtonClicked)
	{
		nameText.text = _nameText;
		playerIdentifier = _playerIdentifier;
		OnObjectClicked += _onObjectClicked;
		OnKickButtonClicked = _onKickButtonClicked;
	}

	public string GetPlayerName()
	{
		return nameText.text;
	}

	public void ObjectClicked()
	{
		OnObjectClicked (this);
	}

	public void KickButtonClicked()
	{
		OnKickButtonClicked (this);
	}
		
}
