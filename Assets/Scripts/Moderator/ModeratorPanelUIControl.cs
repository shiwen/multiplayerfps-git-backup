﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class ModeratorPanelUIControl : MonoBehaviour 
{
	//references
	public GameObject playerObjectsHolder;
	public GameObject slotObjectsHolder;
	public GameObject playerObjectPrefab;
	public GameObject slotObjectPrefab;
	public Button startButton;

	public Action<List<byte>, List<string>> OnStartButtonClicked;
	public Action OnCloseButtonClicked;
	public Action<byte> OnKickButtonClicked;
	public Action<byte, string> OnPostStartGameAssignRole;
	public Action<byte> OnCancelPlayerRole;

	private ModeratorPanelPlayerObject currentDraggingPlayerObject;
	private bool draggingPlayerObject = false;
	private Canvas mainCanvas;
	private bool gameIsStarted;

	public void Setup(Canvas _mainCanvas, Action<List<byte>, List<string>> _onStartButtonClicked, Action _onCloseButtonClicked, 
		Action<byte> _onKickButtonClicked, Action<byte, string> _onPostStartGameAssignRole, Action<byte> _onCancelPlayerRole)
	{
		mainCanvas = _mainCanvas;
		OnStartButtonClicked = _onStartButtonClicked;
		OnCloseButtonClicked = _onCloseButtonClicked;
		OnKickButtonClicked = _onKickButtonClicked;
		OnPostStartGameAssignRole = _onPostStartGameAssignRole;
		OnCancelPlayerRole = _onCancelPlayerRole;

		for (int i = 0; i < slotObjectsHolder.transform.childCount; i++) 
		{
			slotObjectsHolder.transform.GetChild (i).GetComponent<ModeratorPanelSlotObject> ().Setup (SetRoleText);
		}
	}

	public void AddPlayerObject(byte _playerIdentifier, string _playerName)
	{
		ModeratorPanelPlayerObject _playerObject = GameObject.Instantiate (playerObjectPrefab).GetComponent<ModeratorPanelPlayerObject> ();
		_playerObject.SetUp ((playerObjectsHolder.transform.childCount + 1).ToString() + "." + _playerName, _playerIdentifier, StartDragPlayerObject, PlayerObjectKickButtonClicked);
		_playerObject.transform.SetParent (playerObjectsHolder.transform);
		_playerObject.transform.localScale = new Vector3 (1, 1, 1);
	}

	public void RemovePlayerObject(byte _playerIdentifier)
	{
		for (int i = 0; i < playerObjectsHolder.transform.childCount; i++) 
		{
			ModeratorPanelPlayerObject _playerObject = playerObjectsHolder.transform.GetChild (i).GetComponent<ModeratorPanelPlayerObject> ();
			
			if (_playerObject.playerIdentifier == _playerIdentifier) 
			{
				GameObject.Destroy (_playerObject.gameObject);
				break;
			}
		}

		for (int i = 0; i < slotObjectsHolder.transform.childCount; i++) 
		{
			ModeratorPanelSlotObject _slotObject = slotObjectsHolder.transform.GetChild (i).GetComponent<ModeratorPanelSlotObject> ();
			
			if (_slotObject.bindedPlayerIdentifier == _playerIdentifier) 
			{
				_slotObject.SetEmpty ();
				break;
			}
		}
	}

	public void DisableStartButton()
	{
		startButton.interactable = false;
	}

	void Update()
	{
		if (draggingPlayerObject) 
		{
			Vector2 _screenPos;
			RectTransformUtility.ScreenPointToLocalPointInRectangle (mainCanvas.transform as RectTransform, Input.mousePosition, mainCanvas.worldCamera, out _screenPos);
			currentDraggingPlayerObject.transform.position = mainCanvas.transform.TransformPoint (_screenPos);

			if (Input.GetMouseButtonUp (0)) 
			{
				PointerEventData _pointerEvent = new PointerEventData (EventSystem.current);
				_pointerEvent.position = Input.mousePosition;
				List<RaycastResult> _result = new List<RaycastResult> ();

				EventSystem.current.RaycastAll (_pointerEvent, _result);

				foreach (RaycastResult _candidate in _result) 
				{
					if (_candidate.gameObject.GetComponent<ModeratorPanelSlotObject> () != null) 
					{
						SetBindedPlayer (_candidate.gameObject.GetComponent<ModeratorPanelSlotObject> (), currentDraggingPlayerObject.playerIdentifier, currentDraggingPlayerObject.GetPlayerName ());
					}
				}

				GameObject.Destroy (currentDraggingPlayerObject.gameObject);
				draggingPlayerObject = false;
			}
		}
	}

	private void SetBindedPlayer(ModeratorPanelSlotObject _targetedSlotObject, byte _playerIdentifier, string _playerName)
	{
		for (int i = 0; i < slotObjectsHolder.transform.childCount; i++) 
		{
			ModeratorPanelSlotObject _currentSlotObject = slotObjectsHolder.transform.GetChild(i).GetComponent<ModeratorPanelSlotObject>();

			if(_currentSlotObject != _targetedSlotObject)
			{
				if (_currentSlotObject.bindedPlayerIdentifier == _playerIdentifier) 
				{
					if (gameIsStarted) 
					{
						OnCancelPlayerRole (_playerIdentifier);
					}
					
					_currentSlotObject.SetEmpty ();
				}
			}
		}

		for (int i = 0; i < slotObjectsHolder.transform.childCount; i++) 
		{
			ModeratorPanelSlotObject _currentSlotObject = slotObjectsHolder.transform.GetChild (i).GetComponent<ModeratorPanelSlotObject> ();

			if (_currentSlotObject == _targetedSlotObject) 
			{
				if (_currentSlotObject.bindedPlayerIdentifier != _playerIdentifier) 
				{
					if (gameIsStarted) 
					{
						if (_currentSlotObject.bindedPlayerIdentifier != 0) 
						{
							OnCancelPlayerRole (_currentSlotObject.bindedPlayerIdentifier);
						}

						OnPostStartGameAssignRole (_playerIdentifier, _targetedSlotObject.GetCurrentRoleString ());
					}

					_targetedSlotObject.SetBindedPlayer (_playerIdentifier, _playerName);
				}
			}
		}

	}

	#region button listeners

	public void StartButtonClicked()
	{
		List<byte> _playerIdentifiers = new List<byte> ();
		List<string> _roleStrings = new List<string> ();

		for (int i = 0; i < slotObjectsHolder.transform.childCount; i++) 
		{
			ModeratorPanelSlotObject _slotObject = slotObjectsHolder.transform.GetChild (i).GetComponent<ModeratorPanelSlotObject> ();

			if (_slotObject.bindedPlayerIdentifier != 0) //only add if this player is not moderator
			{
				_playerIdentifiers.Add (_slotObject.bindedPlayerIdentifier);
				_roleStrings.Add(_slotObject.GetCurrentRoleString());
			}
		}

		OnStartButtonClicked (_playerIdentifiers, _roleStrings);
		gameIsStarted = true;
	}

	public void CloseButtonClicked()
	{
		OnCloseButtonClicked ();
	}

	public void StartDragPlayerObject(ModeratorPanelPlayerObject _playerObject)
	{
		currentDraggingPlayerObject = GameObject.Instantiate (_playerObject).GetComponent<ModeratorPanelPlayerObject> ();
		currentDraggingPlayerObject.transform.SetParent (this.transform);
		currentDraggingPlayerObject.transform.localScale = new Vector3 (1, 1, 1);
		currentDraggingPlayerObject.GetComponent<RectTransform> ().sizeDelta = _playerObject.transform.GetComponent<RectTransform> ().sizeDelta;
		draggingPlayerObject = true;
	}

	public void PlayerObjectKickButtonClicked(ModeratorPanelPlayerObject _playerObject)
	{
		OnKickButtonClicked (_playerObject.playerIdentifier);
	}

	public void SetRoleText(ModeratorPanelSlotObject _slotObject, string _roleText)
	{
		if (gameIsStarted) 
		{
			if (_slotObject.bindedPlayerIdentifier != 0) 
			{
				OnCancelPlayerRole (_slotObject.bindedPlayerIdentifier);
				OnPostStartGameAssignRole (_slotObject.bindedPlayerIdentifier, _roleText);
			}
		}
	}

	#endregion
}
