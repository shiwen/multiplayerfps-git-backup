﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ModeratorUIControl : MonoBehaviour
{
	public ModeratorPanelUIControl moderatorPanelUIControl;
	public Button panelToggleButton;
	
	public void Setup(Canvas _mainCanvas, Action<List<byte>, List<string>> _startButtonClicked,  Action<byte> _playerObjectKickButtonClicked, 
		Action<byte, string> _postStartGameAssignRole, Action<byte> _postStartGameCancelRole)
	{
		gameObject.SetActive (true);
		moderatorPanelUIControl.Setup (_mainCanvas, _startButtonClicked, HidePanelUI, _playerObjectKickButtonClicked, _postStartGameAssignRole, _postStartGameCancelRole);
		ShowPanelUI ();
	}

	public void AddNewPlayerObject(byte _identifier, string _playerName)
	{
		moderatorPanelUIControl.AddPlayerObject (_identifier, _playerName);
	}

	public void RemovePlayerObject(byte _identifier)
	{
		moderatorPanelUIControl.RemovePlayerObject (_identifier);
	}

	public void OnGameplayStarted()
	{
		moderatorPanelUIControl.DisableStartButton ();
	}

	public void HidePanelUI()
	{
		moderatorPanelUIControl.gameObject.SetActive (false);
		panelToggleButton.gameObject.SetActive (true);
	}

	public void ShowPanelUI()
	{
		moderatorPanelUIControl.gameObject.SetActive (true);
		panelToggleButton.gameObject.SetActive (false);
	}
}
