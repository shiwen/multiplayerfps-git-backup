﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: Grenade.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Ronnie
/// DATE			: 27/06/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Ronnie	| 27/06/16	|	- added explosion checking logic
/// 0.2		| Ronnie	| 30/06/16	|	- added damage checking and server checking logic
/// </summary> ------------------------------------------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public enum GrenadeType {
	FragGrenade, FlashBang
}
	
public class Grenade : NetworkBehaviour {
	
	public Action <Collider,Grenade> OnGrenadeImpact;

	public GameObject masterObject;

	protected PreciseCollider preciseCollider; 

	public Vector3 hitPoint, hitNormal;

	public byte shooterIdentifier;

	public float timeBfrExplode = 2f;
	public float explosionRadius = 20.0f;

	private bool isServerCalculate;

	public string grenadeTexturePath;

	protected Grenade(Grenade _grenade) {
		grenadeTexturePath = _grenade.grenadeTexturePath;
	}

	void Start () {
		preciseCollider = GetComponent<PreciseCollider>();
	}

	public void SetShooterIdentifier(byte _identifier){
		shooterIdentifier = _identifier;
	}

	public void CanSelfHarm(bool _value = true, GameObject _masterObject = null)
	{
		if(!_value){
			masterObject = _masterObject;
			if(_masterObject == null){
				Debug.LogError("Master object is not defined");
			}
		}
	}

	protected virtual IEnumerator Detonate() 
	{
		yield return new WaitForSeconds(timeBfrExplode);

		hitPoint = preciseCollider.GetPreviousPosition();
		hitNormal = preciseCollider.GetHitNormal();

		Collider[] _hitColliders = Physics.OverlapSphere(hitPoint, explosionRadius);

		foreach(Collider col in _hitColliders) {

			if(OnGrenadeImpact != null){
				OnGrenadeImpact(col,this);
			}
		}
	}
}
