﻿using UnityEngine;
using System.Collections;

public class PlayerCollect : MonoBehaviour, ICollectable
{
	WeaponDatabase weaponDatabase;

	void Awake()
	{
		weaponDatabase = WeaponDatabase.instance;
	}

	public void OnWeaponCollected(WeaponType _weaponType, int ammoAmmount)
	{
		GetComponent<PlayerManager> ().PickUpWeapon (weaponDatabase.GetWeapon(_weaponType), ammoAmmount);
	}

	public void OnPowerUpCollected(int _powerUpIdentifier)
	{
		GetComponent<PlayerPowerUpApplifier> ().ApplyPowerUp (_powerUpIdentifier);
	}

	public void OnHealthBagCollected(int _healAmount)
	{
		GetComponent<PlayerManager> ().ReceiveHealth (_healAmount);
	}

	public void OnGrenadeCollected(GrenadeType _grenadeType, int _amount)
	{
		GetComponent<PlayerManager> ().PickUpGrenade (_grenadeType);
	}
}
