﻿using UnityEngine;
using System.Collections;

public class PlayerPostDeathCameraControl : MonoBehaviour 
{
	public GameObject pivotObject;
	public Vector3 pivotOffset;
	private GameObject lockedTarget;

	public void Execute(GameObject _killerObject)
	{
		lockedTarget = _killerObject;
		this.enabled = true;
	}

	void Update()
	{
		Vector3 _worldSpacePivotPoint = pivotObject.transform.position + pivotOffset;
		Vector3 _targetedLocalVector = -(lockedTarget.transform.position - _worldSpacePivotPoint).normalized;
		Vector3 _selfLocalVector = (transform.position - _worldSpacePivotPoint).normalized;
		transform.RotateAround(_worldSpacePivotPoint, Vector3.Cross(_selfLocalVector, _targetedLocalVector), Vector3.Angle(_targetedLocalVector, _selfLocalVector) * Time.deltaTime);
		transform.LookAt (_worldSpacePivotPoint);
	}
}
