﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerNameUI : MonoBehaviour {

	[SerializeField]
	private Text player_name_txt;

	public void SetPlayerName(string _name){
		player_name_txt.text = _name;
	}

	public void TogglePlayerName(bool _value){
		player_name_txt.gameObject.SetActive (_value);
	}
}
