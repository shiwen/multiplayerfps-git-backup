﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: PlayerController.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 16/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Cee Jay	| 16/05/16	|	- added player controller properties functions
/// 									- reworked sprint only works in forward direction
/// 									- added callback on death trigger
/// 									- added connectionIndex <byte>
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {
	[Header("Locomotion Properties")]
	[SerializeField]
	private float lookSensitivity = 3f;
	[SerializeField]
	private float speed = 3f;
	[SerializeField]
	private float runSpeed = 5f;
	
	private bool isRunning;

	//Components
	private PlayerMovement playerMovement;
	private Destructible destructible;
	private PlayerShoot playerShoot;
	private PlayerAnimationManager playerAnim;

	[SyncVar]
	public byte connectionIndex;


	[SerializeField]
	private Behaviour[] disableOnDeathBehaviours;

	// Use this for initialization
	void Start () {
		playerMovement = GetComponent<PlayerMovement>();
		playerShoot = GetComponent<PlayerShoot>();
		destructible = GetComponent<Destructible>();
		playerAnim = GetComponent<PlayerAnimationManager>();

		playerShoot.Init();

		destructible.OnDestroy = null;
		destructible.OnDestroy += playerAnim.CmdPlayDead;

		destructible.OnGrenadeDestroy = null;
		destructible.OnGrenadeDestroy = playerAnim.CmdPlayDead;
	}
	
	// Update is called once per frame
	void Update () {

		//Player movement
		Locomotion();

		//Shoot
		ShootAction();
	}

	#region Locomotion
	private void Locomotion(){
		UpdateMovement();
		UpdateRotation();

		UpdateInput();
	}

	private void UpdateInput(){
		playerAnim.CmdSetXBlend(Input.GetAxis("Horizontal"));
		playerAnim.CmdSetZBlend(Input.GetAxis("Vertical"));

		//can only sprint on forward direction
		if(Input.GetKey(KeyCode.LeftShift) && Input.GetAxis("Horizontal") == 0f && Input.GetAxis("Vertical") > 0){
			playerAnim.CmdTriggerSprint(true);
		}else{
			playerAnim.CmdTriggerSprint(false);
		}
	}

	private void UpdateMovement(){
		float _xMovement = Input.GetAxis("Horizontal");
		float _zMovement = Input.GetAxis("Vertical");


		Vector3 _moveHorizontal = transform.right * _xMovement;
		Vector3 _moveVertical = transform.forward * _zMovement;

		//can only sprint on forward direction
		if(Input.GetKey(KeyCode.LeftShift) && _moveHorizontal == Vector3.zero && Input.GetAxis("Vertical") > 0){
			isRunning = true;
		}else{
			isRunning = false;
		}

		//final movement vector
		Vector3 _velocity = (_moveHorizontal + _moveVertical) * (isRunning ? runSpeed : speed);

		playerMovement.Move(_velocity);

	}

	private void UpdateRotation(){
		float _yRotation = Input.GetAxis("Mouse X");
		Vector3 _rotation = new Vector3(0,_yRotation,0) * lookSensitivity;

		playerMovement.Rotate(_rotation);

		//calculate rotation as a 3D vector (turning around)
		float _xRot = Input.GetAxisRaw("Mouse Y");

		float _cameraRotationX =_xRot * lookSensitivity;

		playerMovement.RotateCamera(_cameraRotationX);
	}

	#endregion

	private void ShootAction(){
		if(isRunning){
			return;
		}

		if(Input.GetButton("Fire1")){
			playerShoot.Shoot();
		}

		if(Input.GetKeyUp(KeyCode.R)){
			playerShoot.Reload();
		}
	}



	//Damage Logic
//	[ClientRpc]
//	public void RpcTakeDamage(int _amount){
//		if(destructible != null){
//			destructible.ApplyDamage(_amount);
//
//		}else{
//			Debug.Log("This object is indestructible");
//		}
//	}
}
