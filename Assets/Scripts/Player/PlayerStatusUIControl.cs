﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: PlayerStatusUIControl.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 27/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Patrick	| 27/05/16	|	- derived health bar related function from player status ui control			
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStatusUIControl : MonoBehaviour 
{
	public PlayerStatusHealthUIControl playerStatusHealthUIControl;
}
