﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: PlayerManager.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 10/06/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Cee Jay	| 10/06/16	|	- added various functions for each of the components
/// 		| Cee Jay	| 17/06/16	|	- added shield related functions.
/// 								|	- updated shield support on destructible side
/// 									- added callback via shield expires
/// 									- added remove shield
/// 		| Patrick	| 23/06/16  |   - added "OnDeath", which will trigger when player is dead.
/// 		| Cee Jay	| 27/06/16	|	- append parameter <Weapon> weaponUsed & <bool> isHeadShot in CheckIsDead
/// 		| Patrick	| 23/06/16  |   - added "OnDeath", which will trigger when player is dead.
///                                     - added PlayerDamageReceiveDricetionTracker component and related functions.
/// 					| 11/07/16  |   - added flash bang related function and statements.
/// 		| Cee Jay	| 02/08/16	|	- added SetUIPlayerName and TogglePlayerName functions
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;

public class PlayerManager : NetworkBehaviour
{
	public Action OnDeath;
	public byte identifier;

	[SerializeField]
	private Behaviour[] disableOnDeathBehaviours = null; 

	//Components
	private MultiPlayerFPSNetworkConnectionManager networkManager;
	private GameManager gameManager;
	private Destructible destructible;
	private PlayerShoot playerShoot;
	private Launcher launcher;
	private PlayerAnimationManager animationManager;
	private CharacterController charController;
	private PlayerPowerUpApplifier powerUpApplier;
	private PlayerPostDeathCameraControl deathCameraControl;
	private PlayerDamageReceiveDirectionTracker damageReceiveDirectionTracker;
	private PlayerNameUI playerNameUI;

	[SyncVar]private bool isDead = false;

	// Use this for initialization
	void Awake () {
		networkManager = MultiPlayerFPSNetworkConnectionManager.GetInstance ();
		gameManager = GameManager.instance;

		destructible = GetComponent<Destructible>();
		playerShoot = GetComponent<PlayerShoot>();
		launcher = GetComponent<Launcher>();
		animationManager = GetComponent<PlayerAnimationManager>();
		charController = GetComponent<CharacterController>();
		powerUpApplier = GetComponent<PlayerPowerUpApplifier> ();
		deathCameraControl = GetComponentInChildren<PlayerPostDeathCameraControl> ();
		damageReceiveDirectionTracker = GetComponent<PlayerDamageReceiveDirectionTracker> ();
		playerNameUI = GetComponentInChildren<PlayerNameUI> ();

		deathCameraControl.enabled = false;

		destructible.OnDestroy = null;
		destructible.OnDestroy += animationManager.CmdPlayDead;

		destructible.OnGrenadeDestroy = null;
		destructible.OnGrenadeDestroy += animationManager.CmdPlayDead;

		destructible.OnDeath = null;
		destructible.OnDeath += CheckIsDead;

		destructible.OnGrenadeDeath = null;
		destructible.OnGrenadeDeath += CheckGrenadeDeath;

		destructible.OnShieldExpire = null;
		destructible.OnShieldExpire += DestroyShield;

		destructible.OnDirectionalDamageReceive = null;
		destructible.OnDirectionalDamageReceive += TriggerDamageReceiveDirection;
	}

	void Start()
	{
		playerShoot.Init ();

		if (isLocalPlayer) 
		{
			gameManager.uiManager.RegisterFlashBangCamera(GetComponentInChildren<Camera> ());

			TogglePlayerName (false);
		}
	}

	public void SetupNetworkProperties(byte _identifier, string _playerName)
	{
		identifier = _identifier;
		playerShoot.SetPlayerIdentifier (identifier);
		launcher.SetPlayerIdentifier(identifier);
		playerNameUI.SetPlayerName (_playerName);
	}

	private void CheckIsDead(bool _value, byte _shooterIdentifier, Weapon _weaponUsed, bool _isHeadShot){
		isDead = _value;

		for (int i = 0; i < disableOnDeathBehaviours.Length; i++) {
			disableOnDeathBehaviours[i].enabled = _value;
		}

		if (isDead) 
		{
			networkManager.ServerTriggerPlayerDeath (_shooterIdentifier, identifier, (int)(System.Enum.Parse(typeof(WeaponType), _weaponUsed.weaponName)), _isHeadShot);

			if (OnDeath != null)
				OnDeath ();
		}
	}

	private void CheckGrenadeDeath(bool _value, byte _shooterIdentifier) {
		isDead = _value;

		for (int i = 0; i < disableOnDeathBehaviours.Length; i++) {
			disableOnDeathBehaviours[i].enabled = _value;
		}

		if (isDead) 
		{
			networkManager.ServerTriggerPlayerDeathByGrenade (_shooterIdentifier, identifier);

			if (OnDeath != null)
				OnDeath ();
		}
	}

	public void TriggerPostDeathCamera(GameObject _killerObject)
	{
		GetComponent<NetworkCharacterMovement> ().enabled = false; //temp use this
		deathCameraControl.Execute (_killerObject);
	}

	public void TriggerFlashBang(float _freezeTime = 1.0f, float _recoveryRate = 0.3f)
	{
		gameManager.uiManager.TriggerFlashBang (_freezeTime, _recoveryRate);
	}

	#region PlayerShoot
	public void SwitchWeapon(int _inputIndex){
		if(isDead) return;
		playerShoot.SwitchWeapon(_inputIndex);
	}

	public void PickUpWeapon(Weapon _weapon,int _ammo){
		if(isDead) return;
		playerShoot.PickUpWeapon(_weapon,_ammo);
	}

	public List<GameObject> SpawnBullet (){
		if(isDead) return null;
		return playerShoot.SpawnBullet();
	}

	public GameObject SpawnGrenade() {
		if(isDead) return null;
		return launcher.GetGrenadeObj();
	}

	public void FireBullet (byte _shotID, bool _isServerCalculate, List<GameObject> _bullet){
		if(isDead) return;
		playerShoot.FireBullet(_shotID,_isServerCalculate,_bullet);
	}

	public void ReloadBullet (bool _hasAuthority){
		if(isDead) return;
		playerShoot.ReloadBullet(_hasAuthority);
	}

	public void FireGrenade (bool _isServerCalculate, GameObject _grenade, bool _isTriggerGrenadeUp) {
		if(isDead) return;
		if(_isTriggerGrenadeUp)
			launcher.FireGrenade(_isServerCalculate, _grenade);
	}

	public bool GetCanFire (){
		if(isDead) return false;
		return playerShoot.GetCanFire();
	}

	public bool GetCanReload (){
		if(isDead) return false;
		return playerShoot.GetCanReload();
	}
	#endregion

	#region Destructible
	public void ReceiveHealth(int _healAmount){
		if(isDead) return;
		destructible.ReceiveHealth(_healAmount);
	}

	#endregion

	#region Power Up

	public void ApplyShield(int _shieldAmount)
	{
		destructible.ReceiveShield(_shieldAmount);
	}

	public void RemoveShield()
	{
		destructible.RemoveShield ();
	}

	public void DestroyShield()
	{
		powerUpApplier.DestroyShield ();
	}

	#endregion

	#region damage receive direction

	public void TriggerDamageReceiveDirection(byte _shooterIdentifier)
	{
		damageReceiveDirectionTracker.Trigger (gameManager.GetPlayerManagerByIdentifier (_shooterIdentifier).gameObject);
	}

	#endregion


	#region Movement
	public void Move(Vector3 _dir){
		if(isDead) return;
		charController.Move (_dir);
	}

	public void Rotate(ref Quaternion _target, ref Quaternion _viewRot, float _vert, float _hori){
		if(isDead) return;

		_target = Quaternion.Euler (0, _hori, 0);
		_viewRot = Quaternion.Euler (_vert, _hori, 0);
	}
	#endregion

	#region grenade

	public void PickUpGrenade(GrenadeType _grenadeType)
	{
		launcher.PickupGrenade (_grenadeType);
	}

	#endregion

	#region Player UI
	public void SetUIPlayerName(string _name){
		playerNameUI.SetPlayerName (_name);
	}

	public void TogglePlayerName(bool _value){
		playerNameUI.TogglePlayerName (_value);
	}
	#endregion
}
