﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStatusHealthBarUIControl : MonoBehaviour 
{
	public Text CurrentHpText;
	public Image HpIndicatorImage;

	[System.Serializable]
	public struct IndicatorChangesProperties
	{
		public float targetRatio;
		public Color indicatorColor;
	}

	public IndicatorChangesProperties[] IndicatorChangesPropertieses;

	private IndicatorChangesProperties currentIndicatorChangesProperties;
	private bool isBlinking;
	private float defaultAlpha;
	private float blinkingStartRatio = 0.3f;
	private float blinkingInterval = 0.5f;
	private float blinkingIntervalCounter = 0.0f;
	private float blinkingDuration = 0.2f;
	private float blinkingCounter = 0.0f;
	private int blinkCompletedCounter = 0;

	void Awake()
	{
		defaultAlpha = HpIndicatorImage.color.a;
		ResetBlinkingRelatedProperties ();
	}

	void Update()
	{
		if (isBlinking)
			UpdateBlinking ();
	}

	//Updates player health ui
	public void UpdateHealthBar(string _currentHealthText, float _currentRatio)
	{
		CurrentHpText.text = _currentHealthText;

		IndicatorChangesProperties _targetProperties;
		float _localRatio;

		GetBetweenIndicatorChangesProperties (_currentRatio, out currentIndicatorChangesProperties, out _targetProperties, out _localRatio);
		HpIndicatorImage.color = Color.Lerp (currentIndicatorChangesProperties.indicatorColor, _targetProperties.indicatorColor, _localRatio);

		if (_currentRatio <= blinkingStartRatio) 
		{
			if (_currentRatio <= 0) 
			{
				isBlinking = false;
				ResetBlinkingRelatedProperties ();
			} 
			else 
			{
				isBlinking = true;
			}
		} 
		else 
		{
			if (isBlinking) 
			{
				ResetBlinkingRelatedProperties ();
			}

			isBlinking = false;
		}
	}

	private void GetBetweenIndicatorChangesProperties(float _currentRatio, out IndicatorChangesProperties _currentProperties, out IndicatorChangesProperties _targetProperties, out float _localRatio)
	{
		_currentProperties = new IndicatorChangesProperties();
		_targetProperties = new IndicatorChangesProperties();

		for (int i = 0; i < IndicatorChangesPropertieses.Length; i++) 
		{
			if (IndicatorChangesPropertieses [i].targetRatio >= _currentRatio) 
			{
				_currentProperties = IndicatorChangesPropertieses [i];

				if (i == 0) 
				{
					_targetProperties = _currentProperties;
				} 
				else 
				{
					_targetProperties = IndicatorChangesPropertieses [i - 1];
				}

				break;
			}
		}

		_localRatio = (_currentRatio - _currentProperties.targetRatio) / (_targetProperties.targetRatio - _currentProperties.targetRatio);
	}

	private void ResetBlinkingRelatedProperties()
	{
		blinkingIntervalCounter = 0.0f;
		blinkingCounter = 0.0f;
		blinkCompletedCounter = 0;
		HpIndicatorImage.color = new Color (HpIndicatorImage.color.r, HpIndicatorImage.color.g, HpIndicatorImage.color.b, defaultAlpha);
	}

	private void UpdateBlinking()
	{
		if (blinkingIntervalCounter >= blinkingInterval) 
		{
			if (blinkCompletedCounter >= 2) 
			{
				blinkingIntervalCounter = 0.0f;
				blinkCompletedCounter = 0;
			}
			else 
			{
				if (blinkingCounter >= (blinkingDuration / 2)) 
				{
					blinkCompletedCounter++;
					blinkingCounter = 0.0f;
				} 
				else 
				{
					if (blinkingCounter == 0.0f) 
					{
						if (blinkCompletedCounter == 0)
							HpIndicatorImage.CrossFadeAlpha (0.0f, (blinkingDuration / 2), true);
						else if(blinkCompletedCounter == 1)
							HpIndicatorImage.CrossFadeAlpha (defaultAlpha, (blinkingDuration / 2), true);
					}

					blinkingCounter += Time.deltaTime;
				}
			}
		} 
		else 
		{
			blinkingIntervalCounter += Time.deltaTime;
		}
	}
}
