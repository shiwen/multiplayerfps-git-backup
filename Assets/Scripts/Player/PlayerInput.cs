﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/// <summary>
/// Capture input only from client side and execute movement from server side.
/// Uncheck all field in Network Identity for player prefab.
/// </summary>

public class PlayerInput : NetworkBehaviour {

	int moveX = 0;
	int moveY = 0;
	float moveSpeed = 0.2f;

	void Update () {
		if (!isLocalPlayer) {
			return;
		}

		// input handling for local player only
		int oldMoveX = moveX;
		int oldMoveY = moveY;

		moveX = 0;
		moveY = 0;

		if (Input.GetKey(KeyCode.LeftArrow)) {
			moveX -= 1;
		}
		if (Input.GetKey(KeyCode.RightArrow)) {
			moveX += 1;
		}
		if (Input.GetKey(KeyCode.UpArrow)) {
			moveY += 1;
		}
		if (Input.GetKey(KeyCode.DownArrow)) {
			moveY -= 1;
		}
		if (moveX != oldMoveX || moveY != oldMoveY)	{
			CmdMove(moveX, moveY);
		}
	}

	[Command]
	public void CmdMove(int x, int y) {
		moveX = x;
		moveY = y;
//		isDirty = true;
		Debug.Log("server move");
	}
		
	public void FixedUpdate() {
		if (NetworkServer.active) {
			transform.Translate(moveX * moveSpeed, 0,  moveY * moveSpeed);
		}
	}

}
