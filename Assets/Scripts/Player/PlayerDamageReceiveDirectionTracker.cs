﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PlayerDamageReceiveDirectionTracker : MonoBehaviour 
{
	public class DamageReceiveDirectionTracker
	{
		public GameObject damageDealer;
		public float existTimeCounter;

		public DamageReceiveDirectionTracker(GameObject _damageDealer)
		{
			damageDealer = _damageDealer;
			existTimeCounter = 0;
			
		}
	}

	public float trackerExistTime;

	private DamageReceiveTrackerUIControl uiControl;
	private Dictionary<DamageReceiveDirectionTracker, int> existingTrackers;
	
	void Awake()
	{
		uiControl = GameManager.instance.uiManager.damageReceiveTrackerUIControl;
		existingTrackers = new Dictionary<DamageReceiveDirectionTracker, int> ();
	}
	
	public void Trigger(GameObject _damageDealerObject)
	{
		DamageReceiveDirectionTracker _newTracker = new DamageReceiveDirectionTracker (_damageDealerObject);
		existingTrackers.Add (_newTracker, GetAvailableTrackerId ());

		Vector3 _cross = transform.InverseTransformDirection ((_damageDealerObject.transform.position - transform.position).normalized);
		uiControl.AddTracker(existingTrackers[_newTracker], new Vector2(_cross.x, _cross.z));

		if (existingTrackers.Count == 1)
			this.enabled = true;
	}

	private int GetAvailableTrackerId()
	{
		int _currentId = 0;
		
		while (existingTrackers.ContainsValue (_currentId)) 
		{
			_currentId++;
		}

		return _currentId;
	}

	void Update()
	{
		List<DamageReceiveDirectionTracker> _expiredTrackers = new List<DamageReceiveDirectionTracker> ();
		
		foreach (KeyValuePair<DamageReceiveDirectionTracker, int> entry in existingTrackers) 
		{
			if (entry.Key.existTimeCounter >= trackerExistTime) 
			{
				uiControl.RemoveTracker (entry.Value);
				_expiredTrackers.Add (entry.Key);
			} 
			else 
			{
				Vector3 _cross = transform.InverseTransformDirection ((entry.Key.damageDealer.transform.position - transform.position).normalized);
				uiControl.UpdateTrackerDirection(entry.Value, new Vector2(_cross.x, _cross.z));
			}
			
			entry.Key.existTimeCounter += Time.deltaTime;
		}

		foreach (DamageReceiveDirectionTracker _candidate in _expiredTrackers) 
		{
			existingTrackers.Remove (_candidate);
		}

		if (existingTrackers.Count == 0)
			this.enabled = false;
	}
}
