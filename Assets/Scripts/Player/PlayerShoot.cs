﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: PlayerShoot.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 16/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Cee Jay	| 16/05/16	|	- added shooting functions
/// 					  17/05/16	|	- modified OnShootHit to support ragdoll colliders
/// 					  18/05/16	|	- reduce zeroing issue between crosshair raycast point and bullet hit point
/// 									  (still occur if the shooter is too close to the target)
/// 					  23/05/16	|	- OnShootHit check for IHittable instead of Destructible
/// 									- Shoot & Reload now return boolean 
/// 									- added GetCurrentClip function
/// 					  24/05/16	|	- remove animation components
/// 									- temporarily pull weapon data from weapondatabase
/// 					  25/05/16	|	- fixed incorrect shoot orientation which was initially based on camera properties
/// 									- shoot function now will take the current camera data from client side for calculation
/// 					  26/05/16	|	- restructure the shooting logic, remove redundant cmd and rpc parameters
/// 									- added reticle bloom effect
/// 									- added recoil effect (vertical only)
/// 					  27/05/16	|	- modified recoil logic, reset after every shot 
///			| Patrick	| 27/05/16	|	- use new ui for ammo.
/// 					| 31/05/16	|	- change naming convention of ammo update ui function
/// 		| Cee Jay	| 01/06/16	|	- reworked shooting structure with server authentication
/// 									- fixed bloom and recoil logic (Beta)
/// 					| 02/06/16	|	- set bullet facing rotation according to velocity
/// 		| Jack		| 03/06/16	|	- SpawnBullet & FireBullet now take a list of gameobject instead of one game object	
/// 		| Cee Jay	| 03/06/16	|	- Restructure and separate into several functions for shooting logic
/// 									- reticle bloom and recoil only apply after each shot instead of each bullet spawns
/// 		| Cee Jay	| 06/06/16	|	- reworked on recoil logic
/// 									- added recoil recovery for better realism
/// 									- disable recoil recovery if constantly fire to avoid camera jitter
/// 									- added shotgun fire logic
/// 									- restructure fire logic and added pellet spread logic 
/// 		| Cee Jay	| 08/06/16	|	- added SwitchWeapon, PickUpWeapon, ReplenishAmmo functions
/// 		| Patrick	| 09/06/16	|	- added swap weapon ui related functions
/// 		| Cee Jay	| 16/06/16	|	- added rocket launcher shooting mechanism
/// 									- added shield collision logic support
/// 		| Cee Jay	| 23/06/16	|	- added SetPlayerIdentifier
/// 					  27/16/16	|	- modified SetupBulletCallback
/// 								|	- updated BulletImpactLogic, PlayerIdentifier from <Bullet> is used instead of PlayerIdentifier from current class
/// 		| Patrick	| 11/07/16	| 	- add isLocalPlayer statement on weaponSlotUI's call.
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

public class PlayerShoot : NetworkBehaviour {

	private Weapon currentWeapon;

	private List<List<Weapon>> weaponLists = new List<List<Weapon>>{new List<Weapon>(),new List<Weapon>(),new List<Weapon>(),new List<Weapon>()};

	private int currentWeaponIndexSlot;
	private int currentWeaponTierIndex;

	private byte playerIdentifier;

	[SerializeField]
	private Camera cam = null;

	[SerializeField]
	private LayerMask mask;

	[SerializeField]
	private GameObject bulletSpawnPoint = null;

	private bool isRpmCoolDown;
	private bool isReloading;
	private bool isOutOfAmmo;

	//Gun properties
	private float totalReticleBloomPenalty = 0f;
	private float totalRecoilPenalty = 0f;
	private float accuracyRecoveryRate = 0.005f;

	public Vector3 recoilValue = Vector3.zero;
	private Vector3 toRecoilValue = Vector3.zero;
	private float recoilTimer;
	private const float recoilExpiryTime = .1f;

	//Component
//	private PlayerAnimationManager animationManager;
	private WeaponStatusUIControl weaponStatusUIControl;
	private WeaponSlotsUIControl weaponSlotsUIControl;

	//Testing
	System.Random randSeed = new System.Random(1234);
	private bool isServerCalculate;

	void FixedUpdate(){

		CheckForBloomAndRecoil();

//		if(Input.GetKeyUp(KeyCode.Space)){
//			ShowWeaponList();
//		}

//		if(Input.GetKeyUp(KeyCode.Z)){
//			PickUpWeapon(WeaponDatabase.instance.GetWeapon(WeaponType.MP5),30);
//		}
//
//		if(Input.GetKeyUp(KeyCode.X)){
//			PickUpWeapon(WeaponDatabase.instance.GetWeapon(WeaponType.AUG),30);
//		}
//
//		if(Input.GetKeyUp(KeyCode.C)){
//			PickUpWeapon(WeaponDatabase.instance.GetWeapon(WeaponType.AK47),30);
//		}
//
//		if(Input.GetKeyUp(KeyCode.V)){
//			PickUpWeapon(WeaponDatabase.instance.GetWeapon(WeaponType.M16),30);
//		}
//
//		if(Input.GetKeyUp(KeyCode.Alpha1)){
//			Debug.Log("Pressed 1");
//			SwitchWeapon(1);
//		}

//		if(Input.GetKeyUp(KeyCode.Alpha2)){
//			Debug.Log("Pressed 2");
//			SwitchWeapon(2);
//		}

//		if(Input.GetKeyUp(KeyCode.Alpha3)){
//			Debug.Log("Pressed 3");
//			SwitchWeapon(3);
//		}

//		if(Input.GetKeyUp(KeyCode.Alpha4)){
//			Debug.Log("Pressed 4");
//			SwitchWeapon(4);
//		}
	}

	public void Init(){

		weaponStatusUIControl = GameManager.instance.uiManager.weaponInfoUIControl.weaponStatusUIControl;
		weaponSlotsUIControl = GameManager.instance.uiManager.weaponInfoUIControl.weaponSlotsUIControl;
		weaponSlotsUIControl.InitializeWeaponSlots (new Dictionary<int, List<string>> ());
		PickUpWeapon (WeaponDatabase.instance.GetWeapon(WeaponType.MP5), 100);

		SwitchWeapon (WeaponDatabase.instance.GetWeapon(WeaponType.MP5).weaponTier);

		RefreshAmmoUI();
	}

	//Register player identifier from PlayerManager
	public void SetPlayerIdentifier(byte _identifier){
		playerIdentifier = _identifier;
	}

	public bool Shoot(){
		
		return true;
	}

	public bool Reload(){

		return true;
	}

	public int GetCurrentClip(){

		return currentWeapon.currentClipSize;
	}

	#region Update weapon inventory
	//Swap based on weapon tier type
	public void SwitchWeapon(int _inputIndex){

		//consider if input index start from 1
		int _index = _inputIndex - 1;

		//check if theres any available weapon under this index list
		if(weaponLists[_index].Count == 0){
			Debug.Log("No weapon found under this tier");
			return;
		}

		//selecting the same tier and increase the index count
		if(currentWeaponTierIndex == _index){
			
			if(weaponLists[currentWeaponTierIndex].Count > currentWeaponIndexSlot +1){
				currentWeaponIndexSlot++;
			}else{
				currentWeaponIndexSlot = 0;
			}

		}else{//swapping to different tier
			currentWeaponTierIndex = _index;
			currentWeaponIndexSlot = 0;
		}


		SwapWeapon(currentWeaponTierIndex,currentWeaponIndexSlot);
	}

	private void SwapWeapon(int _tierIndex, int _listIndex){

		currentWeapon = weaponLists[_tierIndex][_listIndex];

		if (isLocalPlayer) 
		{
			weaponSlotsUIControl.UpdateWeaponSlotUI (_tierIndex, _listIndex);
			weaponStatusUIControl.SetupWeaponStatus (currentWeapon.weaponTexturePath, currentWeapon.currentClipSize, currentWeapon.currentClipPool);
		}

//		Debug.LogError("Swap to " + _tierIndex + " : " + _listIndex);
	}

	public void PickUpWeapon(Weapon _weapon,int _ammo){
		//Check if consist current pick up weapon
		foreach(List<Weapon> _weaponList in weaponLists){

			if(_weaponList.Find(w => w.weaponName == _weapon.weaponName) != null){
				Debug.Log("Player already owned this weapon");

				//increase ammo count
				Weapon _thisWeapon =_weaponList[_weaponList.FindIndex(w => w.weaponName == _weapon.weaponName)];
				ReplenishAmmo(_thisWeapon,_ammo);

//				Debug.Log(_weaponList.IndexOf(_weapon));

				Debug.Log(_weaponList.FindIndex(w => w.weaponName == _weapon.weaponName));

				return;
			}
		}

		//add to list based on tier
		weaponLists[_weapon.weaponTier-1].Add(_weapon);

		if(isLocalPlayer)
			weaponSlotsUIControl.AddNewWeaponNode (_weapon.weaponTier-1, _weapon.weaponTexturePath);
		
		Debug.Log("Acquire weapon " + _weapon.weaponName);
	}

	private void ShowWeaponList(){

		Debug.LogError(gameObject.name + " ========================= ");

		for (int i = 0; i < weaponLists.Count; i++) {

			Debug.LogError("Tier Count: " + i);
			List<Weapon> _templist = weaponLists[i];

			for (int j = 0; j < _templist.Count; j++) {
				Debug.LogError("Index slot count: " + j);
			}
		}
	}

	private void ReplenishAmmo(Weapon _weapon, int _ammo){

		if(_weapon.currentClipPool < _weapon.maxClipPool){
			_weapon.currentClipPool += _ammo;

			//reset to maximum clip pool if it exceeds the cap
			if(_weapon.currentClipPool > _weapon.maxClipPool){
				_weapon.currentClipPool = _weapon.maxClipPool;
			}
		}

		Debug.Log("Replenish ammo");
	}


	#endregion

	private void ReloadClip(){
		

		int reloadClipSize = currentWeapon.maxClipSize - currentWeapon.currentClipSize;

		if(currentWeapon.currentClipPool > 0){
			if(currentWeapon.currentClipPool <= reloadClipSize){
				currentWeapon.currentClipSize += currentWeapon.currentClipPool;
				currentWeapon.currentClipPool -= currentWeapon.currentClipPool;
			}else{
				currentWeapon.currentClipPool -= reloadClipSize;
				currentWeapon.currentClipSize = currentWeapon.maxClipSize;
			}
		}else{
			isOutOfAmmo = true;
		}
	}

	private void RefreshAmmoUI(){
		if(isLocalPlayer)
			weaponStatusUIControl.UpdateAmmoCount(currentWeapon.currentClipSize,currentWeapon.currentClipPool);
	}

	private List<GameObject> GeneratePellet(Weapon _weapon){
		List<GameObject> _bulletList = new List<GameObject> ();

		//multiple pellets
		if(_weapon is Shotgun){
			for (int i = 0; i < ((Shotgun)_weapon).pelletCount; i++) {
				GameObject bullet = (GameObject)Instantiate(Resources.Load(_weapon.bulletPrefabPath),bulletSpawnPoint.transform.position,Quaternion.identity);
				_bulletList.Add (bullet);
			}

		//single pellet
		}else if(_weapon is Weapon){
			GameObject bullet = (GameObject)Instantiate(Resources.Load(_weapon.bulletPrefabPath),bulletSpawnPoint.transform.position,Quaternion.identity);
			_bulletList.Add (bullet);

		}

		return _bulletList;
	}
		
	public List<GameObject> SpawnBullet ()
	{
		if(isRpmCoolDown) {
			return null;
		}

		//Remove recoil recovering if constantly fire
		ResetRecoil();

		isRpmCoolDown = true;
		currentWeapon.currentClipSize -= 1;
		RefreshAmmoUI();

		//Get total bullet count based on weapon class
		List<GameObject> bulletList = new List<GameObject> (GeneratePellet(currentWeapon));

		StartCoroutine ("Shooting");
		return bulletList;
	}

	IEnumerator Shooting ()
	{
		yield return new WaitForSeconds (60 / currentWeapon.roundPerMinute);
		isRpmCoolDown = false;
	}

	public void ReloadBullet (bool _hasAuthority)
	{
		if (isReloading) {
			return;
		}

		isReloading = true;
		StartCoroutine ("Reloading");
	}

	IEnumerator Reloading ()
	{
		yield return new WaitForSeconds (currentWeapon.reloadTime);
		ReloadClip ();
		RefreshAmmoUI ();

		isReloading = false;
	}

	#region Bloom & Recoil
	private void CheckForBloomAndRecoil(){

		//Reticle bloom update
		if(totalReticleBloomPenalty > 0 && totalReticleBloomPenalty <= 1){
			totalReticleBloomPenalty -= accuracyRecoveryRate;

		}else if(totalReticleBloomPenalty > 1){
			totalReticleBloomPenalty = 1;

		}else{
			totalReticleBloomPenalty = 0;
		}

		//Recoil update
		if(toRecoilValue != Vector3.zero){

			recoilTimer += Time.fixedDeltaTime;

			//Recoil recovery
			if(recoilTimer > recoilExpiryTime / 2 && recoilTimer < recoilExpiryTime){

				recoilValue = Vector3.Slerp(recoilValue,new Vector3(toRecoilValue.x / 3 * -1, 0,0), Time.fixedDeltaTime * 50);

				//				Debug.Log("recover from recoil");

				//Recoil trigger
			}else if(recoilTimer < recoilExpiryTime){

				recoilValue = Vector3.Slerp(recoilValue,toRecoilValue, Time.fixedDeltaTime * 100);

				//				Debug.Log("up recoil");

				//Recoil expires
			}else if(recoilTimer > recoilExpiryTime){

				ResetRecoil();

				//				Debug.Log("stop recoil");

			}
		}
	}

	private Vector3 ComputeSpread(float _value){
		Vector3 spreadVec3;

		float spreadX;
		float spreadY;
		float spreadZ;

		spreadX = (float) (randSeed.NextDouble() * ((Double)_value - (Double)(-_value)) + (-_value));
		spreadY = (float) (randSeed.NextDouble() * ((Double)_value - (Double)(-_value)) + (-_value));
		spreadZ = (float) (randSeed.NextDouble() * ((Double)_value - (Double)(-_value)) + (-_value));

		spreadVec3 = new Vector3(spreadX,spreadY,spreadZ);

		return spreadVec3;
	}

	private Vector3 GenerateReticleBloomVec3(float _reticleBloomPenalty){

		//return if there is no penalty
		if(_reticleBloomPenalty <= 0f){
			return Vector3.zero;
		}

		return ComputeSpread(_reticleBloomPenalty);
	}

	private Vector3 GeneratePelletSpreadFactor(float _pelletSpreadRadius){

		if(_pelletSpreadRadius <= 0f){
			return Vector3.zero;
		}

		return ComputeSpread(_pelletSpreadRadius);
	}

	private void AccumulateBloom(){
		if(currentWeapon.reticleBloomPenalty <= 0){
			return;
		}

		totalReticleBloomPenalty += currentWeapon.reticleBloomPenalty;
	}

	private void ApplyRecoil(){

		if(currentWeapon.recoilPenalty <= 0){
			return;
		}

		totalRecoilPenalty = currentWeapon.recoilPenalty;

		toRecoilValue = new Vector3(-(float) (randSeed.NextDouble() * ((Double)totalRecoilPenalty - (Double)(totalRecoilPenalty/2)) + (Double)(totalRecoilPenalty/2)),0,0);
	}

	private void ResetRecoil(){
		recoilValue = Vector3.zero;
		toRecoilValue = Vector3.zero;
		recoilTimer = 0;
	}

	#endregion

	public void FireBullet (byte _shotID, bool _isServerCalculate, List<GameObject> _bullet)
	{
		if (_bullet.Count <= 0)
			return;

		isServerCalculate = _isServerCalculate;

		List<GameObject> bulletList = new List<GameObject> (_bullet);

		//Calculate Reticle Bloom
		Vector3 bloomVec3 = GenerateReticleBloomVec3(totalReticleBloomPenalty);

		while (bulletList.Count > 0) {
			
			GameObject bullet = bulletList[0];
			bulletList.RemoveAt (0);

			RaycastHit _hit;
			Vector3 _fromPosition = bulletSpawnPoint.transform.position;
			Vector3 _toPosition;
			Vector3 _direction;

			if(Physics.Raycast(cam.transform.position ,cam.transform.forward, out _hit, currentWeapon.optimalRange, mask)){
				_toPosition = _hit.point;
				_direction = _toPosition - _fromPosition;
			}else{

				//out of bound
				_direction = cam.transform.forward * 10f;
			}

			//Apply reticle bloom to direction
			_direction += bloomVec3;

			//Apply pellet spread logic
			//Calculate Pellet Spread Factor
			if(currentWeapon is Shotgun){
				_direction += GeneratePelletSpreadFactor(((Shotgun)currentWeapon).spreadFactor);
			}

			//Apply velocity force
			bullet.GetComponent<Rigidbody>().AddForce(_direction * currentWeapon.projectileSpeed);

			//adjust bullet facing direction
			bullet.transform.rotation = Quaternion.LookRotation(_direction);

			//bullet callback setup
			SetupBulletCallback(bullet);

//			ShootRay(_hit.point);

//			ShootRayNew (_direction, 1f);
		}

		//Update bloom and recoil logic after each shot
		AccumulateBloom();

		ApplyRecoil();
	}

	private void SetupBulletCallback(GameObject _bullet){
		_bullet.GetComponent<Bullet> ().CanSelfHarm(false,gameObject);
		_bullet.GetComponent<Bullet> ().SetCurrentWeapon (currentWeapon);
		_bullet.GetComponent<Bullet> ().SetShooterIdentifier (playerIdentifier);

		//if weapon is rocket launcher
		if (currentWeapon is RocketLauncher) {
			_bullet.GetComponent<Rocket> ().SetExplosionRadius (((RocketLauncher)currentWeapon).areaOfEffect);
		}

		_bullet.GetComponent<Bullet>().OnBulletImpact = null;
		_bullet.GetComponent<Bullet>().OnBulletImpact += BulletImpactLogic;
	}

	void ShootRay(Vector3 _point) {

		float _travelDistance = 3f;

		Vector3 direction = RandomCircle(_point,1) - bulletSpawnPoint.transform.position;

//		direction = _point - bulletSpawnPoint.transform.position;

		Vector3 finalDirection = direction + direction.normalized * _travelDistance;

		Ray r = new Ray(bulletSpawnPoint.transform.position,finalDirection);

		RaycastHit hit;

		if(Physics.Raycast(r,out hit)){
			Debug.DrawLine(bulletSpawnPoint.transform.position, hit.point, Color.white, 1f);
		}

//		Vector3 direction = new Vector3( Random.Range(-splash,splash)*Mathf.Cos(Random.Range(0,2*Mathf.PI)),Random.Range(-splash,splash)*Mathf.Sin(Random.Range(0,2*Mathf.PI)),0 );
	}

	Vector3 ShootRayNew(Vector3 _direction, float _spreadFactor){
		Vector3 new_direction;

		new_direction = _direction + new Vector3 (UnityEngine.Random.Range (-_spreadFactor, _spreadFactor), UnityEngine.Random.Range (-_spreadFactor, _spreadFactor), UnityEngine.Random.Range (-_spreadFactor, _spreadFactor));

		Debug.Log ("Old " + _direction);

		Debug.Log ("New " + new_direction);



//		new_direction = GetRandomInsideCone (_spreadFactor) * _direction;

//		new_direction = _direction;

		Ray r = new Ray (bulletSpawnPoint.transform.position, new_direction);

		RaycastHit hit;

		if(Physics.Raycast(r,out hit)){
			Debug.DrawLine(bulletSpawnPoint.transform.position, hit.point, Color.white, 1f);
		}

		return new_direction;
	}

	Quaternion GetRandomInsideCone(float conicAngle) {
		// random tilt right (which is a random angle around the up axis)
		Quaternion randomTilt = Quaternion.AngleAxis(UnityEngine.Random.Range(0f, conicAngle), Vector3.up);

		// random spin around the forward axis
		Quaternion randomSpin = Quaternion.AngleAxis(UnityEngine.Random.Range(0f, 360f), Vector3.forward);

		// tilt then spin
		return (randomSpin * randomTilt);
	}

	Vector3 RandomCircle(Vector3 _center, float _radius){

		float _ang = UnityEngine.Random.value * 360;
		Vector3 _pos;
		float _posX,_posY,_posZ;

		_posX = _center.x + _radius * Mathf.Sin(_ang * Mathf.Deg2Rad);
		_posY = _center.y + _radius * Mathf.Cos(_ang * Mathf.Deg2Rad);
		_posZ = _center.z;

		_pos = new Vector3(_posX,_posY,_posZ);

		return _pos;
	}

	private void BulletImpactLogic(Collider _collider, Bullet _bullet){

		if(_collider.transform.GetComponent<IHittable>() != null){
			_collider.transform.GetComponent<IHittable>().Hit(GetComponent<Collider>(),_bullet,isServerCalculate);
		}else if(_collider.transform.root.GetComponent<IHittable>() != null){
			_collider.transform.root.GetComponent<IHittable>().Hit(_collider,_bullet,isServerCalculate);
		}

		Destroy (_bullet.bulletObj);
	}

	public bool GetCanFire ()
	{
		if (isReloading || isOutOfAmmo || currentWeapon.currentClipSize <= 0)
			return false;

		return true;
	}

	public bool GetCanReload ()
	{
		if(isReloading || currentWeapon.currentClipSize == currentWeapon.maxClipSize || isOutOfAmmo)
			return false;

		return true;
	}
}
