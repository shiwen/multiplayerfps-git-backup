﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: PlayerAnimationManager.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 16/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Cee Jay	| 16/05/16	|	- added animation functions
/// 									- added both command and clientrpc functions to sync animation
/// 									- added death animation support
/// 									- modified CmdPlayDead & RpcPlayDead to support headshot dead animation
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerAnimationManager : NetworkBehaviour {

	[SerializeField]
	private Animator characterAnimator;
	[SerializeField]
	private Animator weaponAnimator;

	private float para_xBlend;
	private float para_zBlend;

	private bool isRunning;

	private float sprintLerp;

	void LateUpdate(){
		UpdateAnimatorParameters();
	}

	void UpdateAnimatorParameters(){

		//ease in / out between sprint and walk animation
		if(isRunning){
			if(sprintLerp < 1){
				sprintLerp += 0.1f;
			}else{
				sprintLerp = 1f;
			}

		}else{
			if(sprintLerp > 0){
				sprintLerp -= 0.1f;
			}else{
				sprintLerp = 0f;
			}
		}

//		characterAnimator.SetFloat("xBlend",para_xBlend);
//		characterAnimator.SetFloat("zBlend",para_zBlend + sprintLerp);
//
//		weaponAnimator.SetFloat("xBlend",para_xBlend);
//		weaponAnimator.SetFloat("zBlend",para_zBlend + sprintLerp);
	}

	void UpdateMovementAnimation (object _o)
	{
		NetworkMovement.Inputs inputs = (NetworkMovement.Inputs)_o;

		isRunning = inputs.sprint;
		characterAnimator.SetFloat("xBlend",inputs.horizontal);
		characterAnimator.SetFloat("zBlend",inputs.vertical + sprintLerp);

		weaponAnimator.SetFloat("xBlend",inputs.horizontal);
		weaponAnimator.SetFloat("zBlend",inputs.vertical + sprintLerp);
	}

	void UpdateActionAnimation (object _o)
	{
		NetworkAction.Inputs inputs = (NetworkAction.Inputs)_o;

		if (inputs.reload) {
			characterAnimator.SetTrigger("reload");
			weaponAnimator.SetTrigger("reload");
		}

		if (inputs.shoot) {
			characterAnimator.SetTrigger("shoot");
			weaponAnimator.SetTrigger("shoot");
		}
	}

	public void Shoot ()
	{
		characterAnimator.SetTrigger("shoot");
		weaponAnimator.SetTrigger("shoot");
	}

	public void Reload ()
	{
		characterAnimator.SetTrigger("reload");
		weaponAnimator.SetTrigger("reload");
	}

	void SetDefault(){
		weaponAnimator.gameObject.SetActive(true);
	}

	[Command]
	public void CmdTriggerSprint(bool _value){
		RpcTriggerSprint(_value);
	}

	[ClientRpc]
	private void RpcTriggerSprint(bool _value){
		isRunning = _value;
	}

	[Command]
	public void CmdSetXBlend(float _value){
		RpcSetXBlend(_value);
	}

	[ClientRpc]
	public void RpcSetXBlend(float _value){
		para_xBlend = _value;
	}

	[Command]
	public void CmdSetZBlend(float _value){
		RpcSetZBlend(_value);
	}

	[ClientRpc]
	public void RpcSetZBlend(float _value){
		para_zBlend = _value;
	}

	[Command]
	public void CmdPlayReload(){
		RpcPlayReload();
	}

	[ClientRpc]
	private void RpcPlayReload(){
		characterAnimator.SetTrigger("reload");
		weaponAnimator.SetTrigger("reload");
	}

	[Command]
	public void CmdPlayShoot(){
		RpcPlayShoot();
	}

	[ClientRpc]
	private void RpcPlayShoot(){
		characterAnimator.SetTrigger("shoot");
		weaponAnimator.SetTrigger("shoot");
	}

	[Command]
	public void CmdPlayDead(bool _vitalKill){
		RpcPlayDead(_vitalKill);
	}

	[ClientRpc]
	public void RpcPlayDead(bool _vitalKill){
		if(_vitalKill){
			characterAnimator.SetTrigger("headshotdead");
		}else{
			characterAnimator.SetTrigger("dead");
		}
			
		weaponAnimator.gameObject.SetActive(false);
	}
}
