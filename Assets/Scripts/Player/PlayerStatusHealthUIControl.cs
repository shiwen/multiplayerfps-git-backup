﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStatusHealthUIControl : MonoBehaviour 
{
	public HealthIndicatorUIController healthIndicatorUIController;
	public PlayerStatusHealthBarUIControl healthBarUIControl;

	public void UpdateHealthUI(string _currentHealthText, float _currentRatio)
	{
		healthBarUIControl.UpdateHealthBar (_currentHealthText, _currentRatio);
		healthIndicatorUIController.UpdateHealthUI(_currentRatio);
	}
}
