﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: PlayerSetup.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 30/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Cee Jay	| 30/05/16	|	- added various function for player setup
/// 									- added DisableComponents for remote player
/// 					| 02/06/16	|	- added spectator mode support
/// 									- rearrange init sequences
/// 					| 02/08/16	|	- re-enable scene camera only works on local player
/// 									- set player camera to MainCamera on local player
/// </summary> ------------------------------------------------------------------------------------------------------------------


using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerSetup : NetworkBehaviour {

	[SerializeField]
	Behaviour[] componentsToDisable;

	Camera sceneCamera;

	[SerializeField]
	string remoteLayerName = "RemotePlayer";

	private bool isSpectator;

	//On player connected to server
	public override void OnStartClient(){
		string _netId = GetComponent<NetworkIdentity>().netId.ToString();

		GameManager.LockCursor(true);

		Init();
	}

	void Start(){
		sceneCamera = Camera.main;

		if(!isLocalPlayer){
			DisableComponents();
			AssignRemotePlayer();
		}else{
			
			if(sceneCamera != null){
				sceneCamera.gameObject.SetActive(false);
			}

			GetComponentInChildren<Camera> ().gameObject.tag = "MainCamera";
		}
	}

	private void Init(){
		//Check if player is spectator
		if(GetComponent<Spectator>() != null){
			isSpectator = true;
		}else{
			isSpectator = false;
		}

		if(!isSpectator)
			GameManager.RegisterPlayerCamera(GetComponentInChildren<Camera>());
	}


	private void DisableComponents(){
		for (int i = 0; i < componentsToDisable.Length; i++) {
			componentsToDisable[i].enabled = false;
		}
	}

	void OnDisable()
	{
		if (isLocalPlayer) {
			if (sceneCamera != null) {
				sceneCamera.gameObject.SetActive (true);
			}

			GameManager.LockCursor(false);
		}

		if(!isSpectator)
			GameManager.UnregisterPlayerCamera(GetComponentInChildren<Camera>());
	}

	void AssignRemotePlayer(){
		gameObject.layer = LayerMask.NameToLayer(remoteLayerName);
	}
}
