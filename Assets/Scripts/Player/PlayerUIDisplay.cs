﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerUIDisplay : MonoBehaviour {

	[SerializeField] private Canvas uiCanvas;

	[SerializeField] private Text ammoText;
	[SerializeField] private Text healthText;

	void OnEnable(){
		uiCanvas.gameObject.SetActive(true);
	}

	void OnDisable(){
		uiCanvas.gameObject.SetActive(false);
	}

	public void SetAmmoDisplay(int _currentAmmo, int _maxAmmo){
		ammoText.text = _currentAmmo.ToString() + " / " + _maxAmmo.ToString();
	}

	public void SetHealth(int _health){
		healthText.text = _health.ToString();
	}
}
