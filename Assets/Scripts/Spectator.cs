﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: Spectator.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 30/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Cee Jay	| 30/05/16	|	- added Locomotion
/// 					| 02/06/16	|	- added basic spectate player camera feature 
/// 									- added return to spectator view function
/// 					| 29/06/16	|	- added DisplayAllPlayerView function and DisableAllPlayerView function
/// 									- DisplayAllPlayerView will show all the current active player camera view in grids
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Spectator : NetworkBehaviour {

	//initial speed

	[SerializeField]
	private int moveSpeed =20;

	private int currentMoveSpeed;

	private Camera cam;

	[SerializeField]
	private float mouseSensitivity;

	private float yaw = 0.0f;
	private float pitch = 0.0f;

	private Camera currentViewCamera;

	[SerializeField]
	private SpectatorViewMode viewMode;

	private int cameraIndex = 0;

	private int cameraViewRowCount = 2;

	// Use this for initialization
	void Start () {
		cam = GetComponentInChildren<Camera>();
		currentViewCamera = cam;
		viewMode = SpectatorViewMode.SpectatorView;

		//Disable this object if its not local player
		if(!isLocalPlayer){
			gameObject.SetActive(false);
		}
	}

	// Update is called once per frame
	void Update () {

		if(!isLocalPlayer){
			return;
		}

		//Restrict locomotion if its not using default camera view
		if(currentViewCamera == cam){

			PerformMovement();
			PerformRotation();
		}


		if(Input.GetKeyUp(KeyCode.Space)){
			SwitchCamera();
		}

		if(Input.GetKeyUp(KeyCode.LeftControl)){
			ReturnToSpectatorCamera();
		}

		if (Input.GetKeyUp (KeyCode.LeftShift)) {
			DisplayAllPlayerView ();
		}
	}

	//Locomotion
	#region Locomotion
	void PerformMovement(){
		//press shift to move faster
		if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
		{
			currentMoveSpeed = moveSpeed * 2; 

		}
		else
		{
			currentMoveSpeed = moveSpeed; 
		}
			
		//move camera to the left
		if(Input.GetKey(KeyCode.A))
		{
			transform.position = transform.position + cam.transform.right *-1 * currentMoveSpeed * Time.deltaTime;
		}

		//move camera backwards
		if(Input.GetKey(KeyCode.S))
		{
			transform.position = transform.position + cam.transform.forward *-1 * currentMoveSpeed * Time.deltaTime;

		}
		//move camera to the right
		if(Input.GetKey(KeyCode.D))
		{
			transform.position = transform.position + cam.transform.right * currentMoveSpeed * Time.deltaTime;

		}
		//move camera forward
		if(Input.GetKey(KeyCode.W))
		{

			transform.position = transform.position + cam.transform.forward * currentMoveSpeed * Time.deltaTime;
		}
		//move camera upwards
		if(Input.GetKey(KeyCode.E))
		{
			transform.position = transform.position + cam.transform.up * currentMoveSpeed * Time.deltaTime;
		}
		//move camera downwards
		if(Input.GetKey(KeyCode.Q))
		{
			transform.position = transform.position + cam.transform.up * -1 *currentMoveSpeed * Time.deltaTime;
		}
	}

	void PerformRotation(){
		
		yaw += mouseSensitivity * Input.GetAxis("Mouse X");
		pitch += mouseSensitivity * Input.GetAxis("Mouse Y");

		transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
	}

	#endregion

	private void ChangeCameraView(Camera _cam){
		currentViewCamera.enabled = false;
		currentViewCamera = _cam;
		currentViewCamera.enabled = true;
	}

	//iterate through player camera views
	private void SwitchCamera(){

		//No active player camera registered
		if(GameManager.cameraList.Count == 0){
			return;
		}

		//disable in overall view
		if (viewMode == SpectatorViewMode.OverallView) {
			return;
		}


		viewMode = SpectatorViewMode.PlayerView;

		//Reset if camera index exceed the total player camera count
		if(cameraIndex + 1 > GameManager.cameraList.Count){
			cameraIndex = 0;
		}

		//Switch to player camera 
		if(cameraIndex + 1 <= GameManager.cameraList.Count){

			ChangeCameraView(GameManager.cameraList[cameraIndex]);

			cameraIndex++;
		}
	}

	//return to spectator original camera view
	private void ReturnToSpectatorCamera(){

		viewMode = SpectatorViewMode.SpectatorView;

		DisableAllPlayerView();

		ChangeCameraView(cam);


	}

	private void DisableAllPlayerView(){
		for (int i = 0; i < GameManager.cameraList.Count; i++) {
			GameManager.cameraList [i].enabled = false;

			GameManager.cameraList [i].rect = new Rect (0, 0, 1, 1);
		}
	}

	//show all active players
	private void DisplayAllPlayerView(){

		viewMode = SpectatorViewMode.OverallView;

		//disable spectator's camera
		cam.enabled = false;

		//start off default with 2 by 2, increase row count whenever camera count exceeds the current value
		if (GameManager.cameraList.Count > cameraViewRowCount * cameraViewRowCount) {
			cameraViewRowCount++;
		}

		//get the offset value for viewport rect
		float _offset = 1f / (float)cameraViewRowCount;

		//viewport value parameter
		float _x, _y, _w, _h;

		//row count
		float _row = 0;
		float _column = 0;

		for (int i = 0; i < GameManager.cameraList.Count; i++) {

			if (_column == cameraViewRowCount) {
				_row++;
				_column = 0;
			}
				
			_x = _column * _offset;
			_y = _row * _offset;

			_w = _offset;
			_h = _offset;

			_column++;

			//Final output
			GameManager.cameraList [i].rect = new Rect (_x, _y, _w, _h);
			GameManager.cameraList [i].enabled = true;
		}

	}
}

enum SpectatorViewMode
{
	None,
	PlayerView,
	OverallView,
	SpectatorView
}
