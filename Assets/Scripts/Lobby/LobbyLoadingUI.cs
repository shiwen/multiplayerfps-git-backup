﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: LobbyUIWorldCamera.cs
/// FUNCTION		: - Controls loading dialogue.
/// 				  - How to use: Call "AddNewLoadingRequest" function, pass a checking function (return true if loading is completed).
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LobbyLoadingUI : MonoBehaviour 
{
	public delegate bool LobbyLoadingFunction();
	public delegate void VoidFunction();
	public class LoadingRequestComponent
	{
		public LobbyLoadingFunction loadingCheckerFunction;
		public VoidFunction loadingCompletedFunction;

		public LoadingRequestComponent(LobbyLoadingFunction _loadingCheckerFunction, VoidFunction _loadingCompletedFunction)
		{
			loadingCheckerFunction = _loadingCheckerFunction;
			loadingCompletedFunction = _loadingCompletedFunction;
		}
	}

	public Text loadingText;
	public List<LoadingRequestComponent> loadingRequestComponents = new List<LoadingRequestComponent>();

	public void AddNewLoadingRequest(LoadingRequestComponent _loadingRequestComponent, string _loadingText, bool _showLoadingUI)
	{
		loadingRequestComponents.Add (_loadingRequestComponent);
		loadingText.text = _loadingText;

		if (loadingRequestComponents.Count == 1) 
		{
			InvokeRepeating ("LoadingRequestComponentChekingUpdate", 0, 0.3f);

			if(_showLoadingUI)
				gameObject.SetActive (true);
		}			
	}

	private void LoadingRequestComponentChekingUpdate()
	{
		List<int> _completedComponentsIndex = new List<int> ();
		for(int i = 0; i < loadingRequestComponents.Count; i++) 
		{
			if (loadingRequestComponents[i].loadingCheckerFunction ()) 
			{
				loadingRequestComponents [i].loadingCompletedFunction ();
				loadingRequestComponents.RemoveAt (i);
				i--;
			}
		}

		if (loadingRequestComponents.Count == 0) 
		{
			CancelInvoke ();
			gameObject.SetActive (false);
		}
	}
}
