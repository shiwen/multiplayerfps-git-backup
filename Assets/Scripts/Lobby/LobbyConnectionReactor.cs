﻿using UnityEngine;
using System.Collections;

public class LobbyConnectionReactor : NetworkConnectionEventReactor 
{
	public LobbyController lobbyController;
	private MultiPlayerFPSNetworkConnectionManager connectionManager;

	void Start()
	{
		connectionManager = MultiPlayerFPSNetworkConnectionManager.GetInstance ();
	}

	public override void OnPreStartGame ()
	{
		foreach (MultiPlayerFpsNetworkPlayerInstance _candidate in connectionManager.GetAllMultiplayerFpsNetworkPlayer()) 
		{
			_candidate.playerStatus = NetworkPlayerInstanceStatus.Playing;
		}
	}

	public override void OnStartGameCalled ()
	{
		lobbyController.StartGame ();
	}

	public override void OnDisconnectCalled ()
	{
		lobbyController.ExitFromRoom ();
	}

	public override void OnConnectedToRoom (bool _gameIsStarted, bool _isHost)
	{
		if (lobbyController.directChangeSceneWhileJoinStartedGame)
			MultiPlayerFPSNetworkConnectionManager.GetInstance ().RequestPostStartGameJoinGame ();
		else
			lobbyController.ChangeToRoomState (_gameIsStarted, _isHost);
	}

	public override void PostJoinedRoomSync(bool _gameIsStarted)
	{
		lobbyController.lobbyUI.lobbyRoomUI.SetupCurrentButtonSection(NetworkConnectionManager.instance.startAsHost, lobbyController.readyRequired, _gameIsStarted);
	}

	public override void OnServerDiscovered(DiscoveredServer _discoveredServer)
	{
		lobbyController.lobbyUI.AddRoomObject (_discoveredServer.ipAddress, _discoveredServer.roomName, _discoveredServer.numPlayers, _discoveredServer.maxPlayers);
	}

	public override void OnServerRemoved(DiscoveredServer _removedServer)
	{
		lobbyController.lobbyUI.RemoveRoomObject (_removedServer.ipAddress);
	}

	public override void OnPlayerInstanceAdded (bool _isHost, NetworkPlayerInstance _networkPlayerInstance)
	{
		if (_isHost)
		{
			MultiPlayerFpsNetworkPlayerInstance _networkPlayer = (MultiPlayerFpsNetworkPlayerInstance)_networkPlayerInstance;

			if (lobbyController.readyRequired)
				_networkPlayer.playerStatus = NetworkPlayerInstanceStatus.NonReady;
			else
				_networkPlayer.playerStatus = NetworkPlayerInstanceStatus.Ready;

			if(connectionManager.GetAllNetworkPlayerInstance().Count != 1) // if this player is not the first player
				_networkPlayer.roomSlot = connectionManager.GetUnusedRoomSlot ();
		}
	}
}
