﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: LobbyUIWorldCamera.cs
/// FUNCTION		: - Controls camera to moving arround between state objects of lobby, to create a "Zooming" effect.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LobbyUIObjectsMovementControl : MonoBehaviour 
{
	public Dictionary<LobbyController.LobbyState, Transform> uiReferences;
	private Dictionary<LobbyController.LobbyState, Vector3> referenceTargetedPosition;
	private int currentTargetStateIndex;
	private int previousTargetStateIndex = -1;
	private Vector3 targetedPosition;
	private Vector3 pivotPosition;

	/// <summary>
	/// Initialization of LobbyUIWorld Cameara
	/// </summary>
	/// <param name="_uiReferences">each lobby state's object postion location</param>
	/// <param name="_currentState">inital state for camera to know where to position itself initially</param>
	public void Setup(Dictionary<LobbyController.LobbyState, Transform> _uiReferences, LobbyController.LobbyState _currentState)
	{
		pivotPosition = transform.position;
		uiReferences = _uiReferences;

		referenceTargetedPosition = new Dictionary<LobbyController.LobbyState, Vector3> ();

		foreach (KeyValuePair<LobbyController.LobbyState, Transform> entry in uiReferences) 
		{
			entry.Value.GetComponent<CanvasGroupAlphaFade> ().SetAlpha(0.0f);
			referenceTargetedPosition.Add (entry.Key, entry.Value.position);
		}

		SetTargetPoint (_currentState, false);
	}

	public void SetTargetPoint(LobbyController.LobbyState _targetState, bool _fade)
	{
		currentTargetStateIndex = (int)_targetState;
		targetedPosition = pivotPosition - (referenceTargetedPosition[_targetState] - pivotPosition);

		if (_fade) 
		{
			uiReferences [_targetState].GetComponent<CanvasGroupAlphaFade> ().StartFade (1.0f, 0.5f);

			if (previousTargetStateIndex != -1)
				uiReferences [(LobbyController.LobbyState)previousTargetStateIndex].GetComponent<CanvasGroupAlphaFade> ().StartFade (0.0f, 0.5f);
		} 
		else 
		{
			transform.position = targetedPosition;
			uiReferences [(LobbyController.LobbyState)currentTargetStateIndex].GetComponent<CanvasGroupAlphaFade> ().SetAlpha(1.0f);
		}

		previousTargetStateIndex = currentTargetStateIndex;
	}

	void Update()
	{
		transform.position = Vector3.Lerp (transform.position, targetedPosition, Time.deltaTime * 4);
	}
}
