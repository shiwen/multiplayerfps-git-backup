﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: LobbyRoomUI.cs
/// FUNCTION		: - Sub module of Lobby UI
/// 				  - Controls "Room" state related ui stuffs
/// 				  - Shows players that jonied the room, let host kick player, start game, etc.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LobbyRoomUI: MonoBehaviour 
{
	[System.Serializable]
	public struct GameModeUIObjectReference 
	{
		public LobbyController.GameMode gameMode;
		public GameObject uiObject;
		public GameObject kickButton;
		public GameObject startButton;
		public GameObject readyButton;
		public GameObject[] slots;
		public LobbyRoomUIButtonSection buttonSectionControl;
	}

	//delegates


	public GameModeUIObjectReference[] gameModeUIObjectReference;
	public LobbyUI lobbyUI;
	public Text countdownText;

	public GameObject roomPlayerPrefab;

	private bool networkPlayerListenMode;
	private Dictionary<int, LobbyPlayer> lobbyPlayerReferences = new Dictionary<int, LobbyPlayer>();
	private int currentActivatedUIIndex;
	private LobbyPlayer currentlySelectedPlayer;
	private MultiPlayerFPSNetworkConnectionManager connectionManager;

	void Awake()
	{
		connectionManager = lobbyUI.lobbyController.networkConnectionManager;
		this.enabled = false;
	}

	void Update()
	{
		List<MultiPlayerFpsNetworkPlayerInstance> _players = connectionManager.GetAllMultiplayerFpsNetworkPlayer();

		if (_players != null) 
		{
			List<int> _unusedKeys = new List<int>(lobbyPlayerReferences.Keys);

			for(int i = 0; i <_players.Count; i++) 
			{
				bool _lobbyObjectExisted = false;

				if (_unusedKeys.Contains ((int)_players[i].identifier)) 
				{
					_unusedKeys.Remove ((int)_players[i].identifier);
					_lobbyObjectExisted = true;
				}

				if (!_lobbyObjectExisted) 
				{
					AddNewPlayerObject (_players[i].identifier, _players[i].playerName, _players[i].playerStatus);
				}
			}


			foreach(int _candidate in _unusedKeys)
			{
				RemovePlayerObject (_candidate);
			}

			foreach (KeyValuePair<int, LobbyPlayer> _entry in lobbyPlayerReferences) 
			{
				for (int i = 0; i < _players.Count; i++) 
				{
					if (_entry.Key == _players [i].identifier) 
					{
						_entry.Value.SetPlayerStatusUI ((NetworkPlayerInstanceStatus)_players [i].playerStatus);
						AssignObjectByPositionIndex (_entry.Value.gameObject, _players [i].roomSlot);
						break;
					}
				}
			}
		}
	}

	public void EnableRelatedGameUIObject(LobbyController.GameMode _gameMode, bool _isHost, bool _readyRequired, bool _isGameStarted)
	{
		currentActivatedUIIndex = (int)_gameMode;

		foreach (GameModeUIObjectReference candidate in gameModeUIObjectReference) 
		{
			if (candidate.gameMode == _gameMode) 
			{
				candidate.uiObject.SetActive (true);
				SetupButtonSection (candidate, _isHost, _readyRequired, _isGameStarted);
			} 
			else 
			{
				candidate.uiObject.SetActive (false);
			}
		}
	}

	public void SetupButtonSection(GameModeUIObjectReference _uiObjectReference, bool _isHost, bool _readyRequired, bool _gameIsStarted)
	{
		if (_readyRequired)
			_uiObjectReference.readyButton.SetActive (true);
		else
			_uiObjectReference.readyButton.SetActive (false);

		if (_isHost) 
		{
			_uiObjectReference.kickButton.SetActive (true);

			if (_readyRequired)
				_uiObjectReference.startButton.SetActive (false);
			else
				_uiObjectReference.startButton.SetActive (true);
		} 
		else 
		{
			_uiObjectReference.kickButton.SetActive (false);

			if (_gameIsStarted)
				_uiObjectReference.startButton.SetActive (true);
			else
				_uiObjectReference.startButton.SetActive (false);
		}

		_uiObjectReference.buttonSectionControl.RepositionActivatedButtons ();
	}

	public void SetupCurrentButtonSection(bool _isHost, bool _readyRequired, bool _gameIsStarted)
	{
		SetupButtonSection (gameModeUIObjectReference [currentActivatedUIIndex], _isHost, _readyRequired, _gameIsStarted);
	}

	public GameModeUIObjectReference GetGameModeUIObjectReference(LobbyController.GameMode _gameMode)
	{
		foreach (GameModeUIObjectReference candidate in gameModeUIObjectReference) {
			if (candidate.gameMode == _gameMode)
				return candidate;
		}

		Debug.LogError ("Refernce Not Found!");

		return gameModeUIObjectReference[0];
	}

	private void AddNewPlayerObject(int _identifier, string _playerName, NetworkPlayerInstanceStatus _playerStatus)
	{
		LobbyPlayer _lobbyPlayerHolder = GameObject.Instantiate (roomPlayerPrefab).GetComponent<LobbyPlayer>();
		lobbyPlayerReferences.Add (_identifier, _lobbyPlayerHolder);
		_lobbyPlayerHolder.identifier = (byte)_identifier;
		_lobbyPlayerHolder.SetNameUI (_playerName);
		_lobbyPlayerHolder.SetPlayerStatusUI (_playerStatus);
	}

	private void RemovePlayerObject(int _identifier)
	{
		GameObject _tempObjectHolder = lobbyPlayerReferences [_identifier].gameObject;
		lobbyPlayerReferences.Remove (_identifier);
		GameObject.Destroy (_tempObjectHolder);
	}

	public void ClearRoomPlayers()
	{
		foreach (KeyValuePair<int, LobbyPlayer> entry in lobbyPlayerReferences) 
		{
			GameObject.Destroy (entry.Value.gameObject);
		}

		lobbyPlayerReferences = new Dictionary<int, LobbyPlayer> ();
	}

	public void AssignObjectByPositionIndex(GameObject _playerObject, int _slotPositionIndex)
	{
		_playerObject.transform.SetParent (GetGameModeUIObjectReference ((LobbyController.GameMode)currentActivatedUIIndex).slots [_slotPositionIndex].transform);
		_playerObject.transform.localPosition = new Vector3 (0, 0, 0);
		_playerObject.transform.localScale = new Vector3 (1, 1, 1);
	}

	public void UpdateCountDown(float currentCounter)
	{
		countdownText.gameObject.SetActive (true);
		countdownText.text = Mathf.CeilToInt (currentCounter).ToString ();
	}

	public void HideCountDownText()
	{
		countdownText.gameObject.SetActive (false);
	}
		
	#region button

	public void QuitButtonClicked()
	{
		lobbyUI.lobbyController.ExitFromRoom ();
	}

	public void ReadyButtonClicked()
	{
		lobbyUI.lobbyController.RequestUpdateReadyStatus ();
	}

	public void SwitchTeamButtonClicked(int _targetedTeamIndex)
	{/*
		lobbyUI.lobbyController.networkConnectionManager.customNetworkSynchronizer.TriggerSwitchTeam (_targetedTeamIndex);*/
	}

	public void LobbyPlayerObjectClicked(GameObject _targetedObject)
	{
		if (currentlySelectedPlayer != _targetedObject) 
		{
			if(currentlySelectedPlayer != null)
				currentlySelectedPlayer.transform.GetChild (3).gameObject.SetActive (false);

			currentlySelectedPlayer = _targetedObject.GetComponent<LobbyPlayer>();
			currentlySelectedPlayer.transform.GetChild (3).gameObject.SetActive (true);
		}
	}

	public void StartButtonClicked()
	{
		if(NetworkConnectionManager.instance.startAsHost)
			NetworkConnectionManager.instance.ServerStartGame ();
		else
			NetworkConnectionManager.instance.RequestPostStartGameJoinGame ();
	}

	public void KickButtonClicked()
	{
		if (currentlySelectedPlayer != null && 
			currentlySelectedPlayer.identifier != lobbyUI.lobbyController.networkConnectionManager.GetLocalPlayer().identifier) 
		{
			lobbyUI.lobbyController.networkConnectionManager.KickPlayer (currentlySelectedPlayer.GetComponent<LobbyPlayer>().identifier);
		}
	}

	#endregion
}
