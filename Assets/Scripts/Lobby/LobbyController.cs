/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: LobbyLobbyUI.cs
/// FUNCTION		: - Overall logic of lobby scene
/// 				  - Handle cross module functions.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LobbyController : MonoBehaviour
{
	public enum LobbyState
	{
		SearchingRoom, Lobby, Room
	}

	public enum GameMode
	{
		TeamVsTeam,
		FreeForAll
	}
		
    public string broadcastIdentifier = "CM";
	public GameMode gameMode;
	public bool autoJoin;
	public bool joinSpecificIpRoom;
	public string joinIpAddress = "";
	public bool readyRequired;
	public bool directChangeSceneWhileJoinStartedGame;
    public float countdownDuration = 4; // Wait for this many seconds after people are ready before starting the game
	[HideInInspector]
	public LobbyState currentLobbyState;

	public MultiPlayerFPSNetworkConnectionManager networkConnectionManager;
	public LobbyConnectionReactor lobbyConnectionReactor;
	public LobbyUI lobbyUI;

	private float countDownCounter;

	public bool doCountDown;

    void Awake()
    {
		Application.runInBackground = true;

		if (autoJoin) 
		{
			currentLobbyState = LobbyState.SearchingRoom;
			StartAutoJoining ();
		}
		else
			currentLobbyState = LobbyState.Lobby;
		
		lobbyUI.Setup (currentLobbyState);
		networkConnectionManager.Initialize (lobbyConnectionReactor);
		networkConnectionManager.SetupSelfPlayerName ("DefaultName");
		SetupCustomEventListeners ();
    }

	void Update()
	{
		switch (currentLobbyState) 
		{
		case LobbyState.Lobby:
			break;

		case LobbyState.Room:

			if (readyRequired) 
			{
				if (networkConnectionManager.GetLocalPlayer() != null && networkConnectionManager.startAsHost) 
				{
					if (networkConnectionManager.NumReadyPlayers () == networkConnectionManager.GetAllNetworkPlayerInstance().Count) 
					{
						if (countDownCounter <= 0) 
						{
							networkConnectionManager.ServerStartGame ();
						} 
						else 
						{
							lobbyUI.lobbyRoomUI.UpdateCountDown (countDownCounter);

							if (countDownCounter == countdownDuration) 
							{
								networkConnectionManager.ServerTriggerStartCountdown ();
							}

							countDownCounter -= Time.deltaTime;
						}
					} 
					else 
					{
						lobbyUI.lobbyRoomUI.HideCountDownText ();

						if (countDownCounter != countdownDuration) 
						{
							networkConnectionManager.ServerTriggerCancelCountdown ();
						}

						countDownCounter = countdownDuration;
					}
				} 
				else 
				{
					if (doCountDown) 
					{
						lobbyUI.lobbyRoomUI.UpdateCountDown (countDownCounter);
						countDownCounter -= Time.deltaTime;
					}
					else
					{
						lobbyUI.lobbyRoomUI.HideCountDownText ();
						countDownCounter = countdownDuration;
					}
				}
			}

			break;
		}
	}

	public void ExitFromRoom()
	{
		if (currentLobbyState == LobbyState.Room) 
		{
			networkConnectionManager.Cancel ();
			networkConnectionManager.StartRetreiveRoomList ();
			
			if (autoJoin) 
			{
				currentLobbyState = LobbyState.SearchingRoom;
				lobbyUI.ChangeToSearchRoomView ();
				StartAutoJoining ();
			} 
			else 
			{
				currentLobbyState = LobbyState.Lobby;
				lobbyUI.ChangeToLobbyView ();
			}

		}
	}

	public void StartGame()
	{
		networkConnectionManager.gameplaySceneLoaded = true;
		SceneManager.LoadScene ("Gameplay");
	}

	#region room state changes

	public void ChangeToRoomState(bool _isStarted, bool _isHost)
	{
		if (currentLobbyState != LobbyState.Room) 
		{
			currentLobbyState = LobbyState.Room;
			lobbyUI.ChangeToRoomView ((int)gameMode, _isHost, _isStarted);
		}
	}

	#endregion

	#region network related

	public void SetupCustomEventListeners()
	{
		networkConnectionManager.ClientAddCustomEventListener ((int)MultiPlayerFPSCustomNetworkEvent.StartCountdown, StartCountdown);
		networkConnectionManager.ClientAddCustomEventListener ((int)MultiPlayerFPSCustomNetworkEvent.CancelCountdown, CancelCountdown);
	}

	public void RequestUpdateReadyStatus()
	{
		MultiPlayerFpsNetworkPlayerInstance _networkPlayer = networkConnectionManager.GetLocalMultiplayerFpsNetworkPlayer ();

		if (networkConnectionManager.startAsHost) 
		{
			if (_networkPlayer.playerStatus == NetworkPlayerInstanceStatus.NonReady)
				networkConnectionManager.ServerChangePlayerStatus (networkConnectionManager.GetLocalMultiplayerFpsNetworkPlayer ().identifier, NetworkPlayerInstanceStatus.Ready);
			else if(_networkPlayer.playerStatus == NetworkPlayerInstanceStatus.Ready)
				networkConnectionManager.ServerChangePlayerStatus (networkConnectionManager.GetLocalMultiplayerFpsNetworkPlayer ().identifier, NetworkPlayerInstanceStatus.NonReady);
		} 
		else 
		{
			if (_networkPlayer.playerStatus == NetworkPlayerInstanceStatus.NonReady)
				networkConnectionManager.ClientChangeSelfStatus (NetworkPlayerInstanceStatus.Ready);
			else if(_networkPlayer.playerStatus == NetworkPlayerInstanceStatus.Ready)
				networkConnectionManager.ClientChangeSelfStatus (NetworkPlayerInstanceStatus.NonReady);
		}
	}

	public void StartHosting(string _roomName)
	{
		networkConnectionManager.StartHosting(_roomName, gameMode);
	}

	public void ConnectToServer(string _targetedIpAddress)
	{
		networkConnectionManager.JoinServer (_targetedIpAddress);
	}

	public void ChangeNetworkPlayerName(string _playerName)
	{
		networkConnectionManager.SetupSelfPlayerName(_playerName);
	}

	public void StartCountdown(NetworkCustomMessage _parameter)
	{
		doCountDown = true;
	}

	public void CancelCountdown(NetworkCustomMessage _parameter)
	{
		doCountDown = false;
	}

	#endregion

	#region auto join

	private void StartAutoJoining()
	{
		lobbyUI.lobbyLoadingUI.AddNewLoadingRequest (new LobbyLoadingUI.LoadingRequestComponent (AutoJoiningFindingRoomChecking, RoomFound), "Finding Room...", false);
	}

	private bool AutoJoiningFindingRoomChecking()
	{
		if (joinSpecificIpRoom) 
		{
			if (networkConnectionManager.GetDiscoveredServers().ContainsKey (joinIpAddress))
				return true;
		}
		else if (autoJoin) 
		{
			if (networkConnectionManager.GetDiscoveredServers ().Count != 0)
				return true;
		}

		return false;
	}

	private void RoomFound()
	{
		if (joinSpecificIpRoom) 
		{
			networkConnectionManager.JoinServer (joinIpAddress);
		} 
		else if (autoJoin) 
		{
			networkConnectionManager.JoinServer(networkConnectionManager.GetFirstDiscoveredServer().ipAddress);
		}
			
	}

	#endregion
}