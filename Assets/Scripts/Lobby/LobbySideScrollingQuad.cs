﻿using UnityEngine;
using System.Collections;

public class LobbySideScrollingQuad : MonoBehaviour 
{
	public Camera currentCamera;
	public GameObject[] quadObjects;
	public float quadMoveSpeed;

	private float quadWidthExtend;
	private float viewWidthExtend;

	void Start()
	{
		float _frustumHeight = 2.0f * transform.localPosition.z * Mathf.Tan(currentCamera.fieldOfView * 0.5f * Mathf.Deg2Rad);
		viewWidthExtend = (_frustumHeight * currentCamera.aspect) / 2;
		quadWidthExtend = (quadObjects [0].GetComponent<Renderer> ().bounds.size.x) / 2;
	}

	void Update()
	{
		foreach (GameObject _candidate in quadObjects) 
		{
			_candidate.transform.localPosition += new Vector3((Time.deltaTime * quadMoveSpeed), 0, 0);
		}
		
		for (int i = 0; i < quadObjects.Length; i++) //check whether a quad is moved out of sight, if yes, move it to left hand side
		{
			if ((quadObjects [i].transform.localPosition.x - quadWidthExtend) > viewWidthExtend) 
			{
				float _smallest = 9999;
				int _smallestIndex = -1;

				for (int j = 0; j < quadObjects.Length; j++) 
				{
					if (quadObjects [j].transform.localPosition.x < _smallest) 
					{
						_smallest = quadObjects [j].transform.localPosition.x;
						_smallestIndex = j;
					}
				}

				quadObjects[i].transform.localPosition = new Vector3(quadObjects[_smallestIndex].transform.localPosition.x - (quadWidthExtend * 2), 0, 0);
			}
		}
	}
}
