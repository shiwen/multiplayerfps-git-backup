﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LobbyPlayer : MonoBehaviour 
{
	public byte identifier;
	public Text nameField;
	public Text readyField;

	#region lobby player methods

	public void Start()
	{
		LobbyController lobbyController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<LobbyController> ();
		GetComponent<Button> ().onClick.AddListener (() => {lobbyController.lobbyUI.lobbyRoomUI.LobbyPlayerObjectClicked(gameObject);}); 
	}
		
	#endregion

	#region set ui

	public void SetNameUI(string _name)
	{
		nameField.text = _name;
	}

	public void SetPlayerStatusUI(NetworkPlayerInstanceStatus _playerStatus)
	{
		if (_playerStatus == NetworkPlayerInstanceStatus.Ready) {
			readyField.text = "READY!";
			readyField.color = Color.green;
		} else if (_playerStatus == NetworkPlayerInstanceStatus.NonReady) {
			readyField.text = "not ready";
			readyField.color = Color.red;
		} else if (_playerStatus == NetworkPlayerInstanceStatus.Playing) {
			readyField.text = "Playing";
			readyField.color = Color.blue;
		} else if (_playerStatus == NetworkPlayerInstanceStatus.Waiting) {
			readyField.text = "Waiting";
			readyField.color = Color.cyan;
		}
	}

	#endregion
}
