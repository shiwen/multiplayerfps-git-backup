﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LobbyRoomUIButtonSection : MonoBehaviour 
{
	public float calculateLength = 500.0f;
	
	void Start()
	{
		RepositionActivatedButtons ();
	}

	public void RepositionActivatedButtons()
	{
		List<GameObject> _activateButtonsObject = new List<GameObject> ();

		for (int i = 0; i < transform.childCount; i++) 
		{
			if (transform.GetChild (i).gameObject.activeInHierarchy)
				_activateButtonsObject.Add(transform.GetChild(i).gameObject);
		}

		float _intervalRange = calculateLength / (_activateButtonsObject.Count + 1);

		for (int i = 0; i < _activateButtonsObject.Count; i++) 
		{
			_activateButtonsObject [i].transform.localPosition = new Vector3 ((-calculateLength / 2) + ((i + 1) * _intervalRange), 0, 0);
		}
	}
}
