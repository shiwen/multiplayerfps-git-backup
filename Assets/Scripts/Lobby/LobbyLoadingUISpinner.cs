﻿using UnityEngine;
using System.Collections;

public class LobbyLoadingUISpinner : MonoBehaviour 
{
	public float spinAngleInterval;
	public float spinRate;

	void Start()
	{
		InvokeRepeating ("Spin", 0, spinRate);
	}

	void Spin()
	{
		transform.Rotate (new Vector3 (0, 0, -1), spinAngleInterval);
	}
}
