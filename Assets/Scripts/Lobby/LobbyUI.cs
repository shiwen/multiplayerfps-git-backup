﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: LobbyUI.cs
/// FUNCTION		: - To manage functions that allow other module to interact with lobby ui.
/// 				  - Store reference to other lobby ui sub modules
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LobbyUI : MonoBehaviour 
{
	//references
	public LobbyController lobbyController; //Module that controls overall logic of lobby.
	public LobbyUIObjectsMovementControl uiObjectMovementControl; //module that will moves between sub modules to create zooming effect.
	public GameObject uiObjectHolder; //GameObject that holds all the sub module of lobby ui.
	public GameObject lobbySearchRoomUI; //Search room section (under development).
	public LobbyLobbyUI lobbyLobbyUI; //lobby Section, where the list of rooms will be shown.
	public LobbyRoomUI lobbyRoomUI; //room section, where the players who joined the room will be shown.
	public LobbyLoadingUI lobbyLoadingUI; //loading dialogue that will be called for different purposes (currently unused).

	/// <summary>Initialization of LobbyUI class</summary>
	/// <param name="_initialState">Initial state of lobby</param>
	public void Setup(LobbyController.LobbyState _initialState)
	{
		//objectReferenceDictionary stores game object thats represents different state: "Searching room", "Lobby", and "Room".
		Dictionary<LobbyController.LobbyState, Transform> _objectReferenceDictionary = new Dictionary<LobbyController.LobbyState, Transform> ();
		_objectReferenceDictionary.Add (LobbyController.LobbyState.SearchingRoom, lobbySearchRoomUI.transform);
		_objectReferenceDictionary.Add (LobbyController.LobbyState.Lobby, lobbyLobbyUI.transform);
		_objectReferenceDictionary.Add (LobbyController.LobbyState.Room, lobbyRoomUI.gameObject.transform);

		//pass objectReferenceDictionary so camera knows where is each lobby state's object postion located. pass initalState so that camera know where to position itself initially.
		uiObjectMovementControl.Setup (_objectReferenceDictionary, _initialState);

		//setup listner functions when "Join" button is clicked, when "Host" button is clicked, and when player input player name
		lobbyLobbyUI.SetupListeners(lobbyController.ConnectToServer, lobbyController.StartHosting, lobbyController.ChangeNetworkPlayerName);
	}

	#region state change related

	/// <summary>Change the current lobby UI to "Room" state.</summary>
	/// <param name="_targetedGameMode">Current game mode, module will show different set of ui according to different game modes.</param>
	/// <param name="_isHoste">Whether current player is host, different set of buttons will be shown for host and non-host.</param>
	/// <param name="_isHoste">Whether the room is already started, different set of buttons will be shown for started and non-started game room.</param>
	public void ChangeToRoomView(int _targetedGameMode, bool _isHost, bool _gameStarted)
	{
		ChangeView (LobbyController.LobbyState.Room);
		lobbyRoomUI.enabled = true;
		lobbyRoomUI.EnableRelatedGameUIObject ((LobbyController.GameMode)_targetedGameMode, _isHost, lobbyController.readyRequired, _gameStarted);
	}

	/// <summary>Change the current lobby UI to "Lobby" state.</summary>
	public void ChangeToLobbyView()
	{
		lobbyLobbyUI.ClearRoomList ();
		ChangeView (LobbyController.LobbyState.Lobby);
		lobbyRoomUI.ClearRoomPlayers ();
		lobbyRoomUI.enabled = false;
	}

	/// <summary>Change the current lobby UI to "Searching Room" state.</summary>
	public void ChangeToSearchRoomView()
	{
		lobbyLobbyUI.ClearRoomList ();
		ChangeView (LobbyController.LobbyState.SearchingRoom);
		lobbyRoomUI.ClearRoomPlayers ();
		lobbyRoomUI.enabled = false;
	}

	/// <summary>Ask lobby world camera to move to targeted state's position with fading effect</summary> 
	private void ChangeView(LobbyController.LobbyState _targetedLobbyState)
	{
		uiObjectMovementControl.SetTargetPoint (_targetedLobbyState, true);
	}

	#endregion

	#region info inquiry

	/// <summary>Get current texts in player name input field</summary>
	public string GetPlayerCurrentInputName()
	{
		return lobbyLobbyUI.playerNameInputField.text;
	}

	#endregion

	#region lobby

	/// <summary>Add new room object</summary>
	public void AddRoomObject(string _ipAddress, string _roomName, int _currentPlayersCount, int _maxPlayers)
	{
		lobbyLobbyUI.AddRoomObject (_ipAddress, _roomName, _currentPlayersCount, _maxPlayers);
	}

	/// <summary>Remove existed room object</summary>
	public void RemoveRoomObject(string _ipAddress)
	{
		lobbyLobbyUI.RemoveRoomObject (_ipAddress);
	}

	#endregion
}
