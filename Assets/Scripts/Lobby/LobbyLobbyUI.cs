﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: LobbyLobbyUI.cs
/// FUNCTION		: - Sub module of Lobby UI
/// 				  - Controls "Lobby" state related ui stuffs
/// 				  - Show rooms list
/// 				  - Allow player to join/host game, set player name, etc.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class LobbyLobbyUI : MonoBehaviour 
{
	//references
	public GameObject roomObjectContainer;
	public GameObject roomObjectStartPoint;
	public InputField playerNameInputField;
	public InputField roomNameInputField;

	//prefabs
	public GameObject roomObjectPrefab;

	//properties
	public Color roomObjectSelectedColor;
	public Color roomObjectDefaultColor;

	private Dictionary<GameObject, string> roomObjectServerDictionary = new Dictionary<GameObject, string>();
	private float roomObjectPrefabHeight = 42.0f;
	private GameObject currentSelectedRoomObject;
	private string defaultPlayerName;

	void Start()
	{
		defaultPlayerName = playerNameInputField.text;
	}

	#region room object related

	public void AddRoomObject(string _roomIpAddress, string _roomName, int _currentNumPlayers, int _maxPlayers)
	{
		GameObject _roomObjectHolder = GameObject.Instantiate (roomObjectPrefab);

		_roomObjectHolder.transform.parent = roomObjectContainer.transform;
		_roomObjectHolder.GetComponentInChildren<Text> ().text = _roomName + " (" + _currentNumPlayers + "/" + _maxPlayers + ")";
		_roomObjectHolder.GetComponent<Button> ().onClick.AddListener (() => {RoomObjectClicked(_roomObjectHolder.gameObject);}); 
		roomObjectServerDictionary.Add (_roomObjectHolder, _roomIpAddress);
		_roomObjectHolder.transform.localScale = new Vector3 (1, 1, 1);

		RepositionRoomObjects ();

		if (roomObjectServerDictionary.Count == 1)
			RoomObjectClicked (_roomObjectHolder);
	}

	public void RemoveRoomObject(string _targetedIpAddress)
	{
		GameObject objectToBeDestroyed = null;

		foreach (KeyValuePair<GameObject, string> entry in roomObjectServerDictionary) 
		{
			if (entry.Value == _targetedIpAddress) 
			{
				objectToBeDestroyed = entry.Key;
			}
		}

		roomObjectServerDictionary.Remove (objectToBeDestroyed);

		GameObject.DestroyImmediate (objectToBeDestroyed);

		RepositionRoomObjects ();
	}

	public void ClearRoomList()
	{
		foreach (KeyValuePair<GameObject, string> entry in roomObjectServerDictionary) 
		{
			GameObject.Destroy (entry.Key);
		}

		roomObjectServerDictionary = new Dictionary<GameObject, string> ();
	}

	private void RepositionRoomObjects()
	{
		Vector3 startingPosition = roomObjectStartPoint.transform.localPosition + new Vector3 (0, -(roomObjectServerDictionary.Count * roomObjectPrefabHeight) + (roomObjectPrefabHeight / 2), 0);
		int counter = 0;

		foreach (KeyValuePair<GameObject, string> entry in roomObjectServerDictionary) 
		{
			entry.Key.transform.localPosition = startingPosition + new Vector3 (0, roomObjectPrefabHeight * counter, 0);
			counter++;
		}
	}

	public void RoomObjectClicked(GameObject _objectIdentifier)
	{
		_objectIdentifier.GetComponent<Image> ().color = roomObjectSelectedColor;
		
		if (currentSelectedRoomObject != null) 
		{
			if (currentSelectedRoomObject == _objectIdentifier)
				onJoinButtonClickedFunction (roomObjectServerDictionary [currentSelectedRoomObject]);
			else
				currentSelectedRoomObject.GetComponent<Image>().color = roomObjectDefaultColor;
		}

		currentSelectedRoomObject = _objectIdentifier;
	}

	#endregion

	#region listener

	//delegates
	public delegate void StringParameterFunction(string _string);

	private StringParameterFunction onJoinButtonClickedFunction;
	private StringParameterFunction onHostButtonClickedFunction;
	private StringParameterFunction onPlayerNameChangedFunction;

	public void SetupListeners(StringParameterFunction _onJoinButtonClickedFunction, StringParameterFunction _onHostButtonClickedFunction, StringParameterFunction _onPlayerNameChangedFunction)
	{
		onJoinButtonClickedFunction = _onJoinButtonClickedFunction;
		onHostButtonClickedFunction = _onHostButtonClickedFunction;
		onPlayerNameChangedFunction = _onPlayerNameChangedFunction;
	}

	public void JoinButtonClicked()
	{
		if(currentSelectedRoomObject != null && roomObjectServerDictionary.ContainsKey(currentSelectedRoomObject))
			onJoinButtonClickedFunction(roomObjectServerDictionary[currentSelectedRoomObject]);
	}

	public void HostButtonClicked()
	{
		onHostButtonClickedFunction (roomNameInputField.text);
	}

	public void OnPlayerNameInputTextChanged()
	{
		if (playerNameInputField.text != defaultPlayerName)
			onPlayerNameChangedFunction (playerNameInputField.text);
	}
	
	#endregion
}
