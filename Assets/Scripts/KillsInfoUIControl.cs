﻿using UnityEngine;
using System.Collections;

public class KillsInfoUIControl : MonoBehaviour 
{
	public GameObject killsInfoNodePrefab;
	public float nodeExistTime;
	public float nodeHeight;
	public Sprite grenadeImg;

	public void AddNewKillsInfo(string _killerName, string _dierName, WeaponType _weaponUsed, bool _isHeadShoot)
	{
		KillsInfoUINode _newlyAddedNode = GameObject.Instantiate (killsInfoNodePrefab).GetComponent<KillsInfoUINode> ();
		_newlyAddedNode.transform.SetParent (this.transform);
		_newlyAddedNode.transform.localScale = new Vector3 (1, 1, 1);
		_newlyAddedNode.Setup (_killerName, _dierName, Resources.Load<Sprite> (WeaponDatabase.instance.GetWeapon (_weaponUsed).weaponTexturePath), _isHeadShoot, nodeExistTime);

		_newlyAddedNode.transform.SetAsFirstSibling ();

		for (int i = 0; i < transform.childCount; i++) 
		{
			transform.GetChild(i).transform.localPosition = new Vector3(0, -(nodeHeight / 2 + (i * nodeHeight)), 0);
		}
	}

	public void AddNewGrenadeKillInfo(string _killerName, string _dierName) 
	{
		KillsInfoUINode _newlyAddedNode = GameObject.Instantiate (killsInfoNodePrefab).GetComponent<KillsInfoUINode> ();
		_newlyAddedNode.transform.SetParent (this.transform);
		_newlyAddedNode.transform.localScale = new Vector3 (1, 1, 1);
		_newlyAddedNode.SetupGrenadeKills (_killerName, _dierName, grenadeImg, nodeExistTime);

		_newlyAddedNode.transform.SetAsFirstSibling ();

		for (int i = 0; i < transform.childCount; i++) 
		{
			transform.GetChild(i).transform.localPosition = new Vector3(0, -(nodeHeight / 2 + (i * nodeHeight)), 0);
		}
	}

}
