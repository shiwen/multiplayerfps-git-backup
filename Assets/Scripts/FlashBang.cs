﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: FlashBang.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Ronnie
/// DATE			: 12/07/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Ronnie	| 12/07/16	|	- Init components
/// </summary> ------------------------------------------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;
using System.Collections.Generic;

public class FlashBang : Grenade {	

	[SerializeField] private UIManager uiManager;
	public GameObject explosionPrefab;
	bool hasExploded = false;

	private GameObject player;
	private GameObject[] Players;
	[SerializeField] List<Collider> colliders = new List<Collider>();
	[SerializeField] private List<GameObject> playerList;

	protected FlashBang (FlashBang other) : base (other) {
	}

	void Awake() 
	{
		uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
		player = masterObject;
		StartCoroutine(Detonate());

		playerList = new List<GameObject>();
		Players = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject go in Players) {
			playerList.Add(go);
		}

		for (int i = 0; i < playerList.Count; i++) {
			SetupCollider(i, colliders);
		}
	}

	void Update() {
//		Debug.Log("Angle : " + Vector3.Angle(transform.position - player.transform.position, player.transform.forward));
	}

	protected override IEnumerator Detonate() {
		yield return new WaitForSeconds(timeBfrExplode);
		hasExploded = true;

		hitPoint = preciseCollider.GetPreviousPosition();
		hitNormal = preciseCollider.GetHitNormal();

		if (explosionPrefab != null) {
			Instantiate(explosionPrefab, hitPoint, Quaternion.identity);
		}

		Collider[] _hitColliders = Physics.OverlapSphere(hitPoint, explosionRadius);

		foreach(Collider col in _hitColliders) 
		{
			if(OnGrenadeImpact != null){
				OnGrenadeImpact(col,this);
			}
			Debug.DrawLine (hitPoint, col.transform.position, Color.red, 2f);
		}

		for (int i = 0; i < playerList.Count; i++) {
			RaycastHit hit;
			if(Physics.Raycast(hitPoint, (playerList[i].transform.position - hitPoint), out hit)) {
				if(hit.collider.transform.root.tag != "Player"){
					continue;
				}
				else {
					Debug.Log("Hit col(layer int) : " + LayerMask.LayerToName(hit.collider.transform.root.gameObject.layer) + "(" + hit.collider.transform.root.gameObject.layer + ")");

						if(hit.collider.transform.root.gameObject.layer == 8){
							TriggerFlash(hit.collider.transform.gameObject);
					}
				}
			}

		}

		Destroy(this.gameObject);
	}

	void TriggerFlash(GameObject _GO)
	{
		float playerAngle = Vector3.Angle(transform.position - _GO.transform.position, _GO.transform.forward);
		float distanceBetweenObjs = Vector3.Distance(_GO.transform.position, this.transform.position);
		if(hasExploded) {
			if(distanceBetweenObjs <= 50f) {
				if(playerAngle <= 25) {
					uiManager.TriggerFlashBang(3f, 0.5f);
				}
				else if(playerAngle >= 25 && playerAngle <= 70) {
					uiManager.TriggerFlashBang(0.5f, 0.2f);
				}
				else if(playerAngle > 70 && playerAngle <= 100) { 
					uiManager.TriggerFlashBang(0.3f, 0.5f);
				}
				else if(playerAngle > 100) {
					uiManager.TriggerFlashBang(0.1f, 1f);
				}
			}
			else {
				Debug.Log("FLASH TOO FAR");
			}
		}
	}

	private void SetupCollider(int _index, List<Collider> _list){ 
		Component[] _componentHolders;

		_componentHolders = playerList[_index].GetComponentsInChildren<Collider>();

		_list.Clear();
		foreach(Collider _c in _componentHolders){
			_list.Add(_c);
		}
	}
}
