﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: GameManager.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 16/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Patrick	| 31/05/16	|	- Assign gameplay connection raector to network connection manager
/// 					| 01/06/16  |   - Seperate PlayerDisplayInfo to a different script
/// 								|	- Modified and added player info related functions
/// 								|	- Modified register player and related functions
/// 		| Cee Jay	| 02/06/16	|	- Added register & unregister player camera functions
/// 		| Patrick	| 06/06/16	|	- added gameplay behavior array - scripts that contain gameplay logics 
/// 					| 06/06/16	|	- added gameplay behavior - scripts that control gameplay related thing (ex: takeable object spawner) 
/// 					| 15/06/16  |   - added powerup database and related functions
/// 					| 23/06/16  |   - added custom event related functions.
/// 					| 24/06/16  |   - added player death
/// 								|	- added kill info related functions
/// 					| 29/06/16	|	- UnregisterPlayerCamera now remove camera from list instead of adding it in
/// 					| 30/06/16	|	- Added game state
///                                     - Added moderator related functions
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	public enum GameState
	{
		WAITING_ROOM, GAMEPLAY
	}

	//game properties
	public float deathToRespawnTime = 7.0f;
	public bool skipDeathToSpectator = false;
	public bool hostAsModerator;
	public PlayerRoleBindedPrefab[] rolePrefabs;
	public Transform[] gamePlaySpawnPoints;

	public static GameManager instance;
	public UIManager uiManager;
	public NetworkSpawnManager networkSpawnManager;
	public GameplayBehavior[] gameplayBehaviors;

	public MultiPlayerFPSNetworkConnectionManager networkManager;
	private GameState currentGameState;
	private int currentGamePlaySpawnPointIndex;
	private bool loadThroughLobby;
	private Dictionary<byte, RoomPlayerInfo> playersInfo = new Dictionary<byte, RoomPlayerInfo>();
	private EndGameConditionChecker currentEndGameConditionChecker;

	void Awake()
	{
		networkManager = MultiPlayerFPSNetworkConnectionManager.GetInstance ();

		if (networkManager != null)
			loadThroughLobby = true;

		if(instance != null){
			Debug.LogError("More than one GameManager in scene.");
		}else{
			instance = this;
		}

		WeaponDatabase.instance.Init();
		PowerUpDatabase.instance.Init ();
		networkSpawnManager.Initialize ();
	}

	void Start()
	{
		if (networkManager != null)//is in network mode
		{
			currentGameState = GameState.WAITING_ROOM;
			SetupCustomEventListners (networkManager.startAsHost);
			networkManager.AssignReactor (GetComponent<GameplayConnectionReactor> ());
			SetupUI ();
			RegisterAllPlayer ();
			SetUpGameplayBehavior (networkManager.startAsHost);
			InitializeEndGameCondition ();
		}
	}

	#region game state

	public void ChangeGameState(GameState _targetedState)
	{
		currentGameState = _targetedState;
	}

	#endregion

	#region ui related

	void SetupUI()
	{
		if (hostAsModerator) 
		{
			if (networkManager.startAsHost) 
			{
				uiManager.moderatorUIControl.Setup (uiManager.GetComponent<Canvas> (), ModeratorPanelStartButtonClicked, networkManager.KickPlayer, ModeratorPostStartGameAssignRole, ModeratorPostStartGameCancelRole);

				foreach (MultiPlayerFpsNetworkPlayerInstance _candidate in networkManager.GetAllMultiplayerFpsNetworkPlayer()) 
				{
					if (_candidate.identifier != 0)
						uiManager.moderatorUIControl.AddNewPlayerObject (_candidate.identifier, _candidate.playerName);
					else
						((GameplayNetworkPlayer)_candidate.networkPlayerConnectionAuthority).SpawnSelectedCharacter (PlayerRole.Spectator);
				}
			}
		} 
		else 
		{
			if(loadThroughLobby)
				uiManager.roleSelectionUIControl.gameObject.SetActive (true);
		}

		uiManager.Setup (networkManager.KickPlayer, networkManager.Cancel);
	}

	#endregion

	#region gameplay behavior

	private void SetUpGameplayBehavior(bool _startAsHost)
	{
		if (_startAsHost) 
		{
			foreach (GameplayBehavior _candidate in gameplayBehaviors) 
			{
				_candidate.enabled = true;
				_candidate.HostInitialize ();
			}
		} 
		else 
		{
			foreach (GameplayBehavior _candidate in gameplayBehaviors) 
			{
				_candidate.enabled = false;
				_candidate.ClientInitialize ();
			}
		}
	}

	#endregion

	#region Player infos

	private void RegisterAllPlayer()
	{
		foreach (MultiPlayerFpsNetworkPlayerInstance _candidate in networkManager.GetAllNetworkPlayerInstance()) 
		{
			RegisterPlayer (_candidate.identifier, false);
		}
	}

	public void RegisterPlayer (byte _identifier, bool _replace)
	{
		if(!playersInfo.ContainsKey(_identifier) || _replace)
		{
			if (playersInfo.ContainsKey (_identifier))
				playersInfo.Remove (_identifier);
			
			MultiPlayerFpsNetworkPlayerInstance _networkPlayer = networkManager.GetMultiplayerFpsNetworkPlayerByIdentifier (_identifier);
			RoomPlayerInfo _playerInfo = new RoomPlayerInfo ();
			_playerInfo.playerName = _networkPlayer.playerName;

			GameplayNetworkPlayer _gameplayNetworkPlayer = (GameplayNetworkPlayer)_networkPlayer.networkPlayerConnectionAuthority;

			if ((NetworkPlayerInstanceStatus)_networkPlayer.playerStatus == NetworkPlayerInstanceStatus.Playing && _gameplayNetworkPlayer != null) 
			{

				_playerInfo.playerState = _gameplayNetworkPlayer.currentPlayerState;
				_playerInfo.playerRole = _gameplayNetworkPlayer.currentPlayerRole;
			} 
			else 
			{
				_playerInfo.playerState = GameplayPlayerState.InLobby;
			}
				
			playersInfo.Add (_identifier, _playerInfo);
		}
	}

	public void UnRegisterPlayer(byte _identifier)
	{
		playersInfo.Remove (_identifier);
	}

	public Dictionary<byte, RoomPlayerInfo> GetRoomPlayerInfos()
	{
		return playersInfo;
	}

	public void UpdatePlayerStateInfo(byte _identifier, GameplayPlayerState _targetedPlayerState)
	{
		playersInfo [_identifier].playerState = _targetedPlayerState;
	}

	public void UpdatePlayerRoleInfo(byte _identifier, PlayerRole _targetedPlayerRole)
	{
		playersInfo [_identifier].playerRole = _targetedPlayerRole;
	}

	public void AddPlayerKillScore(byte _identifier)
	{
		playersInfo [_identifier].killsCount += 1;

		if (networkManager.startAsHost)
			CheckEndGameKills (playersInfo [_identifier].killsCount);
	}

	public void AddPlayerDeathScore(byte _identifier)
	{
		playersInfo [_identifier].deathsCount += 1;
	}

	#endregion

	/*
	public static PlayerDisplayInfo GetPlayerDisplayInfo(PlayerController _player){

		if(players.ContainsKey(_player)){
			return players[_player];
		}else{
			return null;
		}
	}*/

	public static void LockCursor(bool _value){
				if(_value){
					Cursor.lockState = CursorLockMode.Locked;
				}else{
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
				}
	}

	#region Register Camera
	public static List<Camera> cameraList = new List<Camera>();

	public static void RegisterPlayerCamera(Camera _cam){
		cameraList.Add(_cam);
	}

	public static void UnregisterPlayerCamera(Camera _cam){
		cameraList.Remove(_cam);
	}

	#endregion

	#region listeners

	private void SetupCustomEventListners(bool _isHost)
	{
		networkManager.ClientClearCustomEventLister ((int)MultiPlayerFPSCustomNetworkEvent.PlayerDeath);
		networkManager.ClientClearCustomEventLister ((int)MultiPlayerFPSCustomNetworkEvent.ChangeGameplayState);
		networkManager.ClientClearCustomEventLister ((int)MultiPlayerFPSCustomNetworkEvent.TriggerEndGame);
		
		if (_isHost) 
		{
			networkManager.ClientAddCustomEventListener ((int)MultiPlayerFPSCustomNetworkEvent.PlayerDeath, OnServerPlayerDeath);
			networkManager.ClientAddCustomEventListener ((int)MultiPlayerFPSCustomNetworkEvent.ChangeGameplayState, OnServerChangeGameplayState);
			networkManager.ClientAddCustomEventListener ((int)MultiPlayerFPSCustomNetworkEvent.TriggerEndGame, OnServerReceiveTriggerEndGame);
		} 
		else 
		{
			networkManager.ClientAddCustomEventListener ((int)MultiPlayerFPSCustomNetworkEvent.PlayerDeath, OnClientPlayerDeath);
			networkManager.ClientAddCustomEventListener ((int)MultiPlayerFPSCustomNetworkEvent.ChangeGameplayState, OnClientChangeGameplayState);
			networkManager.ClientAddCustomEventListener ((int)MultiPlayerFPSCustomNetworkEvent.TriggerEndGame, OnClientReceiveTriggerEndGame);
		}
	}

	public void OnServerPlayerDeath(NetworkCustomMessage _customMessage)
	{
		byte _killerIdentifier = (byte)_customMessage.GetInt ("killer");
		byte _dierIdentifier = (byte)_customMessage.GetInt ("dier");

		GameplayNetworkPlayer _targetedNetworkPalyer = GetGameplayNetworkPlayerByIdentifier (_dierIdentifier);

		if (!skipDeathToSpectator)
			_targetedNetworkPalyer.SwitchToSpectator ();

		_targetedNetworkPalyer.StartDeathToRespawn (deathToRespawnTime);

//		PlayerDeathHandler (_killerIdentifier, _dierIdentifier, (WeaponType)_customMessage.GetInt ("weapon"), _customMessage.GetBool ("headShoot"));
		PlayerDeathByGrenadeHandler(_killerIdentifier, _dierIdentifier);
		_targetedNetworkPalyer.characterHolder.GetComponent<PlayerManager> ().TriggerPostDeathCamera (GetGameplayNetworkPlayerByIdentifier(_killerIdentifier).characterHolder);
	}

	public void OnClientPlayerDeath(NetworkCustomMessage _customMessage)
	{
		byte _killerIdentifier = (byte)_customMessage.GetInt ("killer");
		byte _dierIdentifier = (byte)_customMessage.GetInt ("dier");
//		PlayerDeathHandler (_killerIdentifier, _dierIdentifier, (WeaponType)_customMessage.GetInt ("weapon"), _customMessage.GetBool ("headShoot"));
		PlayerDeathByGrenadeHandler(_killerIdentifier, _dierIdentifier);
	}

	public void OnClientChangeGameplayState(NetworkCustomMessage _customMessage)
	{
		ChangeGameState((GameState)_customMessage.GetInt("state"));
	}

	public void OnServerChangeGameplayState(NetworkCustomMessage _customMessage)
	{
		ChangeGameState((GameState)_customMessage.GetInt("state"));
		uiManager.moderatorUIControl.OnGameplayStarted ();
	}

	public void OnServerReceiveTriggerEndGame(NetworkCustomMessage _customMessage)
	{
		ShowEndGameUI ();
	}

	public void OnClientReceiveTriggerEndGame(NetworkCustomMessage _customMessage)
	{
		ShowEndGameUI ();
	}

	#endregion

	#region gameplay player

	public GameObject GetRolePrefab(PlayerRole _targetedRole)
	{
		foreach (PlayerRoleBindedPrefab _candidate in rolePrefabs) 
		{
			if (_candidate.playerRole == _targetedRole)
				return _candidate.prefab;
		}

		return null;
	}

	private void PlayerDeathHandler(byte _killerIdentifier, byte _dierIdentifier, WeaponType _weaponType, bool _isHeadShoot)
	{
		AddPlayerKillScore (_killerIdentifier);
		AddPlayerDeathScore (_dierIdentifier);

		uiManager.killsInfoUIControl.AddNewKillsInfo (playersInfo [_killerIdentifier].playerName, playersInfo [_dierIdentifier].playerName, _weaponType, _isHeadShoot);

		if (_dierIdentifier == networkManager.GetLocalMultiplayerFpsNetworkPlayer ().identifier) 
		{
			LocalPlayerDeathHandler ((byte)_killerIdentifier);
		}
	}

	private void PlayerDeathByGrenadeHandler(byte _killerIdentifier, byte _dierIdentifier)
	{
		AddPlayerKillScore (_killerIdentifier);
		AddPlayerDeathScore (_dierIdentifier);

		uiManager.killsInfoUIControl.AddNewGrenadeKillInfo (playersInfo [_killerIdentifier].playerName, playersInfo [_dierIdentifier].playerName);

		if (_dierIdentifier == networkManager.GetLocalMultiplayerFpsNetworkPlayer ().identifier) 
		{
			LocalPlayerDeathHandler ((byte)_killerIdentifier);
		}
	}

	private void LocalPlayerDeathHandler(byte _killerIdentifier)
	{
		uiManager.deathTimerUIControl.StartDeathTimer (deathToRespawnTime);
		uiManager.DisableFlashBang ();
		GetLocalGameplayNetworkPlayer ().characterHolder.GetComponent<PlayerManager> ().TriggerPostDeathCamera (GetGameplayNetworkPlayerByIdentifier(_killerIdentifier).characterHolder);
	}

	public GameplayNetworkPlayer GetGameplayNetworkPlayerByIdentifier(byte _identifier)
	{
		return (GameplayNetworkPlayer)(networkManager.GetPlayerByIdentifier (_identifier).networkPlayerConnectionAuthority);
	}

	private GameplayNetworkPlayer GetLocalGameplayNetworkPlayer()
	{
		return (GameplayNetworkPlayer)(networkManager.GetLocalPlayer().networkPlayerConnectionAuthority);
	}

	public PlayerManager GetPlayerManagerByIdentifier(byte _identifier)
	{
		return (GetGameplayNetworkPlayerByIdentifier(_identifier)).characterHolder.GetComponent<PlayerManager>();
	}

	#endregion

	#region moderator related

	private void ModeratorPanelStartButtonClicked(List<byte> _playerIdentifiers, List<string> _roleStrings)
	{
		networkManager.ServerTriggerChangeGameplayState ((int)GameState.GAMEPLAY);
		SpawnModeratorSelectedRoles (_playerIdentifiers, _roleStrings);
	}

	private void SpawnModeratorSelectedRoles(List<byte> _playerIdentifiers, List<string> _roleStrings)
	{
		for(int i = 0; i < _playerIdentifiers.Count; i++)
		{
			GetGameplayNetworkPlayerByIdentifier (_playerIdentifiers [i]).SpawnSelectedCharacter ((PlayerRole)System.Enum.Parse (typeof(PlayerRole), _roleStrings [i]));
		}
	}

	private void ModeratorPostStartGameAssignRole(byte _identifier, string _roleString)
	{
		PlayerRole _targetedRole = (PlayerRole)System.Enum.Parse (typeof(PlayerRole), _roleString);
		GetGameplayNetworkPlayerByIdentifier (_identifier).SpawnSelectedCharacter (_targetedRole);
	}

	private void ModeratorPostStartGameCancelRole(byte _identifier)
	{
		GetGameplayNetworkPlayerByIdentifier (_identifier).SwitchBackToAuthorityMode ();
	}

	#endregion

	#region scene object related

	public Vector3 GetCurrentGameplaySpawnPosition()
	{
		Vector3 _currentSpawnPos = gamePlaySpawnPoints [currentGamePlaySpawnPointIndex].position;

		currentGamePlaySpawnPointIndex++;

		if (currentGamePlaySpawnPointIndex >= gamePlaySpawnPoints.Length) 
		{
			currentGamePlaySpawnPointIndex = 0;
		}

		return _currentSpawnPos;
	}

	#endregion

	#region end game condition

	private void InitializeEndGameCondition()
	{
		currentEndGameConditionChecker = new DeathmatchEndGameConditionChecker ();
		currentEndGameConditionChecker.InitializeEndGameCondition(TriggerEndGame, 2);
	}

	private void CheckEndGameKills(int _currentKills)
	{
		currentEndGameConditionChecker.UpdateCondition (_currentKills);
	}

	private void TriggerEndGame()
	{
		networkManager.ServerTriggerEndGame ();
	}

	private void ShowEndGameUI()
	{
		ShowDeathmatchEndGameUI ();
	}

	private void ShowDeathmatchEndGameUI()
	{
		byte _localPlayerIdentifier = GetLocalGameplayNetworkPlayer ().connectionIndex;
		int _localPlayerIndex = 0;
		List<DeathmatchEndGameUIControl.UIParameter> _uiParameters = new List<DeathmatchEndGameUIControl.UIParameter> ();
		List<RoomPlayerInfo> _rawPlayerInfos = new List<RoomPlayerInfo> ();
		List<byte> _rawPlayerIdentifiers = new List<byte> ();

		foreach (KeyValuePair<byte, RoomPlayerInfo> _entry in playersInfo) 
		{
			if (_entry.Value.playerState != GameplayPlayerState.InLobby && _entry.Value.playerState != GameplayPlayerState.SelectingRole) 
			{
				_rawPlayerInfos.Add (_entry.Value);
				_rawPlayerIdentifiers.Add(_entry.Key);
			}
		}

		int _iterateIndex = 0;
		int _currentHighestKillsCount = -1;
		int _currentHighestKillsIndex = -1;
		while (_rawPlayerInfos.Count != 0) 
		{
			if (_iterateIndex == _rawPlayerInfos.Count) 
			{
				RoomPlayerInfo _infoToBeAdded = _rawPlayerInfos [_currentHighestKillsIndex];
				_uiParameters.Add (new DeathmatchEndGameUIControl.UIParameter (_infoToBeAdded.playerName, _infoToBeAdded.playerRole.ToString(), _infoToBeAdded.killsCount, _infoToBeAdded.deathsCount));

				if (_rawPlayerIdentifiers [_currentHighestKillsIndex] == _localPlayerIdentifier) 
				{
					_localPlayerIndex = _currentHighestKillsIndex;
				}

				_rawPlayerInfos.RemoveAt (_currentHighestKillsIndex);
				_rawPlayerIdentifiers.RemoveAt (_currentHighestKillsIndex);
				_iterateIndex = 0;
				_currentHighestKillsCount = -1;
				_currentHighestKillsIndex = -1;
			} 
			else 
			{
				RoomPlayerInfo _currentInfo = _rawPlayerInfos [_iterateIndex];

				if (_currentInfo.killsCount > _currentHighestKillsCount) 
				{
					_currentHighestKillsCount = _currentInfo.killsCount;
					_currentHighestKillsIndex = _iterateIndex;
				}

				_iterateIndex++;
			}
		}

		uiManager.ShowDeathmatchEndGameUI (_uiParameters, _localPlayerIndex);
	}

	#endregion
}

#region sub classes

[System.Serializable]
public struct PlayerRoleBindedPrefab
{
	public PlayerRole playerRole;
	public GameObject prefab;
}

#endregion
