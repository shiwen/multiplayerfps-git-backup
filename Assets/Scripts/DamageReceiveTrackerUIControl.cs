﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DamageReceiveTrackerUIControl : MonoBehaviour 
{
	public GameObject trackerPrefab;

	private Dictionary<int, GameObject> spawnedTrackers;
	private float angleOffset = 0.0f;

	void Awake()
	{
		spawnedTrackers = new Dictionary<int, GameObject> ();
	}

	public void ClearTrackers()
	{
		foreach (KeyValuePair<int, GameObject> entry in spawnedTrackers) 
		{
			GameObject.Destroy (entry.Value);
		}
	}
	
	public void AddTracker(int _trackerIdentifier, Vector2 _directionVector)
	{
		GameObject _newTracker = GameObject.Instantiate (trackerPrefab);
		_newTracker.transform.SetParent (this.transform);
		_newTracker.transform.localScale = new Vector3 (1, 1, 1);
		_newTracker.transform.localPosition = new Vector3 (0, 0, 0);

		spawnedTrackers.Add (_trackerIdentifier, _newTracker);

		UpdateTrackerDirection (_trackerIdentifier, _directionVector);
	}

	public void UpdateTrackerDirection(int _trackerIdentifier, Vector2 _directionVector)
	{
		float _angle = Vector2.Angle(Vector2.up, _directionVector);

		if (_directionVector.x > 0)
			_angle = -_angle;
	
		spawnedTrackers[_trackerIdentifier].transform.localRotation = Quaternion.Euler(new Vector3(0, 0, _angle + angleOffset));
	}

	public void RemoveTracker(int _trackerIdentifier)
	{
		GameObject _trackerToBeDestroyed = spawnedTrackers [_trackerIdentifier];
		spawnedTrackers.Remove (_trackerIdentifier);
		GameObject.Destroy (_trackerToBeDestroyed);
	}
}
