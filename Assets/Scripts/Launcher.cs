﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: Launcher.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Ronnie
/// DATE			: 20/06/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Ronnie	| 20/06/16	|	- added drawing line renderer position logic
/// 0.2		| Ronnie	| 23/06/16	|	- added launching grenade prefab following line renderer position logic
/// 0.3		| Ronnie	| 11/07/16	|	- added server checking logic
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

public class Launcher : NetworkBehaviour {

	private byte playerIdentifier;
	public GrenadeType grenadeType;
	public float mThrowStrength = 20.0f;
	private LineRenderer lineRenderer;
	private bool mUsingLauncher = false;
	private GameObject grenade;
	[SerializeField] private Camera playerCam;

	[SerializeField] private GameObject bulletSpawnPoint;
	[SerializeField] private GameObject grenadeObj;
	[SerializeField] private GameObject flashObj;

	#region Grenade Lists
	[SerializeField] private List<List<GameObject>> grenadeList = new List<List<GameObject>>(2);
	[SerializeField] private List<GameObject> fragGrenadeList = new List<GameObject>(3);
	[SerializeField] private List<GameObject> flashBangList = new List<GameObject>();
	[SerializeField] private int currentGrenade = 0;
	#endregion

	private bool isServerCalculate;

	private void Start () {
		lineRenderer = GetComponent<LineRenderer>();
		NotUsingLauncher();
		InitGrenadeLists();
	}

	void Update() {
		if(isLocalPlayer) {
			if(Input.GetKeyUp(KeyCode.Alpha5)) {
				UpdateCurrentGrenade();
				SetSelectedGrenade(grenadeList[currentGrenade]);
			}

			if(Input.GetKeyDown(KeyCode.G)) {
				if(grenadeList[currentGrenade].Count == 0) Debug.Log("No Grenades Picked Up"); 
				else UsingLauncher();
			}

//			if(Input.GetKeyUp(KeyCode.G)) {
//				if(grenadeList[currentGrenade].Count == 0) Debug.Log("No Grenades Picked Up");
//				else {
//					LaunchCurrentGrenade(grenadeList[currentGrenade]);
//					grenadeList[currentGrenade].RemoveAt(0);
//					UpdateGrenadeCount(grenadeList[currentGrenade]);
//
//					if(grenadeList[currentGrenade].Count <= 0) {
//						if(currentGrenade <= 0){
//							GameManager.instance.uiManager.grenadeSlotUIControl.RemoveExsitingGrenadeUINode(GrenadeType.FragGrenade);
//						}
//						else {
//							GameManager.instance.uiManager.grenadeSlotUIControl.RemoveExsitingGrenadeUINode(GrenadeType.FlashBang);
//						}
//					}
//				}
//			}

			if(Input.GetKeyUp(KeyCode.L)) {
				Debug.Log("Current Grenade Int : " + currentGrenade);
			}
		}
	}
	void FixedUpdate(){
		if(mUsingLauncher) {
			UpdateTrajectory(bulletSpawnPoint.transform.position, GetAimDir() * mThrowStrength, new Vector3(0,-9.81f,0));
		}
	}

	private void InitGrenadeLists() {
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		fragGrenadeList.Add(grenadeObj);
		flashBangList.Add(flashObj);
		flashBangList.Add(flashObj);
		flashBangList.Add(flashObj);
		flashBangList.Add(flashObj);
		flashBangList.Add(flashObj);
		flashBangList.Add(flashObj);
		flashBangList.Add(flashObj);
		flashBangList.Add(flashObj);
		flashBangList.Add(flashObj);
		flashBangList.Add(flashObj);
		grenadeList.Add(fragGrenadeList);
		grenadeList.Add(flashBangList);
	}	

	public void SetPlayerIdentifier(byte _identifier){
		playerIdentifier = _identifier;
	}


	private void UsingLauncher () {
		mUsingLauncher = true;
		lineRenderer.enabled = true;
	}

	private void NotUsingLauncher() {
		mUsingLauncher = false;
		lineRenderer.enabled = false;
	}

	private void LaunchCurrentGrenade(GameObject _GO){
		SpawnGrenade(_GO);
		NotUsingLauncher();
	}

	private void UpdateCurrentGrenade() {
		if(currentGrenade <= 0)  {
			currentGrenade++;
			if(grenadeList[currentGrenade].Count == 0){
				currentGrenade--;
			}
		}
		else 	{
			currentGrenade--;
			if(grenadeList[currentGrenade].Count == 0) currentGrenade++;
		}
		Debug.Log("CurrentGrenade : " + currentGrenade);
	}

	private void UpdateGrenadeCount(List<GameObject> _currentGameObject) {
		if(currentGrenade <= 0) {
//			GameManager.instance.uiManager.grenadeSlotUIControl.UpdateGranadeCount(GrenadeType.FragGrenade, fragGrenadeList.Count);
		}
		else {
//			GameManager.instance.uiManager.grenadeSlotUIControl.UpdateGranadeCount(GrenadeType.FlashBang, flashBangList.Count);
		}
	}

	private void SetSelectedGrenade(List<GameObject> _currentGameObject) {
		if(currentGrenade <= 0) {
			GameManager.instance.uiManager.grenadeSlotUIControl.SetSelectedGrenadeUINode(GrenadeType.FragGrenade);
		}
		else {
			GameManager.instance.uiManager.grenadeSlotUIControl.SetSelectedGrenadeUINode(GrenadeType.FlashBang);
		}
	}

	public void PickupGrenade(GrenadeType _grenadeType) {
		if(_grenadeType == GrenadeType.FragGrenade) {
			AddGrenade(fragGrenadeList, grenadeObj);
			if(grenadeList[0].Count == 0) {
				GameManager.instance.uiManager.grenadeSlotUIControl.AddNewGrenadeUINode(GrenadeType.FragGrenade);
			}
		}

		else if(_grenadeType == GrenadeType.FlashBang) {
			AddGrenade(flashBangList, flashObj);
			if(grenadeList[1].Count == 0) {
				GameManager.instance.uiManager.grenadeSlotUIControl.AddNewGrenadeUINode(GrenadeType.FlashBang);
			}
		}
	}

	private void AddGrenade(List<GameObject> _grenadeList, GameObject _grenadeType) {
		if(_grenadeList.Count >= 3) Debug.Log("Current Grenade Type is Full");
		else _grenadeList.Add(_grenadeType);
	}

	private void SpawnGrenade(GameObject _grenadeObj) {
		grenade = GameObject.Instantiate(_grenadeObj);
		grenade.transform.position = bulletSpawnPoint.transform.position;
		grenade.GetComponent<Rigidbody>().velocity = GetAimDir() * mThrowStrength;
		grenade.transform.forward = GetAimDir();
	}

	public GameObject GetGrenadeObj() {
		if(grenadeList[currentGrenade].Count == 0) Debug.Log("No Grenade available");
		else grenade = grenadeList[currentGrenade][0];
		Debug.Log("Current Grnade : "  + grenadeList[currentGrenade][0]);
		return grenade;
	}

	void UpdateTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity) {
		int numSteps = 20;
		float timeDelta = 1.0f / initialVelocity.magnitude;

		lineRenderer.SetVertexCount(numSteps);

		Vector3 position = initialPosition;
		Vector3 velocity = initialVelocity;
		for (int i = 0; i < numSteps; ++i) {
			lineRenderer.SetPosition(i, position);
			lineRenderer.SetWidth(0.1f, 1f);

			position += velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta;
			velocity += gravity * timeDelta;
		}
	}

	public Vector3 GetAimDir(){
		Vector3 mousePos = Vector3.zero;
		mousePos.x = Screen.width/2;
		mousePos.y = Screen.height/2;
		mousePos.z = 100.0f;
		Vector3 contactPoint = playerCam.ScreenToWorldPoint(mousePos);

		Vector3 temp = contactPoint - bulletSpawnPoint.transform.position;
		temp.Normalize();
		return temp;
	}

	public void FireGrenade(bool _isServerCalculate, GameObject _grenade) {
		isServerCalculate = _isServerCalculate;

		if(grenadeList[currentGrenade].Count <= 0) Debug.Log("No Grenades Picked Up");

		else {
			grenade = _grenade;
			_grenade.GetComponent<Grenade>().CanSelfHarm(false, gameObject);
			LaunchCurrentGrenade(_grenade);
			grenadeList[currentGrenade].RemoveAt(0);
			UpdateGrenadeCount(grenadeList[currentGrenade]);

			if(grenadeList[currentGrenade].Count <= 0) {
				if(currentGrenade <= 0){
//					GameManager.instance.uiManager.grenadeSlotUIControl.RemoveExsitingGrenadeUINode(GrenadeType.FragGrenade);
				}
				else {
//					GameManager.instance.uiManager.grenadeSlotUIControl.RemoveExsitingGrenadeUINode(GrenadeType.FlashBang);
				}
			}
			GrenadeCallBack(grenade);
		}
	}

	private void GrenadeCallBack(GameObject _grenade) {
		_grenade.GetComponent<Grenade>().SetShooterIdentifier(playerIdentifier);
		_grenade.GetComponent<Grenade>().OnGrenadeImpact = null;
		_grenade.GetComponent<Grenade>().OnGrenadeImpact += GrenadeImpactLogic;
	}

	private void GrenadeImpactLogic(Collider _collider, Grenade _grenade) {
		
		if(_collider.transform.GetComponent<IHitByGrenade>() != null){
			_collider.transform.GetComponent<IHitByGrenade>().GrenadeHit(GetComponent<Collider>(), _grenade,isServerCalculate);
		}
		else if(_collider.transform.root.GetComponent<IHitByGrenade>() != null){
			_collider.transform.root.GetComponent<IHitByGrenade>().GrenadeHit(_collider,_grenade,isServerCalculate);
		}

		Destroy (_grenade.gameObject);
	}
}

