﻿using UnityEngine;
using System.Collections;

public class WeaponDatabase {

	public static readonly WeaponDatabase instance = new WeaponDatabase();

	static WeaponDatabase()
	{
	}

	private WeaponDatabase()
	{
	}

	public readonly Weapon SMG = new Weapon();

	public readonly Weapon MP5 = new Weapon();

	public readonly Weapon AUG = new Weapon();

	public readonly Weapon AK47 = new Weapon();

	public readonly Weapon M16 = new Weapon();

	public readonly Shotgun famars = new Shotgun();

	public readonly RocketLauncher rpg7 = new RocketLauncher();


	public void Init(){

		//SMG
		SMG.weaponTier = 1;
		SMG.weaponName = "SMG";
		SMG.damage = 3;
		SMG.roundPerMinute = 750;
		SMG.optimalRange = 500;
		SMG.reloadTime = 2.0f;

		SMG.maxClipSize = 30;
		SMG.maxClipPool = 300;

		SMG.currentClipSize = 30;
		SMG.currentClipPool = 300;

		SMG.bulletPrefabPath = "Prefabs/Bullet";
		SMG.projectileSpeed = 500f;

		SMG.reticleBloomPenalty = 0.015f;
		SMG.recoilPenalty = .25f;

		SMG.weaponTexturePath = "UITextures/Weapon/pickableitem_weapon_smg";

		//MP5
		MP5.weaponTier = 1;
		MP5.weaponName = "MP5";
		MP5.damage = 4;
		MP5.roundPerMinute = 650;
		MP5.optimalRange = 500;
		MP5.reloadTime = 2.0f;

		MP5.maxClipSize = 30;
		MP5.maxClipPool = 300;

		MP5.currentClipSize = 30;
		MP5.currentClipPool = 300;

		MP5.bulletPrefabPath = "Prefabs/Bullet";
		MP5.projectileSpeed = 500f;

		MP5.reticleBloomPenalty = 0.025f;
		MP5.recoilPenalty = .25f;

		MP5.weaponTexturePath = "UITextures/Weapon/pickableitem_weapon_ak47";

		//AUG
		AUG.weaponTier = 1;
		AUG.weaponName = "AUG";
		AUG.damage = 5;
		AUG.roundPerMinute = 600;
		AUG.optimalRange = 500;
		AUG.reloadTime = 2.0f;

		AUG.maxClipSize = 25;
		AUG.maxClipPool = 250;

		AUG.currentClipSize = 25;
		AUG.currentClipPool = 250;

		AUG.bulletPrefabPath = "Prefabs/Bullet";
		AUG.projectileSpeed = 500f;

		AUG.reticleBloomPenalty = 0.025f;
		AUG.recoilPenalty = .25f;

		AUG.weaponTexturePath = "UITextures/Weapon/pickableitem_weapon_ak47";

		//AK47
		AK47.weaponTier = 2;
		AK47.weaponName = "AK47";
		AK47.damage = 5;
		AK47.roundPerMinute = 550;
		AK47.optimalRange = 700;
		AK47.reloadTime = 3.5f;

		AK47.maxClipSize = 30;
		AK47.maxClipPool = 300;

		AK47.currentClipSize = 30;
		AK47.currentClipPool = 300;

		AK47.bulletPrefabPath = "Prefabs/Bullet";
		AK47.projectileSpeed = 500f;

		AK47.reticleBloomPenalty = 0.04f;
		AK47.recoilPenalty = 0.05f;

		AK47.weaponTexturePath = "UITextures/Weapon/pickableitem_weapon_ak47";

		//M16
		M16.weaponTier = 2;
		M16.weaponName = "M16";
		M16.damage = 5;
		M16.roundPerMinute = 550;
		M16.optimalRange = 700;
		M16.reloadTime = 3.5f;

		M16.maxClipSize = 30;
		M16.maxClipPool = 300;

		M16.currentClipSize = 30;
		M16.currentClipPool = 300;

		M16.bulletPrefabPath = "Prefabs/Bullet";
		M16.projectileSpeed = 500f;

		M16.reticleBloomPenalty = 0.04f;
		M16.recoilPenalty = 0.05f;

		M16.weaponTexturePath = "UITextures/Weapon/pickableitem_weapon_m16";

		//FAMARS
		famars.weaponTier = 3;
		famars.weaponName = "FAMARS";
		famars.damage = 2;
		famars.roundPerMinute = 60;
		famars.optimalRange = 500;
		famars.reloadTime = 2f;

		famars.maxClipSize = 12;
		famars.maxClipPool = 240;

		famars.currentClipSize = 12;
		famars.currentClipPool = 240;

		famars.bulletPrefabPath = "Prefabs/Bullet";
		famars.projectileSpeed = 500f;

		famars.reticleBloomPenalty = 0.06f;
		famars.recoilPenalty = 3f;

		famars.pelletCount = 15;
		famars.spreadFactor = .3f;

		famars.weaponTexturePath = "UITextures/Weapon/pickableitem_weapon_famars";

		//RPG-7
		rpg7.weaponTier = 4;
		rpg7.weaponName = "RPG7";
		rpg7.damage = 70;
		rpg7.roundPerMinute = 60;
		rpg7.optimalRange = 1000;
		rpg7.reloadTime = 1f;

		rpg7.maxClipSize = 1;
		rpg7.maxClipPool = 15;

		rpg7.currentClipSize = 100;
		rpg7.currentClipPool = 15;

		rpg7.bulletPrefabPath = "Prefabs/Rocket";
		rpg7.projectileSpeed = 100f;

		rpg7.reticleBloomPenalty = 0f;
		rpg7.recoilPenalty = 0f;

		rpg7.areaOfEffect = 5f;

		rpg7.weaponTexturePath = "UITextures/Weapon/pickableitem_weapon_rpg7";

		Debug.Log("Init weapon database");
	}

	public Weapon GetWeapon(WeaponType _weaponName){
		switch(_weaponName){
		case WeaponType.SMG:
			return (Weapon)SMG.Clone();
		case WeaponType.MP5:
			return (Weapon)MP5.Clone();
		case WeaponType.AUG:
			return (Weapon)AUG.Clone();
		case WeaponType.AK47:
			return (Weapon)AK47.Clone();
		case WeaponType.M16:
			return (Weapon)M16.Clone();
		case WeaponType.FAMARS:
			return (Weapon)famars.Clone();
		case WeaponType.RPG7:
			return (Weapon)rpg7.Clone();
		default:
			Debug.LogError("Incorrect weapon type requested");
			return null;
		}
	}
}
