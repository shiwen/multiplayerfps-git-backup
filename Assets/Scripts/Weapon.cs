﻿using System;

public enum WeaponType
{
	SMG,
	MP5,
	AUG,
	AK47,
	M16,
	FAMARS,
	RPG7
}

[System.Serializable]
public class Weapon : ICloneable{

	public int weaponTier;
	public string weaponName;

	public string weaponTexturePath;

	public int damage;
	public int roundPerMinute;
	public float optimalRange;
	public float reloadTime;

	public int maxClipSize;
	public int maxClipPool;

	public int currentClipSize;
	public int currentClipPool;

	public string bulletPrefabPath;
	public float projectileSpeed;

	public float reticleBloomPenalty;
	public float recoilPenalty;


	public Weapon(){

	}

	protected Weapon(Weapon w){
		weaponTier = w.weaponTier;
		weaponName = w.weaponName;

		weaponTexturePath = w.weaponTexturePath;

		damage = w.damage;
		roundPerMinute = w.roundPerMinute;
		optimalRange = w.optimalRange;
		reloadTime = w.reloadTime;

		maxClipSize = w.maxClipSize;
		maxClipPool = w.maxClipPool;

		currentClipSize = w.currentClipSize;
		currentClipPool = w.currentClipPool;

		bulletPrefabPath = w.bulletPrefabPath;
		projectileSpeed = w.projectileSpeed;

		reticleBloomPenalty = w.reticleBloomPenalty;
		recoilPenalty = w.recoilPenalty;
	}

	public virtual object Clone(){
		return new Weapon(this);
	}
}

[System.Serializable]
public class Shotgun : Weapon {

	public int pelletCount;
	public float spreadFactor;

	public Shotgun(){

	}

	protected Shotgun (Shotgun other) : base (other) {
		pelletCount = other.pelletCount;
		spreadFactor = other.spreadFactor;
	}

	public override object Clone(){
		return new Shotgun (this);
	}
}

[System.Serializable]
public class RocketLauncher : Weapon {

	public float areaOfEffect;

	public RocketLauncher(){

	}

	protected RocketLauncher (RocketLauncher other) : base (other) {
		areaOfEffect = other.areaOfEffect;
	}

	public override object Clone(){
		return new RocketLauncher (this);
	}

}
