﻿using UnityEngine;
using System.Collections;

public class RoleSelectionInfoObjectControl : MonoBehaviour 
{
	public RoleSelectionInfoObject[] roleSelectionInfoObjects;

	private PlayerRole currentShowedInfoRole = PlayerRole.None;

	public void ShowRoleSelectionInfo(PlayerRole _targetedRole)
	{
		if (_targetedRole != currentShowedInfoRole) 
		{
			GetRoleSelectionInfoObject(_targetedRole).ShowInfo ();

			if (currentShowedInfoRole != PlayerRole.None) 
			{
				GetRoleSelectionInfoObject(currentShowedInfoRole).HideInfo ();
			}

			currentShowedInfoRole = _targetedRole;
		}
	}

	private RoleSelectionInfoObject GetRoleSelectionInfoObject(PlayerRole _targetedRole)
	{
		foreach (RoleSelectionInfoObject _candidate in roleSelectionInfoObjects) 
		{
			if (_candidate.representedRole == _targetedRole)
				return _candidate;
		}

		return null;
	}
}
