﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoleSelectionRoleObject : MonoBehaviour 
{
	public PlayerRole representedRole;
	
	public Color onSelectedRoleTextColor;
	public Color defaultRoleTextColor;

	public Color onSelectedBackgroundImageColor;
	public Color defaultBackgroundImageColor;

	public float transitionDuration;
	private float transtionCounter = 0;

	public Text roleText;
	public Image backgroundImage;

	private bool colorTransiting = false;
	private int transitionDirection;

	public void SwitchToSelected()
	{
		colorTransiting = true;
		transitionDirection = 1;
		transtionCounter = 0;
	}

	public void SwitchToUnselected()
	{
		colorTransiting = true;
		transitionDirection = -1;
		transtionCounter = 0;
	}

	void Update()
	{
		if (colorTransiting) 
		{
			if (transtionCounter >= transitionDuration) 
			{
				colorTransiting = false;
			} 
			else 
			{
				transtionCounter += Time.deltaTime;
				
				float _currentColorProgress = transtionCounter / transitionDuration;
				
				if (transitionDirection == 1) 
				{
					roleText.color = Color.Lerp (defaultRoleTextColor, onSelectedRoleTextColor, _currentColorProgress);
					backgroundImage.color = Color.Lerp (defaultBackgroundImageColor, onSelectedBackgroundImageColor, _currentColorProgress);
				} 
				else 
				{
					roleText.color = Color.Lerp (onSelectedRoleTextColor, defaultRoleTextColor, _currentColorProgress);
					backgroundImage.color = Color.Lerp (onSelectedBackgroundImageColor, defaultBackgroundImageColor, _currentColorProgress);
				}
			}
		}
	}
		
}
