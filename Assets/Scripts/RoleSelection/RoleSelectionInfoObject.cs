﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class RoleSelectionInfoObject : MonoBehaviour 
{
	public PlayerRole representedRole;
	
	public float transitionDuration;
	private float transtionCounter = 0.5f;

	public CanvasGroup canvasGroup;
	public Camera infoModelCamera;

	private bool colorTransiting = false;
	private int transitionDirection;

	public void ShowInfo()
	{
		colorTransiting = true;
		transitionDirection = 1;
		transtionCounter = 0;
	}

	public void HideInfo()
	{
		colorTransiting = true;
		transitionDirection = -1;
		transtionCounter = 0;
	}

	void Update()
	{
		if (colorTransiting) 
		{
			if (transtionCounter >= transitionDuration) 
			{
				colorTransiting = false;
			} 
			else 
			{
				transtionCounter += Time.deltaTime;

				float _currentColorProgress = transtionCounter / transitionDuration;

				if (transitionDirection == 1) 
				{
					canvasGroup.alpha = _currentColorProgress;
				} 
				else 
				{
					canvasGroup.alpha = 1 - _currentColorProgress;
				}
			}
		}
	}
}
