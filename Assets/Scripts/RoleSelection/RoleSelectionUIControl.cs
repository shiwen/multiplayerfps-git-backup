﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RoleSelectionUIControl : MonoBehaviour 
{
	public Text currentRoleText;
	public RoleSelectionRoleObjectControl roleSelectionRoleObjectControl;
	public RoleSelectionInfoObjectControl roleSelectionInfoObjectControl;

	void Start () 
	{
		roleSelectionRoleObjectControl.Setup(0, SetupInfoSection);
	}
	
	private void SetupInfoSection(PlayerRole _selectedRole)
	{
		currentRoleText.text = _selectedRole.ToString();
		roleSelectionInfoObjectControl.ShowRoleSelectionInfo (_selectedRole);
	}

	public void StartButtonClicked()
	{
		GameplayNetworkPlayer _connectionAuthority = (GameplayNetworkPlayer)NetworkConnectionManager.instance.GetLocalPlayer().networkPlayerConnectionAuthority;
		//NetworkConnectionManager.instance.GetCurrentLocalObject().GetComponent<GameplayNetworkPlayer>().SpawnSelectedCharacter(roleSelectionRoleObjectControl.currentlySelectedRoleObject.representedRole);
		_connectionAuthority.SpawnSelectedCharacter(roleSelectionRoleObjectControl.currentlySelectedRoleObject.representedRole);
		gameObject.SetActive (false);
	}
}
