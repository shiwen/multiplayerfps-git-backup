﻿using UnityEngine;
using System.Collections;

public class RoleSelectionRoleObjectControl : MonoBehaviour 
{
	public delegate void RoleObjectClickedFunction (PlayerRole _selectedIndex);
	public RoleObjectClickedFunction RoleObjectClickedListener;
	public RoleSelectionRoleObject[] roleObjects;

	[HideInInspector]
	public RoleSelectionRoleObject currentlySelectedRoleObject;

	public void Setup(int _defaultIndex, RoleObjectClickedFunction _onRoleObjectClickedListener)
	{
		RoleObjectClickedListener = _onRoleObjectClickedListener;
		RoleObjectClicked (roleObjects[_defaultIndex]);
	}

	public void RoleObjectClicked(RoleSelectionRoleObject _roleObject)
	{
		if (currentlySelectedRoleObject != _roleObject) 
		{
			_roleObject.SwitchToSelected ();

			if (currentlySelectedRoleObject != null) 
			{
				currentlySelectedRoleObject.SwitchToUnselected ();
			}

			currentlySelectedRoleObject = _roleObject;
			RoleObjectClickedListener (_roleObject.representedRole);
		}
	}

	private RoleSelectionRoleObject GetRoleObject(PlayerRole _targetedRole)
	{
		foreach (RoleSelectionRoleObject _candidate in roleObjects) 
		{
			if (_candidate.representedRole == _targetedRole)
				return _candidate;
		}

		return null;
	}
}
