﻿using UnityEngine;
using System.Collections;

#region player

public enum NetworkPlayerInstanceStatus
{
	NonReady,
	Ready,
	Playing,
	Waiting
}
	
public enum PlayerRole
{
	Spectator,
	Assault,
	Healer,
	Support,
	Trapper,
	None
}

public enum GameplayPlayerState
{
	InLobby,
	Playing,
	WaitingForRespawn,
	SelectingRole
}

#endregion

