﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: CharacterAction.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Jack Lieu
/// DATE			: 19/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Jack		| 13/05/16	|	- Inherit logic from NetworkAction scripts.
/// 		|			|			|	- Override base function to create custom action logic.
/// 		| Cee Jay	| 17/06/16	|	- replace PlayerShoot with PlayerManager
/// </summary> ------------------------------------------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class NetworkCharacterAction : NetworkAction {

	// Array to collect data
	private Inputs cachedData = new Inputs ();

	// Get shooting data
	private PlayerManager playerManager = null;

	void Start ()
	{
		playerManager = GetComponent<PlayerManager> ();
	}

	void LateUpdate ()
	{
		this.SendMessage ("UpdateActionAnimation", cachedData);
	}
	
	public override void GetInputs (ref Inputs _inputs)
	{
		_inputs.reload = Input.GetKeyUp (KeyCode.R);
		_inputs.shoot = Input.GetButton("Fire1");
	}

	public override bool Reload (Inputs _inputs, Results _results)
	{
		cachedData.reload = _inputs.reload;

		if (cachedData.reload) {
//			cachedData.reload = playerManager.Reload ();
		}

		return cachedData.reload;
	}

	public override bool Shoot (Inputs _inputs, Results _results)
	{
		cachedData.shoot = _inputs.shoot;

		if (cachedData.shoot) {
//			cachedData.shoot = playerManager.Shoot ();

			if (!cachedData.shoot)
				cachedData.reload = true;
		}

		return cachedData.shoot;
	}

	public override void UpdateReload (bool _reload)
	{
		cachedData.reload = _reload;
	}

	public override void UpdateShoot (bool _shoot)
	{
		cachedData.shoot = _shoot;
	}
}
