﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class GameplayNetworkPlayer : NetworkPlayerConnectionAuthority 
{
	[SyncVar]
	public GameplayPlayerState currentPlayerState = GameplayPlayerState.SelectingRole;
	[SyncVar]
	public PlayerRole currentPlayerRole;

	public GameObject characterHolder;

	private Camera bindedCamera;

	void Start()
	{
		bindedCamera = GetComponentInChildren<Camera> ();

		if (isLocal)
			bindedCamera.gameObject.SetActive (true);
		else
			bindedCamera.gameObject.SetActive (false);
	}

	public override void OnStartLocalPlayer ()
	{
		base.OnStartLocalPlayer ();
		CmdRegisterPlayer ();
		CmdRegisterForAllPlayer ();
		CmdAssignCharacterHolderForAllPlayer ();
		CmdAssignState (GameplayPlayerState.SelectingRole);
		CmdAssignRole (PlayerRole.None);
	}
		
	#region in game events

	public void SpawnSelectedCharacter (PlayerRole _selectedCharacterRole) 
	{
		CmdSpawnSelectedCharacter (_selectedCharacterRole);
	}

	private void UpdateRoleUI(PlayerRole _targetedRole)
	{
		GameManager.instance.uiManager.EnableRoleUI (_targetedRole);
	}

	public void ServerReplaceCharacterHolder(GameObject _targetedObject, PlayerRole _role)
	{
		NetworkServer.ReplacePlayerForConnection (connectionToClient, _targetedObject, 0);
		RpcAssignCharacterHolder (_targetedObject.GetComponent<NetworkIdentity> ().netId);
		RpcUpdateRoleUI (_role);
	}

	#region playable role

	public void StartDeathToRespawn(float _duration)
	{
		Invoke ("RespawnPlayer", _duration);
	}

	public void RespawnPlayer()
	{
		CmdSwitchRole (currentPlayerRole);
	}

	#endregion

	#region non playable role

	public void SwitchToSpectator()
	{
		CmdSwitchRole (PlayerRole.Spectator);
	}

	public void SwitchBackToAuthorityMode()
	{
		CmdDestroyRoleObject ();
		characterHolder = null;
		RpcEnableBindedCamera ();
	}

	#endregion

	#endregion
		
	#region commands and rpcs

	[Command]
	void CmdRegisterPlayer()
	{
		NetworkConnectionManager.instance.AssignNetworkPlayerConnectionAuthority (connectionIndex, this);
		RpcRegisterPlayer (connectionIndex);
	}

	[Command]
	void CmdRegisterForAllPlayer()
	{
		foreach(NetworkPlayerInstance _candidate in NetworkConnectionManager.instance.GetAllNetworkPlayerInstance())
		{
			GameplayNetworkPlayer _connectionAuthority = (GameplayNetworkPlayer)_candidate.networkPlayerConnectionAuthority;

			if (_connectionAuthority != null) 
			{
				_connectionAuthority.RpcRegisterPlayer (_candidate.identifier);
			}
		}
	}

	[ClientRpc]
	void RpcRegisterPlayer(byte _identifier)
	{
		GameManager.instance.RegisterPlayer (_identifier, true);
	}

	[Command]
	void CmdAssignState(GameplayPlayerState _targetedState)
	{
		currentPlayerState = _targetedState;
		RpcUpdatePlayerStateInfo (connectionIndex, _targetedState);
	}

	[ClientRpc]
	void RpcUpdatePlayerStateInfo(byte _identifier, GameplayPlayerState _targetedState)
	{
		GameManager.instance.UpdatePlayerStateInfo (_identifier, _targetedState);
	}

	[Command]
	void CmdAssignRole(PlayerRole _tagetedRole)
	{
		currentPlayerRole = _tagetedRole;
		RpcUpdatePlayerRoleInfo (connectionIndex, _tagetedRole);
	}

	[ClientRpc]
	void RpcUpdatePlayerRoleInfo(byte _identifier, PlayerRole _targetedRole)
	{
		GameManager.instance.UpdatePlayerRoleInfo (_identifier, _targetedRole);
	}

	[Command]
	void CmdSpawnSelectedCharacter(PlayerRole _selectedCharacterRole)
	{
		CmdAssignState(GameplayPlayerState.Playing);
		CmdAssignRole(_selectedCharacterRole);
		RpcDisableBindedCamera ();
	
		characterHolder = (GameObject)GameObject.Instantiate (GameManager.instance.GetRolePrefab (_selectedCharacterRole), 
			GameManager.instance.GetCurrentGameplaySpawnPosition(), Quaternion.Euler (0, Random.Range (0, 360), 0));

		ServerReplaceCharacterHolder (characterHolder, _selectedCharacterRole);

		if (_selectedCharacterRole != PlayerRole.None && _selectedCharacterRole != PlayerRole.Spectator) 
		{
			string _playerName = GameManager.instance.networkManager.GetMultiplayerFpsNetworkPlayerByIdentifier (connectionIndex).playerName;
			
			characterHolder.GetComponent<PlayerManager> ().SetupNetworkProperties(connectionIndex, _playerName);
			RpcSetPlayerCharacterNetworkProperties (connectionIndex, _playerName);
		}
	}

	[Command]
	public void CmdSwitchRole(PlayerRole _targetedRole)
	{
		Destroy (characterHolder);
		CmdSpawnSelectedCharacter (_targetedRole);
	}

	[Command]
	public void CmdDestroyRoleObject()
	{
		Destroy (characterHolder);
	}

	[ClientRpc]
	void RpcUpdateRoleUI(PlayerRole _targetedRole)
	{
		if (isLocal) 
		{
			UpdateRoleUI (_targetedRole);
		}
	}

	[Command]
	void CmdAssignCharacterHolderForAllPlayer()
	{
		foreach(MultiPlayerFpsNetworkPlayerInstance _candidate in NetworkConnectionManager.instance.GetAllNetworkPlayerInstance())
		{
			GameplayNetworkPlayer _networkPlayer = (GameplayNetworkPlayer)_candidate.networkPlayerConnectionAuthority;
			if (_networkPlayer != null && _networkPlayer.characterHolder != null) 
			{
				_networkPlayer.RpcAssignCharacterHolder (_networkPlayer.characterHolder.GetComponent<NetworkIdentity>().netId);
			}
		}
	}

	[ClientRpc]
	void RpcAssignCharacterHolder(NetworkInstanceId _netId)
	{
		characterHolder = ClientScene.FindLocalObject (_netId);
	}

	[ClientRpc]
	void RpcSetPlayerCharacterNetworkProperties(byte _identifier, string _playerName)
	{
		characterHolder.GetComponent<PlayerManager> ().SetupNetworkProperties(_identifier, _playerName);
	}

	[ClientRpc]
	void RpcDisableBindedCamera()
	{
		if (isLocal) 
		{
			bindedCamera.gameObject.SetActive (false);
		}
	}

	[ClientRpc]
	void RpcEnableBindedCamera()
	{
		if (isLocal) 
		{
			bindedCamera.gameObject.SetActive (true);
		}
	}

	#endregion
}
