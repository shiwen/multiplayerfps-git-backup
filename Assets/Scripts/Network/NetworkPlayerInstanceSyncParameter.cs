﻿using UnityEngine;
using System.Collections;

public enum NetworkPlayerInstanceSyncParameterType
{
	Integer,
	Bool,
	String,
	Enum
}

[System.Serializable]
public class NetworkPlayerInstanceSyncParameter 
{
	public string parameterKey;
	public NetworkPlayerInstanceSyncParameterType parameterType;
	public object parameterValue;

	public NetworkPlayerInstanceSyncParameter(string _parameterKey, NetworkPlayerInstanceSyncParameterType _parameterType, object _parameterDefaultValue)
	{
		parameterKey = _parameterKey;
		parameterType = _parameterType;
		parameterValue = _parameterDefaultValue;
	}

	public string GetParameterKey()
	{
		return parameterKey;
	}

	public NetworkPlayerInstanceSyncParameterType GetParameterType()
	{
		return parameterType;
	}
}
