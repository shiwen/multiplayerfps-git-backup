﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: NetworkMessageEventSynchronizer.cs
/// FUNCTION		: - Sub module of NetworkConnectionManager
/// 				  - Provides functions to handle NetworkMessage transmition between host and client.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public enum CustomNetworkMessageEvent
{
	JoinedRoom = 4001,
	UpdateRoomPlayerInfos = 4002,
	UpdateParticularRoomPlayerInfo = 4003,
	PlayerQuited = 4004,
	StartGame = 4005,
	PostStartGameJoinGame = 4006,
	CustomEvent = 4007
}

public class NetworkMessageEventSynchronizer : MonoBehaviour 
{	
	public delegate void ListenerFunction(NetworkCustomMessage _parameter);

	public class MessageDeletegateToListenerFunctionLinker
	{
		public ListenerFunction listenerFunction;

		public void OnLinkedFunctionToBeCall(NetworkMessage _parameter)
		{
			NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
			_customMessage.Deserialize (_parameter.reader);
			_customMessage.senderConnnection = _parameter.conn;
			_customMessage.ProcessData ();

			listenerFunction (_customMessage);
		}
	}

	public class EventBindedListenerFunction
	{
		public List<MessageDeletegateToListenerFunctionLinker> serverSideFunction = new List<MessageDeletegateToListenerFunctionLinker>();
		public List<MessageDeletegateToListenerFunctionLinker> clientSideFunction = new List<MessageDeletegateToListenerFunctionLinker>();
	}
		
	Dictionary<CustomNetworkMessageEvent, EventBindedListenerFunction> bindedFunctions = new Dictionary<CustomNetworkMessageEvent, EventBindedListenerFunction>();
	bool isServer;

	public void Setup(bool _isServer)
	{
		isServer = _isServer;

		foreach (CustomNetworkMessageEvent _candidate in System.Enum.GetValues(typeof(CustomNetworkMessageEvent))) 
		{
			bindedFunctions.Add (_candidate, new EventBindedListenerFunction ());
		}
			
	}
		
	public void AddListenerFunction(CustomNetworkMessageEvent _event, bool _isServer, ListenerFunction _function)
	{
		MessageDeletegateToListenerFunctionLinker _linker = new MessageDeletegateToListenerFunctionLinker ();
		_linker.listenerFunction = _function;
		
		if (_isServer)
			bindedFunctions [_event].serverSideFunction.Add (_linker);
		else
			bindedFunctions [_event].clientSideFunction.Add (_linker);
	}

	public void RegisterListener(NetworkConnection _conn, bool _isServer)
	{
		foreach (CustomNetworkMessageEvent _candidate in System.Enum.GetValues(typeof(CustomNetworkMessageEvent))) 
		{
			List<MessageDeletegateToListenerFunctionLinker> _listenerFunctions;

			if (_isServer)
				_listenerFunctions = bindedFunctions [_candidate].serverSideFunction;
			else
				_listenerFunctions = bindedFunctions [_candidate].clientSideFunction;
		
			foreach (MessageDeletegateToListenerFunctionLinker _linker in _listenerFunctions) 
			{
				_conn.RegisterHandler ((short)_candidate, _linker.OnLinkedFunctionToBeCall); 
			}
		}
	}

	public void TriggerEventAsCaller(CustomNetworkMessageEvent _event, NetworkCustomMessage _parameter, NetworkConnection _targetedConnections)
	{
		TriggerEventAsCaller (_event, _parameter, new List<NetworkConnection> (){ _targetedConnections });
	}

	public void TriggerEventAsCaller(CustomNetworkMessageEvent _event, NetworkCustomMessage _parameter, List<NetworkConnection> _targetedConnections)
	{
		foreach (NetworkConnection _candidate in _targetedConnections)
		{
			_candidate.Send ((short)_event, _parameter);
		}
	}
}
