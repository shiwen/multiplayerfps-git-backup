﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: NetworkPlayerInstanceManager.cs
/// FUNCTION		: - Sub module of NetworkConnectionManager.
/// 				  - Contains function to manage the network player instance.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class NetworkPlayerInstanceManager : MonoBehaviour 
{
	//properties
	private byte localPlayerIdentifier;
	private List<NetworkPlayerInstance> networkPlayers = new List<NetworkPlayerInstance>();

	#region info inquiry

	public NetworkPlayerInstance GetLocalPlayer()
	{
		return GetPlayerByIdentifier (localPlayerIdentifier);
	}

	public int GetNetworkPlayersCount()
	{
		return networkPlayers.Count;
	}

	public NetworkPlayerInstance GetPlayerByIdentifier(byte _identifier)
	{
		foreach (NetworkPlayerInstance player in networkPlayers) 
		{
			if (player.identifier == _identifier) 
			{
				return player;
			}
		}

		return null;
	}

	public List<NetworkPlayerInstance> GetAllNetworkPlayerInstance()
	{
		return networkPlayers;
	}

	public List<NetworkConnection> GetAllNetworkConnectionOfPlayers()
	{
		List<NetworkConnection> _result = new List<NetworkConnection> ();

		foreach (NetworkPlayerInstance _candidate in networkPlayers) 
		{
			_result.Add (_candidate.connection);
		}

		return _result;
	}

	public List<NetworkConnection> GetAllNetworkConnectionOfPlayersExcludeSelf()
	{
		List<NetworkConnection> _result = new List<NetworkConnection> ();

		foreach (NetworkPlayerInstance _candidate in networkPlayers) 
		{
			if(_candidate != GetLocalPlayer())
				_result.Add (_candidate.connection);
		}

		return _result;
	}

	public List<NetworkConnection> GetAllNetworkConnectionOfPlayersExcludeSelfAndSpecificConnection(NetworkConnection _targetedConnection)
	{
		List<NetworkConnection> _result = new List<NetworkConnection> ();

		foreach (NetworkPlayerInstance _candidate in networkPlayers) 
		{
			if(_candidate != GetLocalPlayer() && _candidate.connection != _targetedConnection)
				_result.Add (_candidate.connection);
		}

		return _result;
	}

	#endregion

	#region mutator

	public void SetLocalPlayerIdentifier(byte _identifier)
	{
		localPlayerIdentifier = _identifier;
	}

	public void ClearNetworkPlayersInstance()
	{
		networkPlayers = new List<NetworkPlayerInstance>();
	}

	#endregion

	public NetworkPlayerInstance ServerAddNewPlayerInstance(System.Type _instanceType, NetworkPlayerInstanceSyncParameter[] _permanentParameters, NetworkConnection _conn, bool _isSelf)
	{
		NetworkPlayerInstance _newPlayerInstance = (NetworkPlayerInstance)System.Activator.CreateInstance (_instanceType);
		_newPlayerInstance.identifier = (byte)GetUnusedIdentifier();
		_newPlayerInstance.connection = _conn;
		_newPlayerInstance.permanentParemeters = _permanentParameters;

		networkPlayers.Add (_newPlayerInstance);

		if (_isSelf)
			localPlayerIdentifier = _newPlayerInstance.identifier;

		return _newPlayerInstance;
	}

	public NetworkPlayerInstance ClientAddNewPlayerInstance(System.Type _instanceType, byte _identifer, NetworkPlayerInstanceSyncParameter[] _permanentParameters, NetworkPlayerInstanceSyncParameter[] _frequentChangeParameters)
	{
		NetworkPlayerInstance _newPlayer = (NetworkPlayerInstance)System.Activator.CreateInstance (_instanceType);
		_newPlayer.identifier = _identifer;
		_newPlayer.permanentParemeters = _permanentParameters;
		_newPlayer.frequentChangeParameters = _frequentChangeParameters;
		networkPlayers.Add (_newPlayer);

		return _newPlayer;
	}

	public byte RemovePlayerInstanceByConnection(NetworkConnection _conn)
	{
		int _targetedIndex = -1;

		for (int i = 0; i < networkPlayers.Count; i++) 
		{
			if (networkPlayers [i].connection == _conn) 
			{
				_targetedIndex = i;
				break;
			}
		}

		byte _playerIdentifierToBeRemoved = networkPlayers [_targetedIndex].identifier;

		networkPlayers.RemoveAt (_targetedIndex);

		return _playerIdentifierToBeRemoved;
	}

	public void RemovePlayerInstanceByIdentifier(byte _identifier)
	{
		int _targetedIndex = -1;

		for (int i = 0; i < networkPlayers.Count; i++) 
		{
			if (networkPlayers [i].identifier == _identifier) 
			{
				_targetedIndex = i;
				break;
			}
		}
			
		networkPlayers.RemoveAt (_targetedIndex);
	}

	private int GetUnusedIdentifier()
	{
		int _result = 0;

		while (true) 
		{
			bool _identifierUnused = true;

			foreach (NetworkPlayerInstance _candidate in networkPlayers) 
			{
				if (_candidate.identifier == _result) 
				{
					_identifierUnused = false;
					break;
				}
			}

			if (_identifierUnused)
				break;
			else
				_result++;
		}

		return _result;
	}

	public void AssignPlayerNetworkConnectionAuthority(byte _identifier, NetworkPlayerConnectionAuthority _connectionAuthority)
	{
		GetPlayerByIdentifier (_identifier).networkPlayerConnectionAuthority = _connectionAuthority;
	}
}
