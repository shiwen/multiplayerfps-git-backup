﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: CharacterMovement.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Jack Lieu
/// DATE			: 13/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Jack		| 13/05/16	|	- Inherit logic from NetworkMovement scripts.
/// 		|			|			|	- Override base function to create custom movement logic.
/// 		| Cee Jay	| 26/05/16	|	- Added recoil support for camera movement
/// 		| Cee Jay	| 27/05/16	|	- Modified recoil support logic
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

[RequireComponent (typeof (CharacterController))]
public class NetworkCharacterMovement : NetworkMovement {

	[SerializeField] private float snapDistance = 1f;
	[SerializeField] private float movingSpeed = 10f;
	[SerializeField] private float runningSpeed = 10f;
	[SerializeField] private float viewSensitivity = 10f;
	[SerializeField] private float groundCheckDistance = 0.1f;
	[SerializeField] private float gravityMultiplier = 1.0f;

	// To store character forward view direction (Shooting direction)
	private Quaternion viewRotation = Quaternion.identity;
	private bool isInvert = false;

	// Get fps camera that attached on this character
	private Camera fpsCamera = null;

	// Can be replace with rigidbody
	private CharacterController charController = null;

	// Array to collect data
	private Inputs cachedData = new Inputs ();

	private Vector3 forwardDirection = Vector3.zero;
	private float speedMultiplier = 1f;

	private PlayerShoot playerShoot;
	private PlayerManager playerManager;

	void Start ()
	{
		base.Start ();
		charController = transform.GetComponent<CharacterController> ();
		playerManager = transform.GetComponent<PlayerManager> ();
		if (transform.Find ("Camera").GetComponent<Camera> ()) {
			fpsCamera = transform.Find ("Camera").GetComponent<Camera> ();
			viewRotation = transform.rotation;
		}
		else
			Debug.Log ("There is no camera attach to this character.");

		playerShoot = GetComponent<PlayerShoot>();

		// Turn off the camera if the player not the owner for this character
		if (!isLocalPlayer)
			fpsCamera.GetComponent<Camera> ().enabled = false;
	}

	void LateUpdate ()
	{
		this.SendMessage ("UpdateMovementAnimation", cachedData);
	}

	public override void GetInputs (ref Inputs _inputs)
	{
		_inputs.horizontal = Input.GetAxis ("Horizontal");
		_inputs.vertical = Input.GetAxis ("Vertical");
		_inputs.pitch = Input.GetAxis ("Mouse X") * viewSensitivity * Time.fixedDeltaTime/ Time.deltaTime;
		_inputs.yaw = (Input.GetAxis ("Mouse Y") * ((isInvert)? 1: -1))  * viewSensitivity * Time.fixedDeltaTime/ Time.deltaTime;
//		_inputs.crouch = Input.GetKey(KeyCode.LeftShift);
		_inputs.sprint = Input.GetKey(KeyCode.LeftShift);

		_inputs.yaw += playerShoot.recoilValue.x * 100f;
	}

	public override Quaternion Rotate (Inputs _inputs, Results _results)
	{
		viewRotation = _results.rotation;
		transform.rotation = Quaternion.Euler (0, _results.rotation.eulerAngles.y, 0);

		cachedData.pitch = _inputs.pitch;
		cachedData.yaw = _inputs.yaw;
		float hori = _results.rotation.eulerAngles.y + _inputs.pitch * Time.fixedDeltaTime;
		float vert = _results.rotation.eulerAngles.x + _inputs.yaw * Time.fixedDeltaTime;

		if (vert > 180f)
			vert -= 360f;

		vert = Mathf.Clamp (vert, -170f * 0.5f, 170f * 0.5f);

		//vert += playerShoot.recoilValue.x;

		Quaternion rotAngle = Quaternion.identity;

//		rotAngle = Quaternion.Euler (0, hori, 0);
//		viewRotation = Quaternion.Euler (vert, hori, 0);

		playerManager.Rotate (ref rotAngle, ref viewRotation, vert, hori);

//		if (fpsCamera.gameObject.activeInHierarchy)

		transform.rotation = rotAngle;
		fpsCamera.transform.rotation = viewRotation;

		return viewRotation;
	}

	public override Vector3 Move (Inputs _inputs, Results _results)
	{
		transform.position = _results.position;

		cachedData.horizontal = _inputs.horizontal;
		cachedData.vertical = _inputs.vertical;

		float vertical = (_inputs.vertical > 0f)? ((_inputs.vertical > 0f)? 1f: 0f): ((_inputs.vertical < 0f)? -1f: 0f);
		float horizontal = (_inputs.horizontal > 0f)? ((_inputs.horizontal > 0f)? 1f: 0f): ((_inputs.horizontal < 0f)? -1f: 0f);

		Vector3 desiredMove = transform.forward * vertical 
			+ transform.right * horizontal;
		Vector3 resultPosition = Vector3.zero;

		// get a normal for the surface that is being touched to move along it
		RaycastHit hitInfo;
		Physics.SphereCast(transform.position, charController.radius, Vector3.down, out hitInfo,
			charController.height/2f, ~0, QueryTriggerInteraction.Ignore);

		desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;
		desiredMove.y += Physics.gravity.y * gravityMultiplier * Time.fixedDeltaTime;

		resultPosition.x = desiredMove.x;
		resultPosition.y = desiredMove.y;
		resultPosition.z = desiredMove.z;

//		charController.Move (resultPosition * Time.fixedDeltaTime * (movingSpeed * speedMultiplier));
		playerManager.Move (resultPosition * Time.fixedDeltaTime * (movingSpeed * speedMultiplier));

		return transform.position;
	}

	public override bool Crouch (Inputs _inputs, Results _results)
	{
		// Currently no crouch is implemented
		return false;
	}

	public override bool Sprint (Inputs _inputs, Results _results)
	{
		if (_inputs.vertical < 0f || _inputs.horizontal != 0f) {
			speedMultiplier = 1f;
			return false;
		}

		if (_inputs.sprint) {
			speedMultiplier = Mathf.Lerp (speedMultiplier, runningSpeed/ movingSpeed, Time.fixedDeltaTime);
		} else {
			speedMultiplier = 1f;
		}

		cachedData.sprint = _inputs.sprint;
		return _inputs.sprint;
	}

	public override void UpdatePosition (Vector3 _pos)
	{
		forwardDirection = Vector3.Lerp (forwardDirection, transform.InverseTransformDirection (charController.velocity), movingSpeed * Time.fixedDeltaTime);
		cachedData.horizontal = Mathf.Min (forwardDirection.x, 1f);
		cachedData.vertical = Mathf.Min (forwardDirection.z, 1f);

		// Force position sync if the distance is too far
		if (Vector3.Distance (_pos, transform.position) > snapDistance)
			transform.position = _pos;
		else {
			playerManager.Move ((_pos - transform.position));
		}
	}

	public override void UpdateRotation (Quaternion _newRot)
	{
//		cachedData.pitch = _inputs.pitch;
//		cachedData.yaw = _inputs.yaw;

		transform.rotation = Quaternion.Euler (0f, _newRot.eulerAngles.y, 0f);
		viewRotation = _newRot;
		fpsCamera.transform.rotation = viewRotation;
	} 

	public override void UpdateCrouch (bool _crouching)
	{
		cachedData.crouch = _crouching;
	}

	public override void UpdateSprint (bool _sprinting)
	{
		cachedData.sprint = _sprinting;
	}
		
	// Temporary solution to identity own character
//	public override void OnStartLocalPlayer ()
//	{
//		if (this.GetComponent<MeshRenderer> ())
//			this.GetComponent<MeshRenderer> ().material.color = Color.blue;
//	}
}
