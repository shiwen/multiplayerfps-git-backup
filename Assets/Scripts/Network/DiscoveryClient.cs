﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: DiscoveryServer.cs
/// FUNCTION		: - Sub module of NetworkConnectionManager.
/// 				  - Contains function to read the available braodcasted room info.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DiscoveredServer : BroadcastData
{
	public string rawData;
	public string ipAddress;
	public float timestamp;

	public DiscoveredServer(BroadcastData aData)
	{
		version = aData.version;
		peerId = aData.peerId;
		isStarted = aData.isStarted;
		numPlayers = aData.numPlayers;
		maxPlayers = aData.maxPlayers;
		base.roomName = aData.roomName;
		base.gameMode = aData.gameMode;
	}
}

public class DiscoveryClient : NetworkDiscovery
{
	public NetworkConnectionManager networkConnectionManager;
	public Dictionary<string, DiscoveredServer> discoveredServers;
	public const float ServerKeepAliveTime = 3.0f;

	public Queue<string> receivedBroadcastLog;

	private const int maxLogLines = 4;
	private const string broadcastLogTokens = "-.";
	private int broadcastLogCounter = 0;

	public void Setup(NetworkConnectionManager _lobbyNetworkManager)
	{
		networkConnectionManager = _lobbyNetworkManager;
		broadcastKey = networkConnectionManager.broadcastIdentifier.GetHashCode(); // Make sure broadcastKey matches server

		discoveredServers = new Dictionary<string, DiscoveredServer>();
		receivedBroadcastLog = new Queue<string>();
		showGUI = false;

		StartRetreiveRoomList ();
	}

	public void Reset()
	{
		discoveredServers.Clear();
		receivedBroadcastLog.Clear ();
	}

	public void StartRetreiveRoomList()
	{
		Debug.Log ("Start retrieving room list");
		
		Reset();

		if (!Initialize()) {
			Debug.LogError("Network port is unavailable!");
		}
		if (!StartAsClient()) {
			Debug.LogError("Unable to listen!");
		}

		CancelInvoke ();
		InvokeRepeating("CleanServerList", 3, 1);
    }

	public List<DiscoveredServer> CleanServerList()
	{
		var toRemove = new List<string>();
		List<DiscoveredServer> removedServers = new List<DiscoveredServer> ();

		foreach (var kvp in discoveredServers)
		{
			float timeSinceLastBroadcast = Time.time - kvp.Value.timestamp;

			if (timeSinceLastBroadcast > ServerKeepAliveTime) 
			{
				removedServers.Add (kvp.Value);
				toRemove.Add(kvp.Key);
			}
		}

		foreach (string server in toRemove)
		{
			networkConnectionManager.OnServerRemoved (discoveredServers[server]);
			discoveredServers.Remove(server);
		}

		return removedServers;
	}

    public override void OnReceivedBroadcast(string aFromAddress, string aRawData)
	{
    	BroadcastData data = new BroadcastData();
    	data.FromString(aRawData);

    	// DEBUG LOG
		broadcastLogCounter += 1;
    	receivedBroadcastLog.Enqueue(broadcastLogTokens[broadcastLogCounter % broadcastLogTokens.Length] + " " + aRawData);
    	if (receivedBroadcastLog.Count > maxLogLines) {
    		receivedBroadcastLog.Dequeue();
    	}

    	var server = new DiscoveredServer(data);
    	server.rawData = aRawData;
		server.ipAddress = aFromAddress;
    	server.timestamp = Time.time;

    	bool newData = false;
		bool newServer = false;

    	if (!discoveredServers.ContainsKey(aFromAddress))
		{
    		// New server
			discoveredServers.Add(aFromAddress, server);
			newData = true;
			newServer = true;
		}
		else
		{
	    	if (discoveredServers[aFromAddress].rawData != aRawData)
	    	{
	    		// Old server with new info
	    		discoveredServers[aFromAddress] = server;
    			newData = true;
    		}
    		else
    		{
    			// Just update the timestamp
    			discoveredServers[aFromAddress].timestamp = Time.time;
    			newData = false;
    		}
    	}

    	if (newServer)
		{
			networkConnectionManager.OnServerDiscovered(server);
        }
    }

	public void JoinRoomStopBroadCast()
	{
		CancelInvoke ();
		StopBroadcast ();
	}

	public void ReStartBroadcast()
	{
		Reset ();
		Initialize ();
		StartAsClient ();
		CancelInvoke ();
		InvokeRepeating("CleanServerList", 3, 1);
	}
}
