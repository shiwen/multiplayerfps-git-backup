﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: NetworkPlayerInstance.cs
/// FUNCTION		: - will be created by NetworkConnectionManager once a player is connected to room.
/// 				  - Contains info that required to identify a player.
/// 				  - Can be inherit to create a more specific class that will store specific parameters. (ex: name, room slot index, etc)
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------


using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NetworkPlayerInstance
{
	public byte identifier;
	public bool isLocalPlayer;
	public NetworkConnection connection;
	public NetworkPlayerConnectionAuthority networkPlayerConnectionAuthority;
	public NetworkPlayerInstanceSyncParameter[] permanentParemeters;
	public NetworkPlayerInstanceSyncParameter[] frequentChangeParameters;

	public void UpdateFrequentChangeParameterValue(NetworkPlayerInstanceSyncParameter _targetParameter)
	{
		GetSyncParameterByKey (frequentChangeParameters, _targetParameter.GetParameterKey()).parameterValue = _targetParameter.parameterValue;
	}

	public NetworkPlayerInstanceSyncParameter GetFrequentUpdateParamaterByKey(string _targetedKey)
	{
		return GetSyncParameterByKey (frequentChangeParameters, _targetedKey);
	}

	private NetworkPlayerInstanceSyncParameter GetSyncParameterByKey(NetworkPlayerInstanceSyncParameter[] _parametersHolder, string _targetedKey)
	{
		foreach (NetworkPlayerInstanceSyncParameter _candidate in _parametersHolder) 
		{
			if (_candidate.GetParameterKey () == _targetedKey)
				return _candidate;
		}

		return null;
	}

	protected virtual void ImplementParameters()
	{
	}

	public NetworkPlayerInstance()
	{
		ImplementParameters ();
	}
}

