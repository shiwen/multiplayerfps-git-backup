﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: NetworkConnectionEventReactor.cs
/// FUNCTION		: - Listener function for various connection related situations
/// 				  - Inherrit this class to override and specify the methods.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public abstract class NetworkConnectionEventReactor: MonoBehaviour
{
	public virtual void OnPreStartGame()
	{
	}
	
	public virtual void OnStartGameCalled()
	{
	}

	public virtual void OnGameplayLevelLoaded()
	{
	}

	public virtual void OnDisconnectCalled()
	{
	}

	public virtual void OnConnectedToRoom(bool _gameIsStarted, bool _isHost)
	{
	}

	public virtual void PostJoinedRoomSync(bool _gameIsStarted)
	{
	}

	public virtual void OnServerDiscovered(DiscoveredServer _discoveredServer)
	{
	}

	public virtual void OnServerRemoved(DiscoveredServer _removedServer)
	{
	}

	public virtual void OnPlayerInstanceAdded(bool _isHost, NetworkPlayerInstance _networkPlayerInstance)
	{
	}

	public virtual void OnPlayerInstanceRemoved(byte _identifier)
	{
	}

	public virtual void ServerOnPlayerPostStartGameJoinedGame(NetworkPlayerInstance _joinedRoomPlayer)
	{
	}
}
