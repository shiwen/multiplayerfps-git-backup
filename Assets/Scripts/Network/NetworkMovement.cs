﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: NetworkCharacter.cs
/// FUNCTION		: This network class is created mainly for fps and tps game.
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Jack Lieu
/// DATE			: 10/05/16
/// SCRIPT REF		: https://github.com/GenaSG/UnityUnetMovement/blob/master/Scripts/NetworkMovement.cs
/// -----------------------------------------------------------------------------------------------------------------------------
/// DESCRIPTION
/// 
/// This class can be inherit to create custom movement controller based on game requirement. Both PREDICTION
/// and RECONCILIATION method is implemented to sync state between server and client.
/// - Client-side PREDICTION	: Immediately predict movement before server process the input. 
/// - Server RECONCILIATION		: Client will store any input before sending it to server, once server has 
/// 							  authorize the input and send to to client, client will need to discard any
/// 							  processed input and only continue with the unprocessed data. (To prevent unwanted 
/// 							  replay or force sync effect occur)
/// 
/// Channel description :
/// #0	: Reliable sequenced (For state update)
/// #1	: Unreliable Sequenced (Recommended for movement update such as position and rotation)
/// 
/// 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Jack		| 10/05/16	|	- Implement basic function to move character based on input.
/// 		|			|			|	- Implement client-side prediction and server reconciliation logic
/// 		|			|			|	  to sync game state between server and client.
/// 		|			|			|	- Create virtual function to allow child class to create custom logic.
/// 		|			|			|	- Client now run data based on server time stamp, client side time update every 0.05s.
/// 0.2		| Jack		| 16/05/16	|	- Added new bool data to sync :~ crouch, sprint.
/// 		|			|			|	- Create virtual input authorization function. (Input should be verified on
/// 		|			|			|	  server side before send back to client)
/// 		|			|			|	- Add array to store at least 1000ms of data.
/// 0.3		| Jack		| 18/05/16	|	- Add function to force sync client data.
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

[NetworkSettings (channel = 1, sendInterval = 0.05f)]
public class NetworkMovement : NetworkBehaviour {

	public struct Inputs
	{
		// movement
		public float vertical;
		public float horizontal;

		//rotation
		public float yaw;
		public float pitch;

		//action
		public bool sprint;
		public bool crouch;

		public float timeStamp;
	}

	public struct SyncInputs
	{
		// movement
		public float vertical;
		public float horizontal;

		//rotation
		public float yaw;
		public float pitch;

		//action
		public bool sprint;
		public bool crouch;

		public float timeStamp;
	}

	public struct Results
	{
		// movement
		public Vector3 position;

		//rotation
		public Quaternion rotation;

		//action
		public bool sprinting;
		public bool crouching;
		
		public float timeStamp;
	}

	public struct SyncResults
	{
		// movement
		public Vector3 position;

		//rotation
		public float yaw;
//		public ushort yaw;
		public float pitch;
//		public ushort pitch;

		//action
		public bool sprinting;
		public bool crouching;

		public float timeStamp;
	}

	private Inputs mInputs;
	private Results mResults;

	// Store inputs that belong to client and server owner
	private List<Inputs> mInputsList = new List<Inputs> ();

	// Store results for non-owner client interpolation
	private List<Results> mResultsList = new List<Results> ();

	// Interpolation related variables
	private bool mPlayData = false;
	private float mDataStep = 0f;
	private float mStep = 0f;
	private float mInterpolateInterval = 0.2f;
	private float mInterpolateStep = 0f;
	private Vector3 mStartPosition = Vector3.zero;
	private Quaternion lookRotation;
	private Quaternion mStartRotation = Quaternion.identity;

	// Store at least 1000ms executed result to do replay
	private SyncResults[] syncResultArray = new SyncResults[20];
	private int syncStep = 0;
	private float stateSyncInterval = 5.0f;
	private float lastStateSyncTime = 0.0f;
	private float lastReceivedTimeStamp = 0.0f;

	[SyncVar]
	private float serverTimeStamp = 0f;

	public void Awake ()
	{
		Application.runInBackground = true;

		mResults.position = transform.position;
		mResults.rotation = transform.rotation;
		mResults.crouching = false;
		mResults.sprinting = false;
	}

	public void Start ()
	{
//		if (isLocalPlayer)
//		{
//			latencyDisplay = GameObject.Find ("pingDisplay").GetComponent<Text> ();
//			serverTime = GameObject.Find ("timeDisplay").GetComponent<Text> ();
//		}
	}

	public void Update()
	{
		if (isLocalPlayer) {
			GetInputs (ref mInputs);
		}

		UpdateServerTime ();
	}

	public void FixedUpdate ()
	{
		if (isLocalPlayer) {
			mInputs.timeStamp = Time.time;

			// Client side prediction for non-authoritative client
			Vector3 lastPosition = mResults.position;
			Quaternion lastRotation = mResults.rotation;
			bool lastCrouch = mResults.crouching;
			mResults.position = Move (mInputs, mResults);
			mResults.rotation = Rotate (mInputs, mResults);
			mResults.crouching = Crouch (mInputs, mResults);
			mResults.sprinting = Sprint (mInputs, mResults);

			if (hasAuthority) {
				// Listen to server/ host
				// Sending result to other clients (state sync)
				if (mDataStep >= GetNetworkSendInterval ()) {
					if (Vector3.Distance (mResults.position, lastPosition) > 0 || 
						Quaternion.Angle (mResults.rotation, lastRotation) > 0 ||
						mResults.crouching != lastCrouch) {

						mResults.timeStamp = mInputs.timeStamp;

						// Struct need to be fully new
						// Convert result to better variable for less traffic
						SyncResults tempSyncData;
						tempSyncData.position = mResults.position;
//						tempSyncData.pitch = (ushort)(mResults.rotation.eulerAngles.x * 182);
						tempSyncData.pitch = mResults.rotation.eulerAngles.y;
//						tempSyncData.yaw = (ushort)(mResults.rotation.eulerAngles.y * 182);
						tempSyncData.yaw = mResults.rotation.eulerAngles.x;
						tempSyncData.crouching = mResults.crouching;
						tempSyncData.sprinting = mResults.sprinting;

						tempSyncData.timeStamp = mResults.timeStamp;
						UpdateSyncArray (tempSyncData);
						Rpc_SyncInputs (tempSyncData);
					}
					mDataStep = 0;
				}
				mDataStep += Time.fixedDeltaTime;
			} else {
				// Client's owner, non-authoritative part
				// Add inputs to the input list so they could be used during reconciliation process
				if (Vector3.Distance (mResults.position, lastPosition) > 0 || 
					Quaternion.Angle (mResults.rotation, lastRotation) > 0 ||
					mResults.crouching != lastCrouch) {
					mInputsList.Add (mInputs);
				}

				// Sending inputs to the server
				// Create temporary variable to save on network traffic
				SyncInputs syncInputs;
				syncInputs.vertical = (sbyte)(mInputs.vertical * 127);
				syncInputs.horizontal = (sbyte)(mInputs.horizontal * 127);

				if (Vector3.Distance (mResults.position, lastPosition) > 0) {
					if (Quaternion.Angle (mResults.rotation, lastRotation) > 0) {
//						Cmd_MovementRotationInputs (syncInputs.vertical, syncInputs.horizontal, mInputs.pitch, mInputs.yaw, mInputs.crouch, mInputs.sprint, mInputs.timeStamp);
						Cmd_MovementRotationInputs (mInputs.vertical, mInputs.horizontal, mInputs.pitch, mInputs.yaw, mInputs.crouch, mInputs.sprint, mInputs.timeStamp);
					} else {
//						Cmd_MovementInputs (syncInputs.vertical, syncInputs.horizontal, mInputs.crouch, mInputs.sprint, mInputs.timeStamp);
						Cmd_MovementInputs (mInputs.vertical, mInputs.horizontal, mInputs.crouch, mInputs.sprint, mInputs.timeStamp);
					}
				} else {
					if (Quaternion.Angle (mResults.rotation, lastRotation) > 0) {
						Cmd_RotationInputs (mInputs.pitch, mInputs.yaw, mInputs.crouch, mInputs.timeStamp);
					} else if (mResults.crouching != lastCrouch){
						Cmd_OnlyStances (mInputs.crouch, mInputs.timeStamp);
					}
				}
			}
		} else {
			if (hasAuthority) {
				// Server

				// Check if there is any record in inputs list
				if (mInputsList.Count == 0)
					return;

				Inputs inputs = mInputsList[0];
				mInputsList.RemoveAt(0);

				Vector3 lastPosition = mResults.position;
				Quaternion lastRotation = mResults.rotation;
				bool lastCrouch = mResults.crouching;
				mResults.position = Move (inputs, mResults);
				mResults.rotation = Rotate (inputs, mResults);
				mResults.crouching = Crouch (inputs, mResults);
				mResults.sprinting = Sprint (inputs, mResults);

				// Send result to other clients (state sync)
				if (mDataStep >= GetNetworkSendInterval ()) {
					if (Vector3.Distance (mResults.position, lastPosition) > 0 || 
						Quaternion.Angle (mResults.rotation, lastRotation) > 0 ||
						mResults.crouching != lastCrouch) {

						mResults.timeStamp = inputs.timeStamp;
						// Struct need to be fully new
						// Convert result to better variable for less traffic
						SyncResults tempSyncData;
						tempSyncData.position = mResults.position;
						tempSyncData.timeStamp = mResults.timeStamp;
//						tempSyncData.pitch = (ushort)(mResults.rotation.eulerAngles.y * 182);
						tempSyncData.pitch = mResults.rotation.eulerAngles.y;
//						tempSyncData.yaw = (ushort)(mResults.rotation.eulerAngles.x * 182);
						tempSyncData.yaw = mResults.rotation.eulerAngles.x;
						tempSyncData.crouching = mResults.crouching;
						tempSyncData.sprinting = mResults.sprinting;

						UpdateSyncArray (tempSyncData);
						Rpc_SyncInputs (tempSyncData);
					}
					mDataStep = 0;
				}
				mDataStep += Time.fixedDeltaTime;
			} else {
				if (mResultsList.Count == 0)
					mPlayData = false;

				if (mResultsList.Count >= 1)
					mPlayData = true;

				if (mPlayData) {
					if (mDataStep == 0f) {
						mStartPosition = mResults.position;
						mStartRotation = mResults.rotation;
					}
						
					mStep = (1/ GetNetworkSendInterval ());
					mDataStep += mStep * Time.fixedDeltaTime;
					mDataStep = Mathf.Min (1.0f, mDataStep);

					mResults.position = Vector3.Lerp (mStartPosition, mResultsList[0].position, mDataStep);
					mResults.rotation = Quaternion.Lerp (mStartRotation, mResultsList[0].rotation, mDataStep);
					mResults.crouching = mResultsList[0].crouching;
					mResults.sprinting = mResultsList[0].sprinting;

					if (mDataStep >= 1f) {
						mDataStep = 0f;
						mResultsList.RemoveAt (0);
					}
				}

				UpdatePosition (mResults.position);
				UpdateRotation (mResults.rotation);
				UpdateCrouch (mResults.crouching);
				UpdateSprint (mResults.sprinting);
			}
		}

		if (hasAuthority && isServer) {
			if ((serverTimeStamp - lastStateSyncTime) > stateSyncInterval) {
//				Rpc_StateSync (syncResultArray[syncStep]);
				lastStateSyncTime = serverTimeStamp;
			}
		}
	}

	// Call this upon instantiate to sync start position
	public void SetStartPosition (Vector3 _pos) {
		mResults.position = _pos;
	}

	// Call this upon instantiate to sync start rotation
	public void SetStartRotation (Quaternion _rot) {
		mResults.rotation = _rot;
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Following function can be changed in inherited class
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	#region Virtual function
	// Override this to create custom input pattern based on gameplay
	public virtual void GetInputs (ref Inputs _inputs)
	{
		// This is the example of input implementation
		mInputs.horizontal = Input.GetAxisRaw("Horizontal");
		mInputs.vertical = Input.GetAxisRaw("Vertical");
		mInputs.pitch = Input.GetAxisRaw("Mouse X") * 10f * Time.fixedDeltaTime/ Time.deltaTime;
		mInputs.yaw = Input.GetAxisRaw("Mouse Y") * 10f * Time.fixedDeltaTime/ Time.deltaTime;
	}

	/// <summary>
	/// Override this to create custom input authorization function
	/// </summary>
	/// <returns><c>true</c>, if inputs is valid compare with server data, <c>false</c> otherwise.</returns>
	/// <param name="_results">Input Sent.</param>
	public virtual bool GetAuthorizedInputs (ref Inputs _inputs)
	{
		return true;
	}

	// Override this to create custom rotating function
	public virtual Quaternion Rotate (Inputs _inputs, Results _results)
	{
		return transform.rotation;
	}

	// Override this to create custom moving function
	public virtual Vector3 Move (Inputs _inputs, Results _results)
	{
		return transform.position;
	}

	// Override this to create custom crouching function
	public virtual bool Crouch (Inputs _inputs, Results _results)
	{
		return _inputs.crouch;
	}

	// Override this to create custom sprinting function
	public virtual bool Sprint (Inputs _inputs, Results _results)
	{
		return _inputs.sprint;
	}

	public virtual void UpdateRotation (Quaternion _Rot)
	{
		transform.rotation = _Rot;
	}

	public virtual void UpdatePosition (Vector3 _pos)
	{
		transform.position = _pos;
	}

	public virtual void UpdateCrouch (bool _crouching)
	{
		
	}

	public virtual void UpdateSprint (bool _sprinting)
	{
		
	}
	#endregion

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Following function provide communication between server and client. 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	#region Network function
	[Command (channel = 0)]
	void Cmd_OnlyStances (bool _crouch, float _timeStamp)
	{
		if (hasAuthority && !isLocalPlayer) {
			Inputs inputs;
			inputs.vertical = 0f;
			inputs.horizontal = 0f;
			inputs.pitch = 0f;
			inputs.yaw = 0f;
			inputs.crouch = _crouch;
			inputs.sprint = false;
			inputs.timeStamp = _timeStamp;

			mInputsList.Add (inputs);
		}
	}

	[Command (channel = 0)]
	void Cmd_MovementInputs (float _vertical, float _horizontal, bool _crouch, bool _sprint, float _timeStamp)
	{
		if (hasAuthority && !isLocalPlayer) {
			Inputs inputs;
//			inputs.vertical = Mathf.Clamp ((float)_vertical/ 127, -1, 1);
			inputs.vertical = _vertical;
//			inputs.horizontal = Mathf.Clamp ((float)_horizontal/ 127, -1, 1);
			inputs.horizontal = _horizontal;
			inputs.pitch = 0f;
			inputs.yaw = 0f;
			inputs.crouch = _crouch;
			inputs.sprint = _sprint;
			inputs.timeStamp = _timeStamp;

			mInputsList.Add (inputs);
		}
	}

	[Command (channel = 0)]
	void Cmd_RotationInputs (float _pitch, float _yaw, bool _crouch, float _timeStamp)
	{
		if (hasAuthority && !isLocalPlayer) {
			Inputs inputs;
			inputs.vertical = 0f;
			inputs.horizontal = 0f;
			inputs.pitch = _pitch;
//			inputs.pitch = (float)(_pitch/ 182);
			inputs.yaw = _yaw;
//			inputs.yaw = (float)(_yaw/ 182);
			inputs.crouch = _crouch;
			inputs.sprint = false;
			inputs.timeStamp = _timeStamp;
			mInputsList.Add (inputs);
		}
	}

	[Command (channel = 0)]
	void Cmd_MovementRotationInputs (float _vertical, float _horizontal, float _pitch, float _yaw, bool _crouch, bool _sprint, float _timeStamp)
	{
		if (hasAuthority && !isLocalPlayer) {
			Inputs inputs;
//			inputs.vertical = Mathf.Clamp ((float)_vertical/ 127, -1, 1);
			inputs.vertical = _vertical;
//			inputs.horizontal = Mathf.Clamp ((float)_horizontal/ 127, -1, 1);
			inputs.horizontal = _horizontal;
			inputs.pitch = _pitch;
//			inputs.pitch = (float)(_pitch/ 182);
			inputs.yaw = _yaw;
//			inputs.yaw = (float)(_yaw/ 182);
			inputs.crouch = _crouch;
			inputs.sprint = _sprint;
			inputs.timeStamp = _timeStamp;

			mInputsList.Add (inputs);
		}
	}

	[ClientRpc (channel = 0)]
	void Rpc_SyncInputs (SyncResults _results)
	{
		// Cache values for local process
		Results results = new Results ();
		results.position = _results.position;
//		results.rotation = Quaternion.Euler ((float)(_results.yaw/ 182), (float)(_results.pitch/ 182), 0);
		results.rotation = Quaternion.Euler (_results.yaw, _results.pitch, 0);
		results.crouching = _results.crouching;
		results.sprinting = _results.sprinting;
		results.timeStamp = _results.timeStamp;

		// Discard out of order results
		if (results.timeStamp <= lastReceivedTimeStamp) 
			return;

		lastReceivedTimeStamp = results.timeStamp;

		// Non-owner client
		if (!isLocalPlayer && !hasAuthority) {
			// Add results to results list for interpolation process
			// results.timeStamp = serverTimeStamp;
			mResultsList.Add (results);	
		}

		// Owner client
		// Server client reconciliation process
		if (!hasAuthority && isLocalPlayer) {
			// Update client's data with ones from server
//			mResults.position = results.position;
//			mResults.rotation = results.rotation;

			int foundIndex = -1;
			// Search received time stamp in client's inputs list
			for (int i = 0; i < mInputsList.Count; i ++) {
				// Retrive the starting index for all the needed data
				if (mInputsList[i].timeStamp > results.timeStamp) {
					foundIndex = i;
					break;
				}
			}

			// No needed record found, clear all the cached record
			if (foundIndex == -1) {
				mInputsList.Clear ();
				return;
			}

			mResults.position = results.position;
			mResults.rotation = results.rotation;

			// Replay recorded inputs
			for (int i = foundIndex; i < mInputsList.Count; ++ i) {
				mResults.position = Move (mInputsList[i], mResults);
				mResults.rotation = Rotate (mInputsList[i], mResults);
				mResults.crouching = Crouch (mInputsList[i], mResults);
				mResults.sprinting = Sprint (mInputsList[i], mResults);
			}

			// Remove all inputs before time stamp
			int targetCount = mInputsList.Count - foundIndex;
			while (mInputsList.Count > targetCount) {
				mInputsList.RemoveAt (0);
			}
		}
	}

	[ClientRpc (channel = 0)]
	void Rpc_StateSync (SyncResults _results)
	{
		// Cache values for local process
		Results results = new Results ();
		results.position = _results.position;
		results.rotation = Quaternion.Euler ((float)(_results.pitch/ 182), (float)(_results.yaw/ 182), 0);
		results.crouching = _results.crouching;
		results.sprinting = _results.sprinting;
		results.timeStamp = _results.timeStamp;

		if (!hasAuthority) {
			// At least 1000ms delay detected
			if ((results.timeStamp - lastReceivedTimeStamp) > 1.0f) {
				Debug.Log ("Forcing update to latest data from server.");
				UpdatePosition (results.position);
				UpdateRotation (results.rotation);
				UpdateCrouch (results.crouching);
				UpdateSprint (results.sprinting);
			}
		}
	}
	#endregion 

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Server only function 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	[ServerCallback]
	void UpdateServerTime ()
	{
		// TODO: server timestamp must be obtain from network manager
		serverTimeStamp = Time.time;
	}

	[ServerCallback]
	void UpdateSyncArray (SyncResults _result)
	{
		if (syncStep >= (syncResultArray.Length - 1))
			syncStep = 0;

		syncResultArray[syncStep] = _result;
		syncStep ++;
	}

	[ServerCallback]
	Inputs AuthorizeInputs (Inputs _input)
	{
		// Cache values for local process
		Inputs inputs = new Inputs ();
		inputs.horizontal = _input.horizontal;
		inputs.vertical = _input.vertical;
		inputs.pitch = _input.pitch;
		inputs.yaw = _input.yaw;
		inputs.crouch = _input.crouch;
		inputs.sprint = _input.sprint;
		inputs.timeStamp = _input.timeStamp;

		bool isValid = GetAuthorizedInputs (ref inputs);

		return inputs;
	}
}
