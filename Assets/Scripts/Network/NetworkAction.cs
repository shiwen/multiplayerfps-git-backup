﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: NetworkAction.cs
/// FUNCTION		: This network class is created mainly for fps and tps game.
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Jack Lieu
/// DATE			: 19/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// DESCRIPTION
/// 
/// 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Jack		| 19/05/16	|	- Implement basic function to perform action based on input.
/// 		|			|			|	- Implement client-side prediction and server reconciliation logic
/// 		|			|			|	  to sync game state between server and client.
/// 		|			|			|	- Create virtual function to allow child class to create custom logic.
/// 		|			|			|	- Client now run data based on server time stamp.
/// 0.2		| Jack		| 30/05/16	|	- Fix shoot direction not updated on server side.
/// 		|			|			|	- Fix shoot boolean still stay true on server side.
/// </summary> ------------------------------------------------------------------------------------------------------------------
/// 
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

[NetworkSettings (channel = 1, sendInterval = 0.05f)]
public class NetworkAction : NetworkBehaviour {

	public struct Inputs
	{
		//action
		public bool reload;
		public bool shoot;

		public float timeStamp;
	}

	public struct Results
	{
		//action
		public bool reloading;
		public bool shooting;

		public float timeStamp;
	}

	public struct SyncResults
	{
		//action
		public bool reloading;
		public bool shooting;

		public float timeStamp;
	}

	private Inputs mInputs;
	private Results mResults;

	// Store inputs that belong to client and server owner
	private List<Inputs> mInputsList = new List<Inputs> ();

	// Store results for non-owner client interpolation
	private List<Results> mResultsList = new List<Results> ();

	private float lastReceivedTimeStamp = 0.0f;

	[SyncVar]
	private float serverTimeStamp = 0f;

	public void Update ()
	{
		if (isLocalPlayer) {
			GetInputs (ref mInputs);
		}

		UpdateServerTime ();
	}

	public void FixedUpdate ()
	{
		if (isLocalPlayer) {
			mInputs.timeStamp = Time.time;

			// This section belong to owner's client
			// Prediction will goes here
			bool lastReloading = mResults.reloading;
			mResults.shooting = Shoot (mInputs, mResults);
			mResults.reloading = Reload (mInputs, mResults);

			if (hasAuthority) {
				// Only server has the authority to control everything

				if (mResults.reloading != lastReloading 
					|| mResults.shooting == true) {
					// Input triggerred

					mResults.timeStamp = mInputs.timeStamp;

					SyncResults tempSyncResult = new SyncResults ();
					tempSyncResult.reloading = mResults.reloading;
					tempSyncResult.shooting = mResults.shooting;
					tempSyncResult.timeStamp = mResults.timeStamp;

					Rpc_SyncInputs (tempSyncResult);
				}
			} else {
				// Client's owner, non-authoritative part
				// Add inputs to the input list so they could be used during reconciliation process

				if (mResults.reloading != lastReloading 
					|| mResults.shooting == true) {
					mInputsList.Add (mInputs);

					Cmd_TriggerAction (mInputs.reload, mInputs.shoot, mInputs.timeStamp);
				}
			}
		} else {
			if (hasAuthority) {
				// Server

				// Check if there is any record in inputs list
				if (mInputsList.Count == 0)
					return;

				Inputs inputs = mInputsList[0];
				mInputsList.RemoveAt(0);

				bool lastReloading = mResults.reloading;
				mResults.shooting = Shoot (inputs, mResults);
				mResults.reloading = Reload (inputs, mResults);

				// Send result to other clients (state sync)
				if (mResults.reloading != lastReloading 
					|| mResults.shooting == true) {
					// Input triggerred

					mResults.timeStamp = inputs.timeStamp;

					SyncResults tempSyncResult = new SyncResults ();
					tempSyncResult.reloading = mResults.reloading;
					tempSyncResult.shooting = mResults.shooting;
					tempSyncResult.timeStamp = mResults.timeStamp;

					Rpc_SyncInputs (tempSyncResult);
				}

			} else {
				if (mResultsList.Count == 0)
					return;

				mResults.reloading = mResultsList[0].reloading;
				mResults.shooting = mResultsList[0].shooting;

				mResultsList.RemoveAt (0);

				UpdateReload (mResults.reloading);
				UpdateShoot (mResults.shooting);
			}
		}
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Following function can be changed in inherited class
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	#region Virtual function
	// Override this to create custom input pattern based on gameplay
	public virtual void GetInputs (ref Inputs _inputs)
	{
		// This is the example of input implementation
		_inputs.reload = Input.GetKeyUp (KeyCode.R);
		_inputs.shoot = Input.GetButton("Fire1");
	}

	// Override this to create custom crouching function
	public virtual bool Reload (Inputs _inputs, Results _results)
	{
		return _inputs.reload;
	}

	// Override this to create custom sprinting function
	public virtual bool Shoot (Inputs _inputs, Results _results)
	{
		return _inputs.shoot;
	}

	public virtual void UpdateReload (bool _crouching)
	{

	}

	public virtual void UpdateShoot (bool _sprinting)
	{

	}
	#endregion

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Following function provide communication between server and client. 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	#region Network function
	[Command (channel = 0)]
	void Cmd_TriggerAction (bool _reload, bool _shoot, float _timeStamp)
	{
		if (hasAuthority && !isLocalPlayer) {
			Inputs inputs = new Inputs ();
			inputs.reload = _reload;
			inputs.shoot = _shoot;
			inputs.timeStamp = _timeStamp;

			mInputsList.Add (inputs);
		}
	}

	[ClientRpc (channel = 0)]
	void Rpc_SyncInputs (SyncResults _results)
	{
		// Cache values for local process
		Results results = new Results ();
		results.reloading = _results.reloading;
		results.shooting = _results.shooting;
		results.timeStamp = _results.timeStamp;

		// Discard out of order results
		if (results.timeStamp <= lastReceivedTimeStamp) 
			return;

		lastReceivedTimeStamp = results.timeStamp;

		// Non-owner client
		if (!isLocalPlayer && !hasAuthority) {
			// Add results to results list for interpolation process
			// results.timeStamp = serverTimeStamp;
			mResultsList.Add (results);	
		}

		// Owner client
		// Server client reconciliation process
		if (!hasAuthority && isLocalPlayer) {
			// Update client's data with ones from server

			int foundIndex = -1;
			// Search received time stamp in client's inputs list
			for (int i = 0; i < mInputsList.Count; i ++) {
				// Retrive the starting index for all the needed data
				if (mInputsList[i].timeStamp > results.timeStamp) {
					foundIndex = i;
					break;
				}
			}

			// No needed record found, clear all the cached record
			if (foundIndex == -1) {
				mInputsList.Clear ();
				return;
			}

			// Replay recorded inputs
			for (int i = foundIndex; i < mInputsList.Count; i ++) {
				mResults.reloading = Reload (mInputsList[i], mResults);
				mResults.shooting = Shoot (mInputsList[i], mResults);
			}

			// Remove all inputs before time stamp
			int targetCount = mInputsList.Count - foundIndex;
			while (mInputsList.Count > targetCount) {
				mInputsList.RemoveAt (0);
			}
		}
	}
	#endregion

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Server only function 
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	[ServerCallback]
	void UpdateServerTime ()
	{
		// TODO: server timestamp must be obtain from network manager
		serverTimeStamp = Time.time;
	}
}
