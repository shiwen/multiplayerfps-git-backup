﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: NetworkMessageEventSynchronizer.cs
/// FUNCTION		: - A class that will carry Unity built-in network player connection authority.
/// 				  - Inherit this class to create a more specific class.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public abstract class NetworkPlayerConnectionAuthority : NetworkBehaviour
{
	[SyncVar]
	public byte connectionIndex;
	public bool isLocal;

	public override void OnStartLocalPlayer ()
	{
		connectionIndex = NetworkConnectionManager.instance.GetLocalPlayer().identifier;
		CmdAssignConnectionIndex(NetworkConnectionManager.instance.GetLocalPlayer().identifier);
		CmdRegisterConnectionAuthority ();
		CmdAuthorityAssignForAllPlayer ();
		isLocal = true;
	}

	[Command]
	void CmdRegisterConnectionAuthority()
	{
		NetworkConnectionManager.instance.AssignNetworkPlayerConnectionAuthority (connectionIndex, GetComponent<NetworkPlayerConnectionAuthority>());
		RpcRegisterConnectionAuthority (connectionIndex);
	}

	[Command]
	void CmdAssignConnectionIndex(byte _identifier)
	{
		connectionIndex = _identifier;
	}

	[Command]
	void CmdAuthorityAssignForAllPlayer()
	{
		foreach (NetworkPlayerInstance _candidate in NetworkConnectionManager.instance.GetAllNetworkPlayerInstance()) //appropriate to put this here?
		{
			if(_candidate.networkPlayerConnectionAuthority != null)
				_candidate.networkPlayerConnectionAuthority.RpcRegisterConnectionAuthority (_candidate.identifier);
		}
	}

	[ClientRpc]
	protected void RpcRegisterConnectionAuthority(byte _identifier)
	{
		NetworkConnectionManager.instance.AssignNetworkPlayerConnectionAuthority (_identifier, this);
	}
}
