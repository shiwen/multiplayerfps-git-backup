﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: DiscoveryServer.cs
/// FUNCTION		: - Sub module of NetworkConnectionManager.
/// 				  - Contains function to broadcast infomation of room after player created a room.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using System;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class BroadcastData
{
	public int version = 1;
	public string peerId;
	public bool isStarted;
	public int numPlayers;
	public int maxPlayers;
	public string roomName;
	public int gameMode;

	public override string ToString()
	{
		return String.Format("{0}:{1}:{2}:{3}:{4}:{5}:{6}:", version, peerId, isStarted ? 1 : 0, numPlayers, maxPlayers, roomName, gameMode);
	}

	public string GetRoomString()
	{
		return String.Format ("{0} ({1}/{2})", roomName, numPlayers, maxPlayers);
	}

	public void FromString(string aString)
	{
		var items = aString.Split(':');
		version = Convert.ToInt32(items[0]);
		peerId = items[1];
		isStarted = (Convert.ToInt32(items[2]) != 0);
		numPlayers = Convert.ToInt32(items[3]);
		maxPlayers = Convert.ToInt32 (items [4]);
		roomName = items [5];
		gameMode = Convert.ToInt32 (items [6]);
	}
}

public class DiscoveryServer : NetworkDiscovery
{
	public NetworkConnectionManager networkConnectionManager;
	public BroadcastData broadcastDataObject;
	public bool isStarted	{ get { return broadcastDataObject.isStarted; } set { broadcastDataObject.isStarted = value; } }
	public int numPlayers	{ get { return broadcastDataObject.numPlayers; } set { broadcastDataObject.numPlayers = value; } }

	void Start()
	{
		showGUI = false;
		useNetworkManager = false;
	}

	public void Setup(NetworkConnectionManager _networkConnectionManager)
	{
		networkConnectionManager = _networkConnectionManager;
		broadcastKey = networkConnectionManager.broadcastIdentifier.GetHashCode(); // Make sure broadcastKey matches client
		isStarted = false;
		numPlayers = 0;

		broadcastDataObject = new BroadcastData();
		UpdateBroadcastData();
	}

	public void UpdateBroadcastData()
	{
		broadcastData = broadcastDataObject.ToString();
	}

	public void Reset()
	{
		isStarted = false;
		numPlayers = 0;
		UpdateBroadcastData();
	}

	public void RestartBroadcast()
	{
		if (running)
		{
			StopBroadcast();
		}

		// Delay briefly to let things settle down
		CancelInvoke("RestartBroadcastInternal");
		Invoke("RestartBroadcastInternal", 0.5f);
	}

	private void RestartBroadcastInternal()
	{
		Debug.Log ("Start broadcasting room data");
		
		UpdateBroadcastData();

		if (!Initialize()) {
			Debug.LogError("Network port is unavailable!");
		}
		if (!StartAsServer()) {
			Debug.LogError("Unable to broadcast!");
		}
	}
}
