﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;

public struct ShotInfo 
{
	public byte ID;
	public byte numOfBullet;
	public Vector3 startPosition;
	public Vector3 startDirection;
}

public struct WeaponInfo
{
	public byte key;
	public byte weaponId;
}

public class NetworkWeapon : NetworkBehaviour {

	private byte shotID = 0;
	private bool hasTriggeredFire = false;
	private bool hasTriggeredReload = false;
	private bool hasTriggeredChangeWeapon = false;
	private bool hasTriggeredGrenadeDown = false;
	private bool hasTriggeredGrenadeUp = false;

	private bool isRunning = false;

	// Store shot history for future verification
	private int maxShotHistory = 30;
	private List<ShotInfo> shotInfoHistory = new List<ShotInfo> ();

	// Get shooting and animation data
	private PlayerManager playerManager = null;
	private PlayerAnimationManager playerAnimation = null;

	// Weapon change info
	private WeaponInfo weaponInfo;

	void Start () 
	{
		playerAnimation = GetComponent<PlayerAnimationManager> ();
		playerManager = GetComponent<PlayerManager> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
//		if (isLocalPlayer) {
//			hasTriggeredReload = Input.GetKeyUp (KeyCode.R);
//			hasTriggeredFire = Input.GetButton ("Fire1");
//			hasTriggeredGrenade = Input.GetKeyUp (KeyCode.G);
//
//			hasTriggeredChangeWeapon = GetWeaponChangeInfo (ref weaponInfo);
//		}
	}

	void UpdateMovementAnimation (object _o)
	{
		NetworkMovement.Inputs inputs = (NetworkMovement.Inputs)_o;

		isRunning = inputs.sprint;
	}

	void FixedUpdate ()
	{
		if (isLocalPlayer) {

			hasTriggeredReload = Input.GetKeyUp (KeyCode.R);
			hasTriggeredFire = Input.GetButton ("Fire1");
			hasTriggeredGrenadeDown = Input.GetKeyDown (KeyCode.G);
			hasTriggeredGrenadeUp = Input.GetKeyUp (KeyCode.G);

			hasTriggeredChangeWeapon = GetWeaponChangeInfo (ref weaponInfo);


			if (hasTriggeredFire || hasTriggeredReload || hasTriggeredChangeWeapon || 
				hasTriggeredGrenadeDown || hasTriggeredGrenadeUp) {

				if (hasTriggeredFire && playerManager.GetCanFire () && !isRunning) {
					List<GameObject> bullet = playerManager.SpawnBullet ();
					if (bullet == null)
						return;

					if (bullet.Count <= 0)
						return;

					if (hasAuthority) {
						// Server trigger shot
						UpdateShotHistory (shotID, bullet);
						Rpc_shoot ();
					} else {
						// Client trigger shot
						Cmd_shoot (shotID);
					}

					playerManager.FireBullet (shotID, hasAuthority, bullet);
					playerAnimation.Shoot ();

					shotID ++;

					// Byte only store integer up to 255
					if (shotID == 255) {
						shotID = 0;
					}
				} else if (((hasTriggeredReload || hasTriggeredFire) && !isRunning) 
					&& playerManager.GetCanReload ()) {

					if (hasAuthority) {
						Rpc_reload ();
					} else {
						Cmd_reload ();
					}

					playerManager.ReloadBullet (hasAuthority);
					playerAnimation.Reload ();
				} else if (hasTriggeredChangeWeapon) {
					if (hasAuthority) {
						Rpc_weaponChange (weaponInfo.key);
					} else {
						Cmd_weaponChange (weaponInfo.key);
					}

					playerManager.SwitchWeapon (weaponInfo.key);
				} else if (hasTriggeredGrenadeDown || hasTriggeredGrenadeUp) {
					if (hasAuthority) {
						Rpc_fireGrenade (hasTriggeredGrenadeDown, hasTriggeredGrenadeUp);
					} else {
						Cmd_fireGrenade (hasTriggeredGrenadeDown, hasTriggeredGrenadeUp);
					}

					GameObject grenade = playerManager.SpawnGrenade();
					playerManager.FireGrenade (hasAuthority, grenade, hasTriggeredGrenadeUp);
				}
			}
		}
	}

	void UpdateShotHistory (byte _id, List<GameObject> _bullet)
	{
		ShotInfo info = new ShotInfo ();

		for (int i = 0; i < _bullet.Count; i ++) {
			
			info.ID = _id;
			info.startPosition = _bullet[i].transform.position;
			info.startDirection = _bullet[i].transform.forward;
			shotInfoHistory.Add (info);

			if (shotInfoHistory.Count > maxShotHistory) {
				shotInfoHistory.RemoveAt (0);
			}
		}
	}

	bool GetWeaponChangeInfo (ref WeaponInfo _info)
	{
		if (Input.GetKeyUp (KeyCode.Alpha1)) {
			Debug.Log ("pressed 1");
			_info.key = 1;
			return true;
		} else if (Input.GetKeyUp (KeyCode.Alpha2)) {
			Debug.Log ("pressed 2");
			_info.key = 2;
			return true;
		} else if (Input.GetKeyUp (KeyCode.Alpha3)) {
			Debug.Log ("pressed 3");
			_info.key = 3;
			return true;
		} else if (Input.GetKeyUp (KeyCode.Alpha4)) {
			Debug.Log ("pressed 4");
			_info.key = 4;
			return true;
		}
			

		return false;
	}

	[ClientRpc]
	private void Rpc_shoot ()
	{
		if (!isLocalPlayer) {
			List<GameObject> bullet = playerManager.SpawnBullet ();
			if (bullet == null)
				return;

			if (bullet.Count <= 0)
				return;
			
			playerManager.FireBullet (shotID, false, bullet);
		}
	}

	[ClientRpc]
	private void Rpc_reload ()
	{
		if (!isLocalPlayer) {
			playerManager.ReloadBullet (false);
		}
	}

	[ClientRpc]
	private void Rpc_weaponChange (byte _inputIndex)
	{
		if (!isLocalPlayer) {
			
		}
	}

	[ClientRpc]
	private void Rpc_fireGrenade (bool _hold, bool _release)
	{
		if (!isLocalPlayer) {
			GameObject grenade = playerManager.SpawnGrenade();
			playerManager.FireGrenade (hasAuthority, grenade, _release);
		}
	}

	[Command]
	private void Cmd_shoot (byte _shotID)
	{
		if (hasAuthority && !isLocalPlayer)	{
			List<GameObject> bullet = playerManager.SpawnBullet ();
			if (bullet == null)
				return;

			if (bullet.Count <= 0)
				return;
			
			playerManager.FireBullet (_shotID, true, bullet);
			UpdateShotHistory (shotID, bullet);
			Rpc_shoot ();
		}
	}

	[Command]
	private void Cmd_reload ()
	{
		if (hasAuthority && !isLocalPlayer)	{
			playerManager.ReloadBullet (true);
			Rpc_reload ();
		}
	}

	[Command]
	private void Cmd_weaponChange (byte _inputIndex)
	{
		if (hasAuthority && !isLocalPlayer)	{
			playerManager.SwitchWeapon ((int)_inputIndex);
			Rpc_weaponChange (_inputIndex);
		}
	}

	[Command]
	private void Cmd_fireGrenade (bool _hold, bool _release)
	{
		if (hasAuthority && !isLocalPlayer) {
			GameObject grenade = playerManager.SpawnGrenade();
			playerManager.FireGrenade (true, grenade, _release);
			Rpc_fireGrenade (_hold, _release);
		}
	}
}
