﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public enum MultiPlayerFPSCustomNetworkEvent
{
	StartCountdown,
	CancelCountdown,
	PlayerDeath,
	ChangeGameplayState,
	TriggerEndGame
}

public class MultiPlayerFPSNetworkConnectionManager :NetworkConnectionManager 
{
	//properties
	private bool directChangeSceneWhileJoinStartedGame;
	private bool readyIsRequired;

	public override void Initialize (NetworkConnectionEventReactor _connectionReactor)
	{
		base.Initialize (_connectionReactor);
	}

	public static MultiPlayerFPSNetworkConnectionManager GetInstance()
	{
		return (MultiPlayerFPSNetworkConnectionManager)instance;
	}

	#region basic function

	public GameObject SpawnObject(GameObject _objectPrefab)
	{
		GameObject _objectHolder = GameObject.Instantiate (_objectPrefab);
		NetworkServer.Spawn (_objectHolder);

		return _objectHolder;
	}

	#endregion

	#region info inquiry

	public int GetUnusedRoomSlot()
	{
		List<MultiPlayerFpsNetworkPlayerInstance> _networkPlayers = GetAllMultiplayerFpsNetworkPlayer ();

		for (int i = 0; i < _networkPlayers.Count; i++) 
		{
			bool _slotOccupied = false;

			foreach (MultiPlayerFpsNetworkPlayerInstance _candidate in _networkPlayers) 
			{
				if (_candidate.roomSlot == i) 
				{
					_slotOccupied = true;
					break;
				}
			}

			if (!_slotOccupied)
				return i;
		}

		Debug.LogError ("slot identify failed");
		return -1;
	}

	#endregion

	#region permanent parameters

	public void SetupSelfPlayerName(string _playerName)
	{
		SetupSelfPermanentParameter ("playerName", _playerName);
	}

	#endregion

	#region accessors

	public MultiPlayerFpsNetworkPlayerInstance GetMultiplayerFpsNetworkPlayerByIdentifier(byte _identifier)
	{
		return (MultiPlayerFpsNetworkPlayerInstance)GetPlayerByIdentifier (_identifier);
	}

	public MultiPlayerFpsNetworkPlayerInstance GetLocalMultiplayerFpsNetworkPlayer()
	{
		return (MultiPlayerFpsNetworkPlayerInstance)GetLocalPlayer ();
	}

	public List<MultiPlayerFpsNetworkPlayerInstance> GetAllMultiplayerFpsNetworkPlayer()
	{
		List<MultiPlayerFpsNetworkPlayerInstance> _resultHolder = new List<MultiPlayerFpsNetworkPlayerInstance>();

		foreach (NetworkPlayerInstance _candidate in GetAllNetworkPlayerInstance ()) 
		{
			_resultHolder.Add ((MultiPlayerFpsNetworkPlayerInstance)_candidate);
		}

		return _resultHolder;
	}

	public int NumReadyPlayers()
	{
		int readyCount = 0;
		foreach (MultiPlayerFpsNetworkPlayerInstance _candidate in GetAllMultiplayerFpsNetworkPlayer())
		{
			if (_candidate.playerStatus == NetworkPlayerInstanceStatus.Ready) 
			{
				readyCount += 1;
			}
		}
		return readyCount;
	}

	#endregion

	#region sync parameter

	public void ServerChangePlayerStatus(byte _identifier, NetworkPlayerInstanceStatus _targetedStatus)
	{
		if (startAsHost) 
		{
			GetMultiplayerFpsNetworkPlayerByIdentifier (_identifier).playerStatus = _targetedStatus;
			ServerUpdateParticularRoomPlayerInfo (_identifier, "playerStatus");
		} 
	}

	public void ClientChangeSelfStatus(NetworkPlayerInstanceStatus _targetedStatus)
	{
		if (!startAsHost) 
		{
			ClientRequestUpdateSelf ("playerStatus", _targetedStatus);
		}
	}

	#endregion

	#region custom event

	public void ServerTriggerStartCountdown()
	{
		SeverTriggerCustomEvent ((int)MultiPlayerFPSCustomNetworkEvent.StartCountdown, true);
	}

	public void ServerTriggerCancelCountdown()
	{
		SeverTriggerCustomEvent ((int)MultiPlayerFPSCustomNetworkEvent.CancelCountdown, true);
	}

	public void ServerTriggerChangeGameplayState(int _gameStateInt)
	{
		NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
		_customMessage.PutInt ("state", _gameStateInt);
		SeverTriggerCustomEvent ((int)MultiPlayerFPSCustomNetworkEvent.ChangeGameplayState, _customMessage, false);
	}

	public void ServerTriggerPlayerDeath(byte _killerIdentifier, byte _dierIdentifier, int _weaponTypeInt, bool _isHeadShoot)
	{
		NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
		_customMessage.PutInt ("killer", _killerIdentifier);
		_customMessage.PutInt ("dier", _dierIdentifier);
		_customMessage.PutInt ("weapon", _weaponTypeInt);
		_customMessage.PutBool ("headShoot", _isHeadShoot);
		
		SeverTriggerCustomEvent ((int)MultiPlayerFPSCustomNetworkEvent.PlayerDeath, _customMessage, false);
	}

	public void ServerTriggerPlayerDeathByGrenade(byte _killerIdentifier, byte _dierIdentifier)
	{
		NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
		_customMessage.PutInt ("killer", _killerIdentifier);
		_customMessage.PutInt ("dier", _dierIdentifier);

		SeverTriggerCustomEvent ((int)MultiPlayerFPSCustomNetworkEvent.PlayerDeath, _customMessage, false);
	}

	public void ServerTriggerEndGame()
	{
		SeverTriggerCustomEvent ((int)MultiPlayerFPSCustomNetworkEvent.TriggerEndGame, new NetworkCustomMessage (), false);
	}

	#endregion
}
