﻿using UnityEngine;
using System.Collections;

public class MultiPlayerFpsNetworkPlayerInstance : NetworkPlayerInstance 
{
	#region parameters access
	
	public string playerName
	{
		get {return (string)permanentParemeters [0].parameterValue;}
		set {permanentParemeters [0].parameterValue = value;}
	}

	public NetworkPlayerInstanceStatus playerStatus
	{
		get {return (NetworkPlayerInstanceStatus)(int)frequentChangeParameters [0].parameterValue;}
		set {frequentChangeParameters [0].parameterValue = value;}
	}

	public int roomSlot
	{
		get {return (int)frequentChangeParameters [1].parameterValue;}
		set {frequentChangeParameters [1].parameterValue = value;}
	}

	public int teamIndex
	{
		get {return (int)frequentChangeParameters [2].parameterValue;}
		set {frequentChangeParameters [2].parameterValue = value;}
	}

	#endregion

	protected override void ImplementParameters ()
	{
		permanentParemeters = new NetworkPlayerInstanceSyncParameter[1];
		permanentParemeters [0] = new NetworkPlayerInstanceSyncParameter ("playerName", NetworkPlayerInstanceSyncParameterType.String, "Default Name");
		
		frequentChangeParameters = new NetworkPlayerInstanceSyncParameter[3];
		frequentChangeParameters [0] = new NetworkPlayerInstanceSyncParameter ("playerStatus", NetworkPlayerInstanceSyncParameterType.Enum, 0);
		frequentChangeParameters [1] = new NetworkPlayerInstanceSyncParameter ("roomSlot", NetworkPlayerInstanceSyncParameterType.Integer, 0);
		frequentChangeParameters [2] = new NetworkPlayerInstanceSyncParameter ("teamIndex", NetworkPlayerInstanceSyncParameterType.Integer, 0);
	}
}
