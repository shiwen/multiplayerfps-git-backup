﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameplayConnectionReactor : NetworkConnectionEventReactor 
{
	private GameManager gameManager;
	private MultiPlayerFPSNetworkConnectionManager connectionManager;

	void Start()
	{
		gameManager = GameManager.instance;
		connectionManager = MultiPlayerFPSNetworkConnectionManager.GetInstance ();
	}
	
	public override void OnStartGameCalled()
	{
	}

	public override void OnDisconnectCalled()
	{
		SceneManager.LoadScene ("Lobby");
		GameObject.Destroy (NetworkConnectionManager.instance.gameObject);
	}

	public override void OnConnectedToRoom(bool _gameIsStarted, bool _isHost)
	{
	}

	public override void OnServerDiscovered(DiscoveredServer _discoveredServer)
	{
	}

	public override void OnServerRemoved(DiscoveredServer _removedServer)
	{
	}

	public override void OnPlayerInstanceAdded(bool _isHost, NetworkPlayerInstance _networkPlayerInstance)
	{
		if (_isHost) 
		{
			MultiPlayerFpsNetworkPlayerInstance _networkPlayer = (MultiPlayerFpsNetworkPlayerInstance)_networkPlayerInstance;

			if(connectionManager.GetAllNetworkPlayerInstance().Count != 1) // if this player is not the first player
				_networkPlayer.roomSlot = connectionManager.GetUnusedRoomSlot ();
		}
		
		gameManager.RegisterPlayer (_networkPlayerInstance.identifier, true);
	}

	public override void OnPlayerInstanceRemoved(byte _identifier)
	{
		if (connectionManager.startAsHost) 
		{
			if (gameManager.hostAsModerator) 
			{
				gameManager.uiManager.moderatorUIControl.RemovePlayerObject (_identifier);
			}
		}
		
		gameManager.UnRegisterPlayer (_identifier);
	}

	public override void ServerOnPlayerPostStartGameJoinedGame (NetworkPlayerInstance _joinedRoomPlayer)
	{
		MultiPlayerFpsNetworkPlayerInstance _networkPlayer = (MultiPlayerFpsNetworkPlayerInstance)_joinedRoomPlayer;
		_networkPlayer.playerStatus = NetworkPlayerInstanceStatus.Playing;

		if (gameManager.hostAsModerator) 
		{
			gameManager.uiManager.moderatorUIControl.AddNewPlayerObject (_networkPlayer.identifier, _networkPlayer.playerName);
		}
	}
}
