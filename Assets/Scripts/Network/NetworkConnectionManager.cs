﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: NetworkConnectionManager.cs
/// FUNCTION		: - Inherit unet network manager.
/// 				  - Handle connections and info synchronization functions.
/// 				  - can be inheritted to become a more specfic network manager.
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 16/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Patrick	| 16/05/16	|	- Created class
/// </summary> ------------------------------------------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;

public class NetworkConnectionManager : NetworkManager
{
	//components
	private NetworkConnectionEventReactor connectionReactor;
	private NetworkMessageEventSynchronizer networkMessageEventSynchronizer;
	private NetworkPlayerInstanceManager networkPlayerInstanceManager;
	private DiscoveryServer discoveryServer;
	private DiscoveryClient discoveryClient;

	//delegates
	public delegate void CustomEventListenerFunction(NetworkCustomMessage _parameter);

	//properties
	[HideInInspector]
	public static NetworkConnectionManager instance;
	[HideInInspector]
	public bool joinedLobby;
	[HideInInspector]
	public NetworkConnection connectionToServer;
	[HideInInspector]
	public bool startAsHost;
	[HideInInspector]
	public bool gameplaySceneLoaded;

	public string networkPlayerInstanceClassName = "NetworkPlayerInstance";
	public NetworkPlayerInstanceSyncParameter[] playerInstancePreJoinedRoomParameters; //parameter that needed from client before creating player instance, ex: player name
	public string broadcastIdentifier = "CM";
	public int minPlayers = 2;
	public int maxPlayers = 4;
	public string lobbySceneName;
	public string gameplaySceneName;

	private System.Type typeOfNetworkInstance;
	private Dictionary<int, List<CustomEventListenerFunction>> customEventListenerFunctions;

	//gameplay
	private int currentStartPositionIndex = 0;
		
	public virtual void Initialize (NetworkConnectionEventReactor _connectionReactor)
	{
		maxConnections = maxPlayers;
		typeOfNetworkInstance = System.Type.GetType (networkPlayerInstanceClassName);
		connectionReactor = _connectionReactor;
		discoveryServer = transform.GetComponentInChildren<DiscoveryServer> ();
		discoveryClient = transform.GetComponentInChildren<DiscoveryClient> ();
		discoveryServer.Setup(this);
		discoveryClient.Setup(this);

		if (singleton != this)
		{
			Destroy(gameObject);
		}

		instance = (NetworkConnectionManager)singleton;

		SetupSynchronizers ();

		networkPlayerInstanceManager = gameObject.AddComponent<NetworkPlayerInstanceManager> ();
		customEventListenerFunctions = new Dictionary<int, List<CustomEventListenerFunction>> ();
	}

	public void AssignReactor(NetworkConnectionEventReactor _reactor)
	{
		connectionReactor = _reactor;
	}

	void OnLevelWasLoaded(int level) 
	{
		if (SceneManager.GetSceneByName(gameplaySceneName).buildIndex == level) 
		{
			PerformActualConnection ();

			if (startAsHost)
				NetworkServer.SpawnObjects ();

			connectionReactor.OnGameplayLevelLoaded ();
		}
	}

	protected void PerformActualConnection()
	{
		base.OnClientConnect (connectionToServer);
	}

	#region info inquiry

	public bool IsConnected()
	{
		return IsClientConnected();
	}

	public bool IsHost()
	{
		NetworkIdentity networkObject = FindObjectOfType(typeof(NetworkIdentity)) as NetworkIdentity;
		return networkObject != null && networkObject.isServer;
	}


	public NetworkPlayerInstance GetPlayerByConnection(NetworkConnection _networkConnection)
	{
		foreach (NetworkPlayerInstance player in networkPlayerInstanceManager.GetAllNetworkPlayerInstance()) 
		{
			if (player.connection == _networkConnection)
			{
				return player;
			}
		}

		return null;
	}

	public bool CurrentRoomIsStarted()
	{
		if (startAsHost)
			return discoveryServer.broadcastDataObject.isStarted;
		else
			return discoveryClient.discoveredServers [networkAddress].isStarted;
	}
		
	public NetworkConnection GetConnectionByConnectionID(int _connId)
	{
		foreach (NetworkConnection _candidate in NetworkServer.connections) 
		{
			if (_candidate.connectionId == _connId)
				return _candidate;
		}

		return null;
	}
		
	public GameObject GetCurrentLocalObject()
	{
		return ClientScene.localPlayers [0].gameObject;
	}

	public Vector3 RetreiveCurrentStartPosition()
	{
		int _targetedIndex = currentStartPositionIndex;

		currentStartPositionIndex++;

		if (currentStartPositionIndex >= NetworkConnectionManager.instance.startPositions.Count)
			currentStartPositionIndex = 0;
		
		return startPositions[_targetedIndex].position;
	}

	public Dictionary<string, DiscoveredServer> GetDiscoveredServers()
	{		
		return discoveryClient.discoveredServers;
	}

	public DiscoveredServer GetFirstDiscoveredServer()
	{		
		foreach (KeyValuePair<string, DiscoveredServer> entry in discoveryClient.discoveredServers) 
		{
			return entry.Value;
		}

		return null;
	}
		
	#endregion

	#region connection related method

	public void StartHosting(string _roomName, LobbyController.GameMode _gameMode)
	{
		StopServer();
		startAsHost = true;

		CancelInvoke("StartHostingInternal");
		Invoke("StartHostingInternal", 0.5f);

		discoveryServer.broadcastDataObject.roomName = _roomName;
		discoveryServer.broadcastDataObject.maxPlayers = maxPlayers;
		discoveryServer.broadcastDataObject.gameMode = (int)_gameMode;
		discoveryServer.broadcastDataObject.numPlayers = 1;
		discoveryServer.RestartBroadcast();
	}

	private void StartHostingInternal()
	{
		if (StartHost() != null) {
			
		} else {
			Debug.LogError("#CaptainsMess# Failed to start hosting!");
		}
	}

	public void JoinServer(string _ipAddress)
	{
		StopHost();
		networkAddress = _ipAddress;
		startAsHost = false;
	
		CancelInvoke("JoinServerInternal");
		Invoke("JoinServerInternal", 0.5f);
	}

	private void JoinServerInternal()
	{
		Debug.Log ("Attempt to join server: " + networkAddress);
		StartClient();
	}

	public void Cancel()
	{
		Debug.Log ("Canceling network connection.");
		
		if (discoveryClient.hostId != -1) 
		{
			discoveryClient.StopBroadcast();
		}

		discoveryClient.Reset();

		if (discoveryServer.hostId != -1)
		{
			discoveryServer.StopBroadcast();
		}

		networkPlayerInstanceManager.ClearNetworkPlayersInstance ();

		discoveryServer.Reset();

		StopClient();
		StopServer();

		NetworkServer.Reset();
	}

	public void StopBroadcast()
	{
		if(discoveryClient.hostId != -1)
			discoveryClient.StopBroadcast ();
		
		if(discoveryServer.hostId != -1)
			discoveryServer.StopBroadcast ();
	}

	public void RestartServerBroadcast()
	{
		discoveryServer.RestartBroadcast ();
	}

	public void KickPlayer(byte _playerIdentifier)
	{
		networkPlayerInstanceManager.GetPlayerByIdentifier (_playerIdentifier).connection.Disconnect ();
	}

	public void StopRetrieveRoomList()
	{
		if (discoveryClient.hostId != -1) 
		{
			discoveryClient.StopBroadcast();
		}

		discoveryClient.CancelInvoke ();
	}

	public void SetBroadcastRoomStatusToStarted()
	{
		discoveryServer.isStarted = true;
		RestartServerBroadcast ();
	}

	private void StartGame()
	{
		Debug.Log ("Executing start game.");
		StopRetrieveRoomList ();
		connectionReactor.OnStartGameCalled();
	}

	public void ServerStartGame()
	{
		Debug.Log ("On server side: start game request being called");
		connectionReactor.OnPreStartGame ();
		networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.StartGame, new NetworkCustomMessage (), GetAllNetworkConnectionOfPlayersExcludeSelf ());
		SyncAllPlayersFrequentChangeStatus ();
		SetBroadcastRoomStatusToStarted ();
		StartGame ();
	}

	public void RequestPostStartGameJoinGame()
	{
		networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.PostStartGameJoinGame, new NetworkCustomMessage (), connectionToServer);
	}
		
	#endregion

	#region override

	public override void OnStartServer() //called when a server is started, only called on host side
	{

	}

	public override void OnServerConnect(NetworkConnection conn) //called each time when any client (include host himself) has connected to the server, only called on host side
	{
		Debug.Log ("On server: detected someone connected to server");
		
		networkMessageEventSynchronizer.RegisterListener (conn, true);

		if (networkPlayerInstanceManager.GetNetworkPlayersCount() >= maxPlayers)
		{
			conn.Disconnect();
			return;
		}
			
		base.OnServerConnect(conn);
	}

	public override void OnClientConnect(NetworkConnection conn) //called when self success connect to server as client, applicable on host and client side 
	{
		Debug.Log ("Sucessfully connected to server");
		
		connectionToServer = conn;
		
		// Stop listening for other servers
		if (discoveryClient.running) {
			discoveryClient.StopBroadcast();
		}

		// Stop broadcasting as a server
		if (discoveryServer.running) {
			discoveryServer.StopBroadcast();
		}

		if (startAsHost) 
		{
			ServerAddNewNetworkPlayerInstance (playerInstancePreJoinedRoomParameters, connectionToServer, true);
			connectionReactor.OnConnectedToRoom (false, startAsHost);
		}
		else
		{
			NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
			_customMessage.PutNetworkPlayerInstanceSyncParameters ("pParam", playerInstancePreJoinedRoomParameters);
			networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.JoinedRoom, _customMessage, connectionToServer);
			connectionReactor.OnConnectedToRoom (false, startAsHost);
		}

		joinedLobby = true;

		networkMessageEventSynchronizer.RegisterListener (conn, false);
	}

	public override void OnServerDisconnect(NetworkConnection conn) //called each time when any client except host himself, has disconnected from the server, only called on host side
	{
		Debug.Log ("On server: detected someone disconnected from server");
		
		base.OnServerDisconnect(conn);
		ServerRemovePlayerByConnection (conn);
		discoveryServer.numPlayers = networkPlayerInstanceManager.GetNetworkPlayersCount();
		discoveryServer.RestartBroadcast();
		//Cancel();
	}

	public override void OnServerRemovePlayer(NetworkConnection conn, UnityEngine.Networking.PlayerController player)
	{
		base.OnServerRemovePlayer(conn, player);
	}

	public override void OnClientDisconnect(NetworkConnection conn) 
	{
		Debug.Log ("Disconnected from server");
		
		base.OnClientDisconnect(conn);
		connectionReactor.OnDisconnectCalled ();
	}

	public override void OnStopClient() //called when self disconnected from server as client, applicable on host and client side
	{
		if (joinedLobby)
		{

		}
		else
		{

		}

		joinedLobby = false;
	}
		
	#endregion

	#region self permanent parameters

	protected void SetupSelfPermanentParameter(string _key, object _value)
	{
		foreach (NetworkPlayerInstanceSyncParameter _candidate in playerInstancePreJoinedRoomParameters) 
		{
			if (_candidate.GetParameterKey () == _key) 
			{
				_candidate.parameterValue = _value;
				return;
			}
		}

		Debug.LogError ("self permanent parameter key not found!");
	}

	#endregion

	#region components related method

	#region network player instance

	public void ServerAddNewNetworkPlayerInstance(NetworkPlayerInstanceSyncParameter[] _permanentParameters, NetworkConnection _conn, bool _isSelf)
	{
		NetworkPlayerInstance _networkPlayerInstance = networkPlayerInstanceManager.ServerAddNewPlayerInstance (typeOfNetworkInstance, _permanentParameters, _conn, _isSelf);

		if (networkPlayerInstanceManager.GetNetworkPlayersCount() >= maxPlayers)
		{
			discoveryServer.StopBroadcast();
		}
		else
		{
			// Update player count for broadcast
			discoveryServer.numPlayers = networkPlayerInstanceManager.GetNetworkPlayersCount();
			discoveryServer.RestartBroadcast();
		}

		connectionReactor.OnPlayerInstanceAdded (startAsHost, _networkPlayerInstance);
	}

	public void ServerRemovePlayerByConnection(NetworkConnection _conn)
	{
		byte _removedPlayerIdentifier = networkPlayerInstanceManager.RemovePlayerInstanceByConnection (_conn);
		connectionReactor.OnPlayerInstanceRemoved (_removedPlayerIdentifier);
		NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
		_customMessage.PutInt ("id", _removedPlayerIdentifier);
		networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.PlayerQuited, _customMessage, GetAllNetworkConnectionOfPlayersExcludeSelf ());
	}

	#endregion

	#region region network message event synchronizer

	public void SetupSynchronizers()
	{
		networkMessageEventSynchronizer = gameObject.AddComponent<NetworkMessageEventSynchronizer> ();
		networkMessageEventSynchronizer.Setup (startAsHost);

		networkMessageEventSynchronizer.AddListenerFunction(CustomNetworkMessageEvent.JoinedRoom, true, OnServerReceiveJoinedRoom);
		networkMessageEventSynchronizer.AddListenerFunction(CustomNetworkMessageEvent.JoinedRoom, false, OnClientReceiveJoinedRoom);
		networkMessageEventSynchronizer.AddListenerFunction(CustomNetworkMessageEvent.UpdateRoomPlayerInfos, false, OnClientReceiveUpdateRoomPlayerInfo);
		networkMessageEventSynchronizer.AddListenerFunction(CustomNetworkMessageEvent.UpdateParticularRoomPlayerInfo, true, OnServerReceiveUpdateParticularRoomPlayerInfo);
		networkMessageEventSynchronizer.AddListenerFunction(CustomNetworkMessageEvent.UpdateParticularRoomPlayerInfo, false, OnClientReceiveUpdateParticularRoomPlayerInfo);
		networkMessageEventSynchronizer.AddListenerFunction(CustomNetworkMessageEvent.PlayerQuited, false, OnClientReceivePlayerQuited);
		networkMessageEventSynchronizer.AddListenerFunction(CustomNetworkMessageEvent.StartGame, false, OnClientReceiveStartGameRequest);
		networkMessageEventSynchronizer.AddListenerFunction (CustomNetworkMessageEvent.PostStartGameJoinGame, true, OnServerReceivePostStartGameJoinGame);
		networkMessageEventSynchronizer.AddListenerFunction (CustomNetworkMessageEvent.CustomEvent, false, OnClientReceiveTriggerCustomEvent);
	}

	private void OnServerReceiveJoinedRoom(NetworkCustomMessage _parameter)
	{
		ServerAddNewNetworkPlayerInstance (_parameter.GetNetworkPlayerInstanceSyncParameters("pParam"), _parameter.senderConnnection, false);

		List<int> _identifierList = new List<int> ();
		List<NetworkPlayerInstanceSyncParameter[]> _permanentParametersList = new List<NetworkPlayerInstanceSyncParameter[]> ();
		List<NetworkPlayerInstanceSyncParameter[]> _frequentChangeParametersList = new List<NetworkPlayerInstanceSyncParameter[]> ();

		foreach (NetworkPlayerInstance _candidate in networkPlayerInstanceManager.GetAllNetworkPlayerInstance()) 
		{
			if (_candidate != null) 
			{
				_identifierList.Add (_candidate.identifier);
				_permanentParametersList.Add (_candidate.permanentParemeters);
				_frequentChangeParametersList.Add (_candidate.frequentChangeParameters);
			}
		}

		NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
		_customMessage.PutIntArray ("id", _identifierList.ToArray ());
		_customMessage.PutNetworkPlayerInstanceSyncParametersArray ("pParam", _permanentParametersList.ToArray());
		_customMessage.PutNetworkPlayerInstanceSyncParametersArray ("fParam", _frequentChangeParametersList.ToArray());

		networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.JoinedRoom, _customMessage, GetAllNetworkConnectionOfPlayersExcludeSelfAndSpecificConnection(_parameter.senderConnnection));
		_customMessage.PutInt ("ownId", GetPlayerByConnection (_parameter.senderConnnection).identifier);
		_customMessage.PutBool ("start", CurrentRoomIsStarted ());
		networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.JoinedRoom, _customMessage, _parameter.senderConnnection);

	}

	private void OnClientReceiveJoinedRoom(NetworkCustomMessage _parameter)
	{
		if (!startAsHost) 
		{
			int[] _idHolder = _parameter.GetIntArray ("id");
			NetworkPlayerInstanceSyncParameter[][] _permanentParameters = _parameter.GetNetworkPlayerInstanceSyncParametersArray ("pParam");
			NetworkPlayerInstanceSyncParameter[][] _frequenctChangesParameters = _parameter.GetNetworkPlayerInstanceSyncParametersArray ("fParam");

			for (int i = 0; i < _idHolder.Length; i++) 
			{
				if (GetPlayerByIdentifier ((byte)_idHolder [i]) == null) 
				{
					connectionReactor.OnPlayerInstanceAdded (startAsHost, networkPlayerInstanceManager.ClientAddNewPlayerInstance(typeOfNetworkInstance, (byte)_idHolder[i], _permanentParameters[i], _frequenctChangesParameters[i]));
				}
			}

			if (_parameter.HasKey("ownId")) 
			{
				networkPlayerInstanceManager.SetLocalPlayerIdentifier((byte)_parameter.GetInt ("ownId"));

				bool _gameIsStarted = _parameter.GetBool ("start");
				if(discoveryClient.discoveredServers.ContainsKey(networkAddress))
					discoveryClient.discoveredServers[networkAddress].isStarted = _gameIsStarted;
				connectionReactor.PostJoinedRoomSync (_gameIsStarted);
			}
		}
	}

	private void OnClientReceiveUpdateRoomPlayerInfo(NetworkCustomMessage _parameter)
	{
		if (!startAsHost) 
		{
			int[] _ids = _parameter.GetIntArray("id");
			NetworkPlayerInstanceSyncParameter[][] _parameters = _parameter.GetNetworkPlayerInstanceSyncParametersArray("param");

			for (int i = 0; i < _ids.Length; i++) 
			{
				NetworkPlayerInstance _targetedPlayer = GetPlayerByIdentifier ((byte)_ids[i]);
				_targetedPlayer.frequentChangeParameters = _parameters[i];
			}
		}
	}

	//-------------------------------------------------- Particular room player info---------------------------------------------------

	protected void ServerRequestUpdateParticularRoomPlayerInfo(byte _targetedPlayerIdentifier, string _key, object value)
	{
		NetworkPlayerInstanceSyncParameter _targetedParameter = GetPlayerByIdentifier (_targetedPlayerIdentifier).GetFrequentUpdateParamaterByKey (_key);
		_targetedParameter.parameterValue = value;
		ServerUpdateParticularRoomPlayerInfo (_targetedPlayerIdentifier, _targetedParameter);
	}

	protected void ServerUpdateParticularRoomPlayerInfo(byte _id, string _key)
	{
		ServerUpdateParticularRoomPlayerInfo (new byte[1]{ _id }, new string[1]{ _key });
	}

	protected void ServerUpdateParticularRoomPlayerInfo(byte[] _ids, string[] _keys)
	{
		if (startAsHost) 
		{
			NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
			_customMessage.PutIntArray ("id", ByteArrayToIntArray(_ids));

			NetworkPlayerInstanceSyncParameter[] _params = new NetworkPlayerInstanceSyncParameter[_ids.Length];

			for (int i = 0; i < _ids.Length; i++) 
			{
				_params [i] = networkPlayerInstanceManager.GetPlayerByIdentifier (_ids [i]).GetFrequentUpdateParamaterByKey (_keys [i]);
			}

			_customMessage.PutNetworkPlayerInstanceSyncParameters ("param", _params);
			networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.UpdateParticularRoomPlayerInfo, _customMessage, networkPlayerInstanceManager.GetAllNetworkConnectionOfPlayersExcludeSelf ());
		}	
	}

	private void ServerUpdateParticularRoomPlayerInfo(byte _id, NetworkPlayerInstanceSyncParameter _param)
	{
		if (startAsHost) 
		{
			NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
			_customMessage.PutIntArray ("id", new int[1]{(int)_id});
			_customMessage.PutNetworkPlayerInstanceSyncParameters ("param", new NetworkPlayerInstanceSyncParameter[1]{_param});
			networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.UpdateParticularRoomPlayerInfo, _customMessage, networkPlayerInstanceManager.GetAllNetworkConnectionOfPlayersExcludeSelf ());
		}	
	}

	private void SyncPlayerFrequentChangeStatus(byte _identifier)
	{
		NetworkPlayerInstance _targetedPlayer = GetPlayerByIdentifier (_identifier);
		
		NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
		_customMessage.PutIntArray ("id", new int[1]{(int)_identifier});
		_customMessage.PutNetworkPlayerInstanceSyncParametersArray ("param", new NetworkPlayerInstanceSyncParameter[1][]{_targetedPlayer.frequentChangeParameters});

		networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.UpdateRoomPlayerInfos, _customMessage, GetAllNetworkConnectionOfPlayersExcludeSelf ());
	}

	private void SyncAllPlayersFrequentChangeStatus()
	{
		List<NetworkPlayerInstance> _networkPlayers = networkPlayerInstanceManager.GetAllNetworkPlayerInstance ();

		int[] _ids = new int[_networkPlayers.Count];
		NetworkPlayerInstanceSyncParameter[][] _parameters = new NetworkPlayerInstanceSyncParameter[_networkPlayers.Count][];

		for(int i =  0; i < _networkPlayers.Count; i++) 
		{
			_ids [i] = _networkPlayers [i].identifier;
			_parameters [i] = _networkPlayers [i].frequentChangeParameters;
		}

		NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
		_customMessage.PutIntArray ("id", _ids);
		_customMessage.PutNetworkPlayerInstanceSyncParametersArray ("param", _parameters);

		networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.UpdateRoomPlayerInfos, _customMessage, GetAllNetworkConnectionOfPlayersExcludeSelf ());
	}

	protected void ClientRequestUpdateSelf(string _key, object _value)
	{
		if (!startAsHost) 
		{
			NetworkPlayerInstance _localPlayer = networkPlayerInstanceManager.GetLocalPlayer ();
			NetworkPlayerInstanceSyncParameter _param = _localPlayer.GetFrequentUpdateParamaterByKey (_key);
			_param.parameterValue = _value;

			NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
			_customMessage.PutInt ("id", _localPlayer.identifier);
			_customMessage.PutNetworkPlayerInstanceSyncParameter ("param", _param);
			networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.UpdateParticularRoomPlayerInfo, _customMessage, connectionToServer);
		}
	}

	private void OnClientReceiveUpdateParticularRoomPlayerInfo(NetworkCustomMessage _parameter)
	{
		if (!startAsHost) 
		{
			int[] _ids = _parameter.GetIntArray("id");
			NetworkPlayerInstanceSyncParameter[] _particularParameters = _parameter.GetNetworkPlayerInstanceSyncParameters("param");

			for (int i = 0; i < _ids.Length; i++) 
			{
				NetworkPlayerInstance _targetedPlayer = GetPlayerByIdentifier ((byte)_ids[i]);
				_targetedPlayer.UpdateFrequentChangeParameterValue (_particularParameters[i]);
			}
		}
	}

	private void OnServerReceiveUpdateParticularRoomPlayerInfo(NetworkCustomMessage _parameter)
	{
		int _id = _parameter.GetInt("id");
		NetworkPlayerInstanceSyncParameter _particularParameter = _parameter.GetNetworkPlayerInstanceSyncParameter("param");

		NetworkPlayerInstance _targetedPlayer = GetPlayerByIdentifier ((byte)_id);
		_targetedPlayer.UpdateFrequentChangeParameterValue (_particularParameter);

		NetworkCustomMessage _customMessage = new NetworkCustomMessage ();
		_customMessage.PutInt ("id", _id);
		_customMessage.PutNetworkPlayerInstanceSyncParameter ("param", _particularParameter);

		networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.UpdateParticularRoomPlayerInfo, _customMessage, networkPlayerInstanceManager.GetAllNetworkConnectionOfPlayersExcludeSelf ());
	}

	//-------------------------------------------------- Player quits ---------------------------------------------------

	private void OnClientReceivePlayerQuited(NetworkCustomMessage _parameter)
	{
		byte _playerIdentifierToBeRemoved = (byte)_parameter.GetInt ("id");
		networkPlayerInstanceManager.RemovePlayerInstanceByIdentifier(_playerIdentifierToBeRemoved);
		connectionReactor.OnPlayerInstanceRemoved (_playerIdentifierToBeRemoved);
	}

	//-------------------------------------------------- Start game ---------------------------------------------------

	private void OnClientReceiveStartGameRequest(NetworkCustomMessage _parameter)
	{
		StartGame ();
	}

	private void OnServerReceivePostStartGameJoinGame(NetworkCustomMessage _parameter)
	{
		networkMessageEventSynchronizer.TriggerEventAsCaller (CustomNetworkMessageEvent.StartGame, new NetworkCustomMessage(), _parameter.senderConnnection);

		NetworkPlayerInstance _targetPlayer = GetPlayerByConnection (_parameter.senderConnnection);
		NetworkCustomMessage _customMessage = new NetworkCustomMessage ();

		connectionReactor.ServerOnPlayerPostStartGameJoinedGame (_targetPlayer);
		SyncPlayerFrequentChangeStatus (_targetPlayer.identifier);
	}

	//-------------------------------------------------- Custom event ---------------------------------------------------

	protected void SeverTriggerCustomEvent(int _eventIdentifier, bool _excludeSelf)
	{
		SeverTriggerCustomEvent (_eventIdentifier, new NetworkCustomMessage (), _excludeSelf);
	}

	protected void SeverTriggerCustomEvent(int _eventIdentifier, NetworkCustomMessage _parameter, bool _excludeSelf)
	{
		if (_parameter == null)
			_parameter = new NetworkCustomMessage ();

		_parameter.PutInt ("event", _eventIdentifier);
	
		TriggerMessageEventAsCaller (CustomNetworkMessageEvent.CustomEvent, _parameter, GetAllNetworkConnectionOfPlayersExcludeSelf ());

		if (!_excludeSelf)
			LocalTriggerCustomEvent (_eventIdentifier, _parameter);
	}

	public void ClientAddCustomEventListener(int _eventIdentifier, CustomEventListenerFunction _listenerFunction)
	{
		if (!customEventListenerFunctions.ContainsKey (_eventIdentifier))
			customEventListenerFunctions.Add (_eventIdentifier, new List<CustomEventListenerFunction> ());

		customEventListenerFunctions [_eventIdentifier].Add (_listenerFunction);
	}

	public void ClientClearCustomEventLister(int _eventIdentifier)
	{
		if (customEventListenerFunctions.ContainsKey (_eventIdentifier))
			customEventListenerFunctions [_eventIdentifier] = new List<CustomEventListenerFunction> ();
	}

	private void OnClientReceiveTriggerCustomEvent(NetworkCustomMessage _parameter)
	{
		int _eventIdentifier = _parameter.GetInt ("event");
		_parameter.RemoveItem ("event");
	
		if(customEventListenerFunctions.ContainsKey(_eventIdentifier))
		{
			foreach (CustomEventListenerFunction _candidate in customEventListenerFunctions [_eventIdentifier]) 
			{
				_candidate (_parameter);
			}
		}
	}

	private void LocalTriggerCustomEvent(int _eventIdentifier, NetworkCustomMessage _parameter)
	{
		_parameter.ProcessData ();

		foreach (CustomEventListenerFunction _candidate in customEventListenerFunctions [_eventIdentifier]) 
		{
			_candidate (_parameter);
		}
	}

	#endregion

	#endregion

	#region components accessors

	#region discovery client

	public void StartRetreiveRoomList()
	{
		discoveryClient.StartRetreiveRoomList ();
	}

	#endregion

	#region network message event synchronizer

	public void TriggerMessageEventAsCaller(CustomNetworkMessageEvent _event, NetworkCustomMessage _parameter, NetworkConnection _connection)
	{
		TriggerMessageEventAsCaller (_event, _parameter, new List<NetworkConnection>{ _connection });	
	}

	public void TriggerMessageEventAsCaller(CustomNetworkMessageEvent _event, NetworkCustomMessage _parameter, List<NetworkConnection> _connections)
	{
		networkMessageEventSynchronizer.TriggerEventAsCaller (_event, _parameter, _connections);
	}

	#endregion

	#region network instance manager

	public NetworkPlayerInstance GetLocalPlayer()
	{
		return networkPlayerInstanceManager.GetLocalPlayer ();
	}

	public NetworkPlayerInstance GetPlayerByIdentifier(byte _identifier)
	{
		return networkPlayerInstanceManager.GetPlayerByIdentifier (_identifier);
	}

	public List<NetworkPlayerInstance> GetAllNetworkPlayerInstance()
	{
		return networkPlayerInstanceManager.GetAllNetworkPlayerInstance();
	}

	public List<NetworkConnection> GetAllNetworkConnectionOfPlayers()
	{
		return networkPlayerInstanceManager.GetAllNetworkConnectionOfPlayers ();
	}

	public List<NetworkConnection> GetAllNetworkConnectionOfPlayersExcludeSelf()
	{
		return networkPlayerInstanceManager.GetAllNetworkConnectionOfPlayersExcludeSelf ();
	}

	public List<NetworkConnection> GetAllNetworkConnectionOfPlayersExcludeSelfAndSpecificConnection(NetworkConnection _targetedConnection)
	{
		return networkPlayerInstanceManager.GetAllNetworkConnectionOfPlayersExcludeSelfAndSpecificConnection (_targetedConnection);
	}

	public void AssignNetworkPlayerConnectionAuthority(byte _identifier, NetworkPlayerConnectionAuthority _connectionAuthority)
	{
		networkPlayerInstanceManager.AssignPlayerNetworkConnectionAuthority (_identifier, _connectionAuthority);
	}

	public NetworkPlayerConnectionAuthority GetLocalConnectionAuthority()
	{
		return GetLocalPlayer ().networkPlayerConnectionAuthority;
	}

	public NetworkPlayerConnectionAuthority GetConnectionAuthorityByIdentifier(byte _identifier)
	{
		return GetPlayerByIdentifier (_identifier).networkPlayerConnectionAuthority;
	}

	#endregion

	#endregion

	#region listener

	public void OnServerDiscovered(DiscoveredServer _discoveredServer)
	{
		connectionReactor.OnServerDiscovered (_discoveredServer);
	}

	public void OnServerRemoved(DiscoveredServer _removedServer)
	{
		connectionReactor.OnServerRemoved (_removedServer);
	}

	#endregion

	#region utility

	private int[] ByteArrayToIntArray(Byte[] _value)
	{
		int[] _result = new int[_value.Length];

		for (int i = 0; i < _value.Length; i++) 
		{
			_result [i] = (int)_value [i];
		}

		return _result;
	}

	#endregion
}