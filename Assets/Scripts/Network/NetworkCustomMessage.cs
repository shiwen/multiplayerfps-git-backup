﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: NetworkMessageEventSynchronizer.cs
/// FUNCTION		: - An intermediate class that makes MessageBase data more readable and simplier to use.
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Patrick
/// DATE			: 13/06/16
/// SCRIPT REF		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public enum NetworkCustomMessageParameterType
{
	String,
	Integer,
	Boolean,
	Float,
	NetworkPlayerInstanceParameter
}

/*
 *	Note: "=" - Between key and data.
 *		  "/" - Between arrays elements
 *		  "-" - Between	values of non-generic data types
 *        "," - Between 2nd dimension arrays elements
 * 
 * 
 */

public class NetworkCustomMessage : MessageBase 
{
	public NetworkConnection senderConnnection;
	Dictionary<string, string> processedRawData;
	private string rawData = "";

	public void ProcessData()
	{
		string[] _devidedStrings = rawData.Split (':');
		processedRawData = new Dictionary<string, string> ();

		foreach (string _candidate in _devidedStrings) 
		{
			if (_candidate.Length != 0) 
			{
				string[] _futherDevidedStrings = _candidate.Split ('=');
				processedRawData.Add (_futherDevidedStrings [0], _futherDevidedStrings [1]);
			}
		}
	}

	public override void Deserialize (NetworkReader reader)
	{
		rawData = reader.ReadString ();
	}

	public override void Serialize (NetworkWriter writer)
	{
		writer.Write (rawData);
	}
		
	public string GetRawData()
	{
		return rawData;
	}

	public bool HasKey(string _key)
	{
		return processedRawData.ContainsKey (_key);
	}

	public void RemoveItem(string _key)
	{
		processedRawData.Remove (_key);
	}

	public void PutInt(string _key, int _value)
	{
		rawData += _key + "=" + _value.ToString() + ":";
	}

	public int GetInt(string _key)
	{
		return int.Parse(processedRawData [_key]);
	}

	public void PutString(string _key, string _value)
	{
		rawData += _key + "=" + _value.ToString() + ":";
	}

	public string GetString(string _key)
	{
		return processedRawData [_key];
	}

	public void PutBool(string _key, bool _value)
	{
		string _tempString;

		if (_value)
			_tempString = "1";
		else
			_tempString = "0";
		
		rawData += _key + "=" + _tempString + ":";
	}

	public bool GetBool(string _key)
	{
		if (processedRawData [_key] == "1")
			return true;
		else
			return false;
	}

	public void PutStringArray(string _key, string[] _value)
	{
		rawData += _key + "=";

		foreach (string _candidate in _value) 
		{
			rawData += _candidate + "/";
		}

		rawData = rawData.Remove (rawData.Length - 1);
		rawData += ":";
	}

	public string[] GetStringArray(string _key)
	{
		string _rawValue = processedRawData [_key];
		string[] _splitedRawValue = _rawValue.Split ('/');

		return _splitedRawValue;
	}

	public void PutIntArray(string _key, int[] _value)
	{
		rawData += _key + "=";

		foreach (int _candidate in _value) 
		{
			rawData += _candidate.ToString() + "/";
		}

		rawData = rawData.Remove (rawData.Length - 1);
		rawData += ":";
	}

	public int[] GetIntArray(string _key)
	{
		string _rawValue = processedRawData [_key];
		string[] _splitedRawValue = _rawValue.Split ('/');
		int[] _resultHolder = new int[_splitedRawValue.Length];

		for (int i = 0; i < _splitedRawValue.Length; i++) 
		{
			_resultHolder [i] = int.Parse(_splitedRawValue [i]);
		}

		return _resultHolder;
	}

	public void PutNetworkPlayerInstanceSyncParameter(string _key, NetworkPlayerInstanceSyncParameter _parameter)
	{
		rawData += _key + "=";

		rawData += ConvertNetworkPlayerInstanceSyncParamterToString (_parameter);

		rawData += ":";
	}

	public NetworkPlayerInstanceSyncParameter GetNetworkPlayerInstanceSyncParameter(string _key)
	{
		return ConvertStringToNetworkPlayerInstanceSyncParameter(processedRawData [_key]);
	}

	public void PutNetworkPlayerInstanceSyncParameters(string _key, NetworkPlayerInstanceSyncParameter[] _parameters)
	{
		rawData += _key + "=";

		foreach (NetworkPlayerInstanceSyncParameter _candidate in _parameters) 
		{
			rawData += ConvertNetworkPlayerInstanceSyncParamterToString (_candidate);
			rawData += "/";
		}

		rawData = rawData.Remove (rawData.Length - 1);
		rawData += ":";
	}

	public NetworkPlayerInstanceSyncParameter[] GetNetworkPlayerInstanceSyncParameters(string _key)
	{
		string _rawValue = processedRawData [_key];
		string[] _splitedRawValue = _rawValue.Split ('/');
		NetworkPlayerInstanceSyncParameter[] _resultHolder = new NetworkPlayerInstanceSyncParameter[_splitedRawValue.Length];

		for (int i = 0; i < _splitedRawValue.Length; i++) 
		{
			_resultHolder [i] = ConvertStringToNetworkPlayerInstanceSyncParameter(_splitedRawValue[i]);
		}

		return _resultHolder;
	}

	public void PutNetworkPlayerInstanceSyncParametersArray(string _key, NetworkPlayerInstanceSyncParameter[][] _parameters)
	{
		rawData += _key + "=";

		foreach (NetworkPlayerInstanceSyncParameter[] _candidate in _parameters) 
		{
			foreach (NetworkPlayerInstanceSyncParameter _parameter in _candidate) 
			{
				rawData += ConvertNetworkPlayerInstanceSyncParamterToString (_parameter);
				rawData += ",";
			}
			rawData = rawData.Remove (rawData.Length - 1);
			rawData += "/";
		}
		rawData = rawData.Remove (rawData.Length - 1);

		rawData += ":";
	}
		
	public NetworkPlayerInstanceSyncParameter[][] GetNetworkPlayerInstanceSyncParametersArray(string _key)
	{
		string _rawValue = processedRawData [_key];
		string[] _splitedRawValue = _rawValue.Split ('/');
		NetworkPlayerInstanceSyncParameter[][] _resultHolder = new NetworkPlayerInstanceSyncParameter[_splitedRawValue.Length][];

		for (int i = 0; i < _splitedRawValue.Length; i++) 
		{
			string[] _futherSplitedValue = _splitedRawValue [i].Split (',');
			NetworkPlayerInstanceSyncParameter[] _parametersHolder = new NetworkPlayerInstanceSyncParameter[_futherSplitedValue.Length];

			for (int j = 0; j < _futherSplitedValue.Length; j++) 
			{
				_parametersHolder [j] = ConvertStringToNetworkPlayerInstanceSyncParameter (_futherSplitedValue[j]);
			}

			_resultHolder[i] = _parametersHolder;
		}

		return _resultHolder;
	}

	private string ConvertNetworkPlayerInstanceSyncParamterToString(NetworkPlayerInstanceSyncParameter _parameter)
	{
		string _resultHolder = "";

		_resultHolder += (_parameter.GetParameterKey () + "-");
		_resultHolder += (((int)_parameter.GetParameterType ()).ToString () + "-");

		if (_parameter.GetParameterType () == NetworkPlayerInstanceSyncParameterType.Enum)
			_resultHolder += ((int)_parameter.parameterValue).ToString();
		else
			_resultHolder += (_parameter.parameterValue.ToString ());

		return _resultHolder;
	}

	private NetworkPlayerInstanceSyncParameter ConvertStringToNetworkPlayerInstanceSyncParameter(string _rawData)
	{
		string[] _splitedRawData =	_rawData.Split ('-');
		NetworkPlayerInstanceSyncParameterType _parameterType = (NetworkPlayerInstanceSyncParameterType)(int.Parse(_splitedRawData[1]));
		object _valueHolder = null;
		NetworkPlayerInstanceSyncParameter _resultHolder;

		switch (_parameterType) 
		{
		case NetworkPlayerInstanceSyncParameterType.String:
			_valueHolder = _splitedRawData [2];
			break;

		case NetworkPlayerInstanceSyncParameterType.Integer:
		case NetworkPlayerInstanceSyncParameterType.Enum: 
			_valueHolder = int.Parse(_splitedRawData [2]);
			break;

		case NetworkPlayerInstanceSyncParameterType.Bool:
			_valueHolder = bool.Parse (_splitedRawData [2]);
			break;
		}

		_resultHolder = new NetworkPlayerInstanceSyncParameter (_splitedRawData [0], _parameterType, _valueHolder);

		return _resultHolder;
	}
}
