﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class FlashBangEffect : MonoBehaviour {

	[SerializeField]
	private RawImage captureShotImage;

	[SerializeField]
	private RawImage flashImage;

	[SerializeField]
	private Camera cam;

	private bool fade;
	private bool count;
	private bool reset;

	private Color colorForCaptureShot;
	private Color colorForFlash;

	private float timer;

	[SerializeField]
	private float recoveryRate;
	[SerializeField]
	private float freezeTime;

	void Start() {
		colorForCaptureShot = captureShotImage.color;
		colorForCaptureShot.a = 0;
		captureShotImage.color = colorForCaptureShot;

		colorForFlash = flashImage.color;
		colorForFlash.a = 0;
		flashImage.color = colorForFlash;
	}

	void Update() {
		if (fade) {
			colorForCaptureShot.a = 1f;
			captureShotImage.color = colorForCaptureShot;

			colorForFlash.a = 1f;
			flashImage.color = colorForFlash;

			captureShotImage.enabled = true;
			flashImage.enabled = true;

			count = true;
			fade = false;
		}

		if (count) {
			timer += Time.deltaTime;

			if (timer > freezeTime) {
				colorForCaptureShot.a -= Time.deltaTime * recoveryRate;
				captureShotImage.color = colorForCaptureShot;

				colorForFlash.a -= Time.deltaTime * recoveryRate;
				flashImage.color = colorForFlash;

				cam.GetComponent<BloomOptimized> ().intensity -= Time.deltaTime * .5f;

//				if (captureShotImage.color.a <= 0) {
//					timer = 0;
//					count = false;
//				}

				if (cam.GetComponent<BloomOptimized> ().intensity <= 0) {
					timer = 0;
					count = false;
					reset = true;
				}
			}
		} else {
			if (reset) {
				cam.GetComponent<BloomOptimized> ().enabled = false;
				captureShotImage.enabled = false;
				flashImage.enabled = false;
				reset = false;
			}
		}
	}

	void LateUpdate() {
		if (cam == null) {
			return;
		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			Flash ();
		}
	}

	private Texture Capture() {
		RenderTexture rt = new RenderTexture (Screen.width, Screen.height, 24);
		cam.targetTexture = rt;
		Texture2D screenShot = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);

		cam.Render ();
		RenderTexture.active = rt;
		screenShot.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);
		screenShot.Apply ();
		cam.targetTexture = null;

		RenderTexture.active = null;
		Destroy (rt);

		return screenShot;
	}

	public void RegisterCamera(Camera _playerCamera)
	{
		cam = _playerCamera;
	}

	public void Flash(float _freezeTime = 1f, float _recoveryRate = 0.2f){

		cam.GetComponent<BloomOptimized> ().intensity = 2.5f;
		cam.GetComponent<BloomOptimized> ().enabled = true;

		captureShotImage.enabled = true;
		captureShotImage.texture = Capture ();

		freezeTime = _freezeTime;
		recoveryRate = _recoveryRate;

		fade = true;

		ResetFlash ();
	}

	private void ResetFlash(){
		
		count = false;
		timer = 0;
	}
}
