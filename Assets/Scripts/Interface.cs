﻿using UnityEngine;
using System.Collections;

public interface IHittable {
	void Hit(Collider _col, Bullet _bullet, bool _isServerProcess);
}

public interface IDamagable {
	void ReceiveDamage(Weapon _weapon, int _damageTaken, bool _vitalShot, byte _shooterIdentifier);
}

public interface ICollectable
{
	void OnWeaponCollected (WeaponType _pickedWeaponType, int _ammoAmount);
	void OnPowerUpCollected(int _powerUpIdentifier);
	void OnHealthBagCollected (int _healAmount);
	void OnGrenadeCollected (GrenadeType _grenadeType, int _amount);
}

public interface IHitByGrenade {
	void GrenadeHit(Collider _col,Grenade _grenade, bool _iServerProcess);
}
