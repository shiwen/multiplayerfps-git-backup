﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: Bullet.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 12/05/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Cee Jay	| 12/05/16	|	- added various function
/// 					  17/05/16	|	- modified OnTriggerEnter to check transform.root if its equals to master object
/// 					  18/05/16	|	- modified OnTriggerEnter so the bullet does not collide with itself
/// 					  20/05/16	|	- rework the collision checking by using reference of the object instead of naming
/// 					  25/05/16	|	- added bullet hole effect
/// 					  30/05/16	|	- removed bullet hole effect, shifted feature to IHittable interface
/// 					  16/06/16	|	- restructure several functions to convert this into a base class as for other projectile classes
/// 									- removed unused variables
/// 					  27/06/16	|	- modified hit_point, hit_normal & shooterIdentifier from protected to public access
/// 									- added bulletObj & currentWeapon
/// 									- modified parameters in OnBulletImpact by using <Bullet> due to the amount limitation from Action class 
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class Bullet : NetworkBehaviour {

	public Action <Collider,Bullet> OnBulletImpact;

	protected GameObject masterObject;

	public GameObject bulletObj;

	protected PreciseCollider preciseCollider; 

	public Vector3 hit_point;

	public Vector3 hit_normal;

	protected Vector3 launchPos;

	public byte shooterIdentifier;

	public Weapon currentWeapon;

	protected void Awake(){
		preciseCollider = GetComponent<PreciseCollider>();
		launchPos = transform.position;
		bulletObj = gameObject;
	}

	public void SetShooterIdentifier(byte _identifier){
		shooterIdentifier = _identifier;
	}

	public void SetCurrentWeapon(Weapon _weapon){
		currentWeapon = _weapon;
	}

	public void CanSelfHarm(bool _value = true, GameObject _masterObject = null){
		if(!_value){

			masterObject = _masterObject;

			if(_masterObject == null){
				Debug.LogError("Master object not defined");
			}
		}
	}

	protected virtual void OnTriggerEnter(Collider _col){

		if(_col.gameObject.GetComponent<Bullet>()){
			return;
		}

		if(masterObject != null){
			if(_col.transform.root.gameObject.Equals(masterObject)){
				return;
			}
		}


//		Debug.Log("Hit " + _col.name);

		//Get Collision Point & normal rotation
		hit_point = preciseCollider.GetPreviousPosition();

		hit_normal = preciseCollider.GetHitNormal();

		if(OnBulletImpact != null){
			OnBulletImpact(_col,this);
		}

//		Destroy (gameObject);
	}
}
