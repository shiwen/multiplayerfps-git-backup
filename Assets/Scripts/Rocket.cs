﻿/// <summary> -------------------------------------------------------------------------------------------------------------------
/// CLASS NAME		: Rocket.cs
/// FUNCTION		: 
/// REQUIREMENT		: N/A
/// -----------------------------------------------------------------------------------------------------------------------------
/// CREATED BY		: Cee Jay
/// DATE			: 16/06/16
/// SCRIPT REF		: 
/// -----------------------------------------------------------------------------------------------------------------------------
/// VERSION	| Name		| DATE		|	DESCRIPTION
/// 0.1		| Cee Jay	| 16/06/16	|	- added explosion checking logic
/// 									- temporarily disable explosion explosure checking
/// </summary> ------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;

public class Rocket : Bullet {

	[SerializeField]
	protected GameObject explosionPrefab;

	private float explosionRadius;

	[SerializeField]
	private LayerMask mask;

	protected override void OnTriggerEnter(Collider _col){
		if(_col.gameObject.GetComponent<Bullet>()){
			return;
		}

		if(masterObject != null){
			if(_col.transform.root.gameObject.Equals(masterObject)){
				return;
			}
		}

		if (explosionRadius == 0) {
			Debug.LogError ("Explosion radius not defined!");
		}

//		Debug.Log("Hit " + _col.name);

		//Get Collision Point & normal rotation
		hit_point = preciseCollider.GetPreviousPosition();

		hit_normal = preciseCollider.GetHitNormal();

		//Create explosion effect
		if (explosionPrefab != null) {
			Instantiate (explosionPrefab, hit_point, Quaternion.identity);
		}


		Collider[] _hitColliders = Physics.OverlapSphere (hit_point, explosionRadius);

		foreach (Collider col in _hitColliders) {

			//if target is exposed to the explosion
//			if(!Physics.Linecast(hit_point,col.transform.position,mask)){
				if(OnBulletImpact != null){
					OnBulletImpact(col,this);
				}

				Debug.DrawLine (hit_point, col.transform.position, Color.red, 2f);

//				Debug.Log ("hit " + col.name);
//			}
		}

//		Destroy (gameObject);
	}

	public void SetExplosionRadius(float _radius){
		explosionRadius = _radius;
	}
}
