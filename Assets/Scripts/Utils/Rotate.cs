﻿using UnityEngine;
using System.Collections;

namespace Utility
{
	public class Rotate : MonoBehaviour 
	{
		public float rotateRate;
		public Vector3 rotateAxis;
		public float rotateDirection = 1.0f;

		void Update()
		{
			transform.Rotate (rotateAxis, rotateRate * rotateDirection * Time.deltaTime);
		}
	}
}