﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkInterpolate : NetworkBehaviour {

	[Header("Options")]
	public float smoothSpeed = 10;

	[SyncVar]
	private Vector3 mostRecentPos;
	[SyncVar]
	private Vector3 mostRecentRot;

	private Vector3 prevPos;
	private Vector3 prevRot;

	void FixedUpdate()
	{
		if (isLocalPlayer)
		{
			if (prevPos != transform.position)
			{
				CmdSendPosToServer(transform.position);
				prevPos = transform.position;
			}

			if (prevRot != transform.eulerAngles) 
			{
				CmdSendRotToServer(transform.eulerAngles);
				prevRot = transform.eulerAngles;
			}
		}
		else
		{
			transform.position = Vector3.Lerp(transform.position, mostRecentPos, smoothSpeed * Time.deltaTime);
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(mostRecentRot), smoothSpeed * Time.deltaTime);
		}
	}

	[Command]
	void CmdSendPosToServer(Vector3 pos)
	{
		mostRecentPos = pos;
	}

	[Command]
	void CmdSendRotToServer(Vector3 rot)
	{
		mostRecentRot = rot;
	}

}