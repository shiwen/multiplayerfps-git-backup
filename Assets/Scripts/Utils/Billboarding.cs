﻿using UnityEngine;
using System.Collections;

public class Billboarding : MonoBehaviour {

	private Camera m_Camera;

	void Start(){
		if (m_Camera == null || m_Camera.gameObject.activeSelf == false) {
			m_Camera = Camera.main;
		}
	}

	void Update()
	{
//		if (m_Camera.gameObject.activeSelf == false) {
//			if (Camera.main != null) { 
				m_Camera = Camera.main;
//			}
//		}

		if (m_Camera != null) {
			transform.LookAt (transform.position + m_Camera.transform.rotation * Vector3.forward, m_Camera.transform.rotation * Vector3.up);
		}
	}
}
