﻿using UnityEngine;
using System.Collections;

public class DestroyOnTimer : MonoBehaviour {

	[SerializeField]
	private float destroyTimer;

	// Use this for initialization
	void Start () {
		Destroy(gameObject,destroyTimer);
	}
}
