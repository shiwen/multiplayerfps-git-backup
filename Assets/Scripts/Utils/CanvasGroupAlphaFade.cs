﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasGroupAlphaFade : MonoBehaviour 
{
	public delegate void FunctionToBeCalled ();
	
	public CanvasGroup canvasGroup;
	private float initialAlpha;
	private float targetedAlpha;
	private float duration;
	private float counter = 0;
	private bool fading = false;
	private FunctionToBeCalled functionToBeCalled;
	
	void Awake()
	{
		canvasGroup = GetComponent<CanvasGroup> ();

		if (canvasGroup == null) 
		{
			Debug.LogError ("canvas group component missing!");
		}
	}

	public void StopFade()
	{
		fading = false;
		functionToBeCalled = null;
	}

	public void StartFade(float _targetAlpha, float _duration, FunctionToBeCalled _functionToBeCalled)
	{
		functionToBeCalled = _functionToBeCalled;
		StartFade (_targetAlpha, _duration);
	}

	public void StartFade(float _targetAlpha, float _duration)
	{
		targetedAlpha = _targetAlpha;
		duration = _duration;
		initialAlpha = canvasGroup.alpha;
		counter = 0;
		fading = true;
	}

	public void SetAlpha(float _targetAlpha)
	{
		canvasGroup.alpha = _targetAlpha;
	}

	void Update()
	{
		if (fading) 
		{
			float currentProgress = counter / duration;

			if (counter >= duration) 
			{
				canvasGroup.alpha = targetedAlpha;
				fading = false;

				if (functionToBeCalled != null) 
				{
					functionToBeCalled ();
					functionToBeCalled = null;
				}
			} 
			else 
			{
				counter += Time.deltaTime;
				canvasGroup.alpha = initialAlpha + ((targetedAlpha - initialAlpha) * currentProgress);
			}
		}
	}
}
