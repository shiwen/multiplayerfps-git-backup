﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIImageColorTransitor : MonoBehaviour 
{
	private class ImageTransitInfo
	{
		public Image targetedImage;
		public float fadeTime;
		public float fadeTimeCounter;
		public Color targetedColor;
		public Color originColor;
	}

	List<ImageTransitInfo> transitInfo = new List<ImageTransitInfo>();

	private bool fading = false;

	public void StartTransition(Image _targetedImage, Color _targetedColor, float _duration)
	{
		ImageTransitInfo _transitInfo = new ImageTransitInfo ();
		_transitInfo.targetedImage = _targetedImage;
		_transitInfo.fadeTime = _duration;
		_transitInfo.fadeTimeCounter = 0.0f;
		_transitInfo.targetedColor = _targetedColor;
		_transitInfo.originColor = _targetedImage.color;

		ImageTransitInfo _existedInfo = GetInfoByImage (_targetedImage);

		if (_existedInfo != null)
			transitInfo.Remove (_existedInfo);
		
		transitInfo.Add (_transitInfo);
	
		if(transitInfo.Count == 1)
			fading = true;
	}

	void Update()
	{
		if (fading) 
		{
			if (transitInfo.Count != 0) 
			{
				List<ImageTransitInfo> _completedTransition = new List<ImageTransitInfo> ();
				
				foreach (ImageTransitInfo _candidate in transitInfo) 
				{
					if (_candidate.fadeTimeCounter >= _candidate.fadeTime) 
					{
						_candidate.targetedImage.color = _candidate.targetedColor;
						_completedTransition.Add (_candidate);
					} 
					else 
					{
						_candidate.targetedImage.color = Color.Lerp (_candidate.originColor, _candidate.targetedColor, _candidate.fadeTimeCounter / _candidate.fadeTime);
						_candidate.fadeTimeCounter += Time.deltaTime;
					}
				}

				foreach (ImageTransitInfo _candidate in _completedTransition) 
				{
					transitInfo.Remove (_candidate);
				}
			} 
			else 
			{
				fading = false;
			}
		}
	}

	private ImageTransitInfo GetInfoByImage(Image _targetedImage)
	{
		foreach (ImageTransitInfo _candidate in transitInfo) 
		{
			if (_candidate.targetedImage == _targetedImage)
				return _candidate;
		}

		return null;
	}
}
