﻿using UnityEngine;
using System.Collections;

public class PickableObjectSpinnerUtility : MonoBehaviour 
{
	public float spinRate;
	public float spinAngle;
	public float restTime;

	private float rotatedAngle = 0;
	private float restTimeCounter = 0;
	private bool resting = false;

	void Update()
	{
		if (resting) 
		{
			if (restTimeCounter >= restTime) 
			{
				restTimeCounter = 0;
				resting = false;
			} 
			else 
			{
				restTimeCounter += Time.deltaTime;
			}
		} 
		else 
		{
			if (rotatedAngle >= spinAngle) 
			{
				resting = true;
				rotatedAngle = 0;
			} 
			else 
			{
				Quaternion _originAngle = gameObject.transform.rotation;
				gameObject.transform.Rotate (new Vector3 (0, 1, 0), spinRate * Time.deltaTime);
				rotatedAngle += Quaternion.Angle (_originAngle, gameObject.transform.rotation);
			}
		}
	}
}
