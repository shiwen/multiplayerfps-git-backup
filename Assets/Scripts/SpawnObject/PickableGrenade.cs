﻿using UnityEngine;
using System.Collections;

public class PickableGrenade :  Pickable
{
	public GrenadeType _grenadeType;
	
	protected override void PickableCollected (ICollectable _pickerInterface)
	{
		_pickerInterface.OnGrenadeCollected (_grenadeType, 1);
	}
}
