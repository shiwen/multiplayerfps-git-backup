﻿using UnityEngine;
using System.Collections;

public class PickablePowerUp : Pickable 
{
	public int powerUpIdentifier;
	
	protected override void PickableCollected (ICollectable _pickerInterface)
	{
		_pickerInterface.OnPowerUpCollected (powerUpIdentifier);
	}
}
