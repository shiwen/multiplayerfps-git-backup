﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public enum NetworkSpawnedObjectType
{
	Enemy,
	Pickable,
	PowerUp
}

public class NetworkSpawnManager : NetworkBehaviour 
{
	public static NetworkSpawnManager instance;
	private Dictionary<NetworkSpawnedObjectType, List<NetworkSpawnedObject>> spawnedObjects;

	public void Initialize()
	{
		instance = this;
		spawnedObjects = new Dictionary<NetworkSpawnedObjectType, List<NetworkSpawnedObject>> ();

		foreach (NetworkSpawnedObjectType _candidate in System.Enum.GetValues(typeof(NetworkSpawnedObjectType))) 
		{
			spawnedObjects.Add (_candidate, new List<NetworkSpawnedObject> ());
		}
	}
	
	public NetworkSpawnedObject SpawnObject(GameObject _originPrefab, NetworkSpawnedObjectType _objectType, Vector3 _position)
	{
		if (_originPrefab.GetComponent<NetworkSpawnedObject> () != null) 
		{
			NetworkSpawnedObject _objectToBeSpawned = GameObject.Instantiate (_originPrefab).GetComponent<NetworkSpawnedObject>();
			_objectToBeSpawned.transform.position = _position;
			spawnedObjects [_objectType].Add (_objectToBeSpawned);
			NetworkServer.Spawn (_objectToBeSpawned.gameObject);
			_objectToBeSpawned.RpcInitialize ();
			return _objectToBeSpawned;
		}
		return null;
	}

	public void UnspawnObject(NetworkSpawnedObject _targetedObject)
	{
		spawnedObjects [_targetedObject.objectType].Remove (_targetedObject);
		GameObject.Destroy (_targetedObject.gameObject);
	}

	public void RegisterSpawnPrefabs(List<GameObject> _prefabs)
	{
		foreach (GameObject _candidate in _prefabs) 
		{
			ClientScene.RegisterPrefab(_candidate);
		}
	}

	#region accessor

	public NetworkSpawnedObject GetSpawnObjectByTypeAndNetID(NetworkSpawnedObjectType _objectType, NetworkInstanceId _netId)
	{
		List<NetworkSpawnedObject> _targetedObjectsHolder = spawnedObjects [_objectType];

		foreach (NetworkSpawnedObject _candidate in _targetedObjectsHolder) 
		{
			if (_candidate.netId == _netId)
				return _candidate;
		}

		return null;
	}

	#endregion
}
