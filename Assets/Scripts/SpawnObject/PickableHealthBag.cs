﻿using UnityEngine;
using System.Collections;

public class PickableHealthBag : Pickable 
{
	public int healAmount;
	
	protected override void PickableCollected (ICollectable _pickerInterface)
	{
		_pickerInterface.OnHealthBagCollected (healAmount);
	}
}
