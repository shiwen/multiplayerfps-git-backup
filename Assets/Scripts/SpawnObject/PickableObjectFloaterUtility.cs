﻿using UnityEngine;
using System.Collections;

public class PickableObjectFloaterUtility : MonoBehaviour 
{
	public float floatDeepRange;
	public float restTime;
	public float floatSpeed;

	private int floatDirection = 1;
	private int floatPortionCompleteCount = 0;
	private float restTimeCounter;
	private bool resting = false;
	private Vector3 originPosition;

	void Start()
	{
		originPosition = transform.position;
	}

	void Update()
	{
		if (resting) 
		{
			if (restTimeCounter >= restTime) 
			{
				resting = false;
				restTimeCounter = 0;
			} 
			else 
			{
				restTimeCounter += Time.deltaTime;
			}
		} 
		else 
		{
			if (floatPortionCompleteCount >= 2) 
			{
				resting = true;
				floatPortionCompleteCount = 0;
			} 
			else 
			{
				if (Vector3.Distance (originPosition, transform.position) >= floatDeepRange) 
				{
					transform.position = originPosition + new Vector3 (0, floatDeepRange * -floatDirection);
					originPosition = transform.position;
					floatDirection = -floatDirection;
					floatPortionCompleteCount++;
				} 
				else 
				{
					transform.position += new Vector3 (0, floatSpeed * -floatDirection * Time.deltaTime);
				}
			}
		}
	}
}
