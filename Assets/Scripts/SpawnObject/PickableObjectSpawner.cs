﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PickableObjectSpawner : GameplayBehavior 
{
	public enum SpawnMethod
	{
		RandomItemRoundRobinPosition,
		RandomItemRandomPosition
	}
	
	public Pickable[] pickableObjectsPool;
	public Transform[] spawnPoints;
	public float spawnInterval;
	public SpawnMethod spawnMethod;
	public float itemAliveTime;

	private NetworkSpawnManager networkSpawnManager;
	private float spawnTimeCounter;
	private int currentSpawnedPositionsIndex = 0;
	private Dictionary<Transform, Pickable> spawnPointObjectReference;

	public override void HostInitialize ()
	{
		networkSpawnManager = NetworkSpawnManager.instance;
		spawnTimeCounter = 0;
		RegisterPickablePrefabs ();
		spawnPointObjectReference = new Dictionary<Transform, Pickable> ();

		foreach(Transform _candidate in spawnPoints)
		{
			spawnPointObjectReference.Add (_candidate, null);
		}
	}

	public override void ClientInitialize ()
	{
		networkSpawnManager = NetworkSpawnManager.instance;
		RegisterPickablePrefabs ();
	}

	private void RegisterPickablePrefabs()
	{
		List<GameObject> _prefabsToBeRegistered = new List<GameObject> ();

		foreach (Pickable _candidate in pickableObjectsPool) 
		{
			_prefabsToBeRegistered.Add (_candidate.gameObject);
		}

		networkSpawnManager.RegisterSpawnPrefabs (_prefabsToBeRegistered);
	}

	void Update()
	{
		if (spawnTimeCounter >= spawnInterval) 
		{
			switch (spawnMethod) 
			{
			case SpawnMethod.RandomItemRoundRobinPosition:
				SpawnPickableObject (GetRandomPickableItem (), spawnPoints [currentSpawnedPositionsIndex], true);
				currentSpawnedPositionsIndex++;
				if (currentSpawnedPositionsIndex >= spawnPoints.Length) 
				{
					currentSpawnedPositionsIndex = 0;
				}
				break;

			case SpawnMethod.RandomItemRandomPosition:
				SpawnPickableObject(GetRandomPickableItem(), GetRandomSpawnPoint(), true);
				break;
			}

			spawnTimeCounter = 0;
		}

		spawnTimeCounter += Time.deltaTime;
	}

	private void SpawnPickableObject(Pickable _targetedPrefab, Transform _targetedSpawnPoint, bool _replaceOccupiedSlot)
	{
		if (spawnPointObjectReference [_targetedSpawnPoint] == null) 
		{
			Pickable _spawnedItem = (Pickable)networkSpawnManager.SpawnObject (_targetedPrefab.gameObject, NetworkSpawnedObjectType.Pickable, _targetedSpawnPoint.position);
			spawnPointObjectReference [_targetedSpawnPoint] = _spawnedItem;
			_spawnedItem.aliveTime = itemAliveTime;
		} 
		else 
		{
			if (_replaceOccupiedSlot) 
			{
				networkSpawnManager.UnspawnObject (spawnPointObjectReference [_targetedSpawnPoint]);
				Pickable _spawnedItem = (Pickable)networkSpawnManager.SpawnObject (_targetedPrefab.gameObject, NetworkSpawnedObjectType.Pickable, _targetedSpawnPoint.position);
				spawnPointObjectReference [_targetedSpawnPoint] = _spawnedItem;
				_spawnedItem.aliveTime = itemAliveTime;
			}
		}
	}

	private Pickable GetRandomPickableItem()
	{
		int _randomedIndex = Random.Range (0, pickableObjectsPool.Length);
		return pickableObjectsPool [_randomedIndex];
	}

	private Transform GetRandomSpawnPoint()
	{
		int _randomedIndex = Random.Range (0, spawnPoints.Length);
		return spawnPoints [_randomedIndex];
	}
}
