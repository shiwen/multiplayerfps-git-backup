﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public abstract class NetworkSpawnedObject : NetworkBehaviour 
{
	[HideInInspector]
	public NetworkSpawnedObjectType objectType;
	protected NetworkSpawnManager spawnManager = NetworkSpawnManager.instance;

	[ClientRpc]
	public void RpcInitialize()
	{
		AssignObjectType ();
	}

	protected abstract void AssignObjectType ();
}
