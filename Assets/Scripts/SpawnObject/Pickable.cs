﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public abstract class Pickable : NetworkSpawnedObject 
{
	//properties
	[HideInInspector]
	public float aliveTime;

	protected abstract void PickableCollected (ICollectable _pickerInterface);

	protected override void AssignObjectType ()
	{
		base.objectType = NetworkSpawnedObjectType.Pickable;
	}
		
	void Start()
	{
		if(isServer)
			Invoke ("AliveTimeOut", aliveTime);
	}

	private void AliveTimeOut()
	{
		GameObject.Destroy (this.gameObject);
	}

	public void OnTriggerEnter(Collider _collider)
	{
		if (isServer) 
		{
			ICollectable _collector = _collider.transform.root.GetComponent<ICollectable> ();
			
			if (_collector != null)
			{
				PickableCollected (_collector);
				RpcPickableCollected (_collider.transform.GetComponent<NetworkIdentity> ().netId);
				spawnManager.UnspawnObject (this);
			}
		}
	}

	[ClientRpc]
	private void RpcPickableCollected(NetworkInstanceId _netID)
	{
		PickableCollected (ClientScene.FindLocalObject(_netID).GetComponent<ICollectable>());
	}
}
