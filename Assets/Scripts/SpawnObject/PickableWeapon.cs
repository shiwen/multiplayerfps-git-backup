﻿using UnityEngine;
using System.Collections;

public class PickableWeapon : Pickable 
{
	public int ammoPickupAmount;
	public WeaponType identifier;
	
	protected override void PickableCollected (ICollectable _pickerInterface)
	{
		_pickerInterface.OnWeaponCollected (identifier, ammoPickupAmount);
	}
}
