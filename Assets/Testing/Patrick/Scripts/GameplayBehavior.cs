﻿using UnityEngine;
using System.Collections;

public abstract class GameplayBehavior : MonoBehaviour 
{
	public abstract void HostInitialize ();
	public abstract void ClientInitialize();
}
