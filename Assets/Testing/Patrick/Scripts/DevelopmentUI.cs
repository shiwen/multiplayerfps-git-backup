﻿using UnityEngine;
using System.Collections;

public class DevelopmentUI : MonoBehaviour 
{
	public DevelopmentNetworkManager networkManager;
	
	void Awake()
	{
		if (MultiPlayerFPSNetworkConnectionManager.GetInstance () != null) 
		{
			gameObject.SetActive (false);
			networkManager.gameObject.SetActive (false);
		}
	}

	public void HostButtonClicked()
	{
		gameObject.SetActive (false);
		networkManager.StartHosting ("development host", LobbyController.GameMode.FreeForAll);
		GameManager.instance.SendMessage ("SetupCustomEventListners", true);
		GameManager.instance.SendMessage ("SetUpGameplayBehavior", true);
		Invoke ("SimultateGameplaySceneInit", 1.0f);
	}

	public void JoinButtonClicked()
	{
		gameObject.SetActive (false);
		networkManager.JoinServer (networkManager.networkAddress);
		Invoke ("SimultateGameplaySceneInit", 1.0f);
	}

	public void SimultateGameplaySceneInit()
	{
		networkManager.FakeInit ();
	}
		
}
