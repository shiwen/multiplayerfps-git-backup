﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class DevelopmentNetworkManager : MultiPlayerFPSNetworkConnectionManager 
{
	void Start()
	{
		Initialize(GameManager.instance.GetComponent<GameplayConnectionReactor>());
		GameManager.instance.networkManager = this;
		SetupSelfPlayerName ("FakePlayer");
	}

	public void FakeInit()
	{
		PerformActualConnection ();
		Invoke ("FakePlayerData", 0.2f);
	}

	private void FakePlayerData()
	{
		((GameplayNetworkPlayer)GetLocalMultiplayerFpsNetworkPlayer ().networkPlayerConnectionAuthority).SpawnSelectedCharacter (PlayerRole.Assault);
	}
}
